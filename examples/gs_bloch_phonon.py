from contextlib import contextmanager
from math import sqrt
from typing import Callable, Mapping

from multirods_pack.ideal import eigen_energy
from multirods_pack.qmc import JastrowModel, MultiRodsSystem
from multirods_pack.qmc.trial_funcs import (
    GSBlochFuncs, OptGSBlochFuncs, OptPhononFuncs, PhononFuncs
)

__all__ = ['GSBlochPhononModel']


class GSBlochPhononModel(JastrowModel):
    """"""

    def __init__(self, system: MultiRodsSystem,
                 boson_number: int,
                 supercell_size: int):
        """

        :param system:
        :param boson_number:
        :param supercell_size:
        """
        super().__init__(system, boson_number, supercell_size)

        v0 = system.lattice_depth
        r = system.lattice_ratio
        e0 = float(eigen_energy(v0, r))
        k1, kp1 = sqrt(e0), sqrt(v0 - e0)

        # For this model, the one-body function parameters
        # remain fixed from the very instantiation.
        self._one_body_func_params = v0, r, e0, k1, kp1

        self._bloch_funcs = GSBlochFuncs()
        self._phonon_funcs = PhononFuncs()
        self._trial_funcs = self._init_trial_funcs()

    @property
    def one_body_func(self) -> Callable:
        return self._trial_funcs[self.FN.OBF]

    @property
    def two_body_func(self) -> Callable:
        return self._trial_funcs[self.FN.TBF]

    @property
    def one_body_func_log_dz(self) -> Callable:
        return self._trial_funcs[self.FN.OBF_LD]

    @property
    def two_body_func_log_dz(self) -> Callable:
        return self._trial_funcs[self.FN.TBF_LD]

    @property
    def one_body_func_log_dz2(self) -> Callable:
        return self._trial_funcs[self.FN.OBF_LD2]

    @property
    def two_body_func_log_dz2(self) -> Callable:
        return self._trial_funcs[self.FN.TBF_LD2]

    @property
    def one_body_func_params(self):
        """

        :return:
        """
        return self._one_body_func_params

    def two_body_func_params(self, contact_cutoff: float):
        """

        :param contact_cutoff:
        :return:
        """
        gn = self.system.interaction_strength
        nop = self.boson_number
        scs = self.supercell_size
        rm = contact_cutoff

        return self._phonon_funcs.two_body_func_params(gn, nop, rm, scs)

    @property
    def trial_funcs(self):
        return self._trial_funcs

    @property
    def boundaries(self):
        """"""
        sc_size = self.supercell_size
        return 0, 1. * sc_size

    def _init_trial_funcs(self):
        """

        :return:
        """
        trial_funcs = self._gather_trial_funcs(self._bloch_funcs,
                                               self._phonon_funcs)
        return trial_funcs

    @classmethod
    def _gather_trial_funcs(cls, bloch_funcs: Mapping,
                            phonon_funcs: Mapping):
        """

        :param bloch_funcs:
        :param phonon_funcs:
        :return:
        """
        fn = cls.FN
        return {
            fn.OBF: bloch_funcs['one_body_func'],
            fn.TBF: phonon_funcs['two_body_func'],

            fn.OBF_LD: bloch_funcs['one_body_func_log_dz'],
            fn.TBF_LD: phonon_funcs['two_body_func_log_dz'],

            fn.OBF_LD2: bloch_funcs['one_body_func_log_dz2'],
            fn.TBF_LD2: phonon_funcs['two_body_func_log_dz2']
        }

    @contextmanager
    def optimized(self, opt_params):
        """

        :param opt_params:
        :return:
        """
        self_funcs = self._trial_funcs

        obf_params, tbf_params = opt_params
        one_body_kernel = OptGSBlochFuncs(obf_params)
        two_body_kernel = OptPhononFuncs(tbf_params)

        opt_funcs = self._gather_trial_funcs(one_body_kernel,
                                             two_body_kernel)
        # Switch trial functions
        self._trial_funcs = opt_funcs

        yield self

        # Restore original trial functions
        self._trial_funcs = self_funcs
