from pprint import pprint
from typing import Sequence

from multirods_pack.config import BaseConfig, EXT_DATA, MESH_ITEMS

config = BaseConfig.from_file('./templates/test_config.yml')


def test_get_group():
    """"""

    sys_cfg = config.get_group('meta', config)
    for k, v in sys_cfg.items():
        print('{} -> {}'.format(k, v))

    assert True


def test_nonexistent_group():
    """"""
    cfg = config.get_group('meta.users', config)
    for k, v in cfg.items():
        print('{} -> {}'.format(k, v))

    assert True


def test_schedule():
    """"""
    # FIXME: dirty test
    schedule = config.get_schedule()
    for elem in schedule:
        pprint(elem)
        if isinstance(elem, Sequence):
            for e in elem:
                ed = e[EXT_DATA]
                it = e[MESH_ITEMS]
                for v in it(ed):
                    print(v)
        else:
            ed = elem[EXT_DATA]
            it = elem[MESH_ITEMS]
            for v in it(ed):
                print(v)
