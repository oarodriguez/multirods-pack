from multirods_pack.config import ID, MESH_ITEMS, STAGES
from multirods_pack.qmc.config import (
    Config, SystemParams
)

config = Config.from_file('../templates/qmc/test_config.yml')


def test_system_group():
    """"""

    sys_cfg = config.system_config
    for k, v in sys_cfg.items():
        print('{} -> {}'.format(k, v))

    assert True


def test_sampler_group():
    """"""

    sys_cfg = config.sampler_config
    for k, v in sys_cfg.items():
        print('{} -> {}'.format(k, v))

    assert True


def test_schedule():
    """"""

    schedule = config.schedule
    for task in schedule[STAGES]:
        print('{}'.format(task.get(ID, None)))
        mesh = task[MESH_ITEMS]
        for v in mesh():
            print('{}'.format(v))

    assert True


def test_schedule_params():
    """"""

    schedule_tasks = config.schedule[STAGES]
    # print(schedule_tasks)
    print()
    for task in schedule_tasks:
        print('***')
        # print('{}'.format(task.get(ID, None)))
        # print('{}'.format(task[MESH_ITEMS]))
        mesh = task[MESH_ITEMS]
        for v in mesh():
            print('{}'.format(v))
        print('***')

    assert True


def test_sys_config_enum():
    """"""
    for param in SystemParams:
        print(param.full_name)


def test_describe():
    """"""
    # parser.describe()
