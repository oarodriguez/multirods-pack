from math import pi
from pprint import pprint

import numpy as np

from multirods_pack.constants import ER, LKP
from multirods_pack.qmc.base import KernelFN
from multirods_pack.qmc.models.phonon_legacy import PhononModel
from multirods_pack.qmc.variational import (
    CbCSampler, CbCSamplerAccumulator, jit_accumulator, jit_base_pdf_sampler,
    jit_base_sampler_accumulator
)

v0 = 25 * ER
r = 1
gn = 0.1 * ER
nop = 100
sc_size = 50 * LKP
rm = 0.25 * LKP

boundaries = 0, sc_size

model = PhononModel(v0, r, gn, nop, sc_size, rm)


def test_jit_mc_sampler():
    """

    :return:
    """
    delta_mh = 0.1 * LKP
    ini_sys_conf = model.init_sys_conf(random_=True)

    chain_samples = 1000
    burn_in_samples = 1000
    rng_seed = np.random.randint(0, 10000)

    sampler = jit_base_pdf_sampler(model)
    chain_a, rate_a = sampler(delta_mh,
                              chain_samples=chain_samples,
                              burn_in_samples=burn_in_samples,
                              ini_sys_conf=ini_sys_conf,
                              rng_seed=rng_seed)

    print(chain_a.shape)


def test_mc_sampler():
    """

    :return:
    """
    move_delta = 0.01 * LKP
    ini_sys_conf = model.init_sys_conf()

    chain_samples = 500
    burn_in_samples = 100
    rng_seed = np.random.randint(0, 10000)

    sampler = CbCSampler(model)
    chain = sampler.exec(move_delta,
                         ini_sys_conf,
                         chain_samples,
                         burn_in_samples,
                         rng_seed)
    print(chain)


def test_jit_sampler_accumulator():
    """

    :return:
    """
    delta_mh = 0.01 * LKP
    ini_sys_conf = model.init_sys_conf(random_=True)

    fn_id = KernelFN.L_E
    fn_args = ()
    chain_steps = 5000
    burn_in_steps = 0
    rng_seed = np.random.randint(0, 10000)

    sampler_acc = jit_base_sampler_accumulator(model, fn_id)
    acc_result = sampler_acc(delta_mh,
                             chain_samples=chain_steps,
                             burn_in_samples=burn_in_steps,
                             ini_sys_conf=ini_sys_conf,
                             rng_seed=rng_seed,
                             fn_args=fn_args)

    e_mean, e_sqr, rate_a = acc_result
    e_apb = e_mean / nop

    print(e_apb)


def test_sampler_accumulator():
    """

    :return:
    """
    move_delta = 0.01 * LKP
    ini_sys_conf = model.init_sys_conf()

    fn_id = KernelFN.L_E
    fn_args = ()
    chain_samples = 5000
    burn_in_samples = 10000
    rng_seed = np.random.randint(0, 10000)

    sampler_acc = CbCSamplerAccumulator(model, fn_id)
    chain = sampler_acc.exec(move_delta,
                             ini_sys_conf,
                             fn_args,
                             chain_samples,
                             burn_in_samples,
                             rng_seed)
    print(chain)


def test_obd_sampler_accumulator():
    """

    :return:
    """
    delta_mh = 0.01 * LKP
    ini_sys_conf = model.init_sys_conf(random_=True)

    fn_id = KernelFN.L_OBD
    fn_args = (0.5 * LKP,)
    chain_samples = 5000
    burn_in_samples = 1000
    rng_seed = np.random.randint(0, 10000)

    obd_acc = jit_base_sampler_accumulator(model, fn_id)
    acc_results = obd_acc(delta_mh,
                          chain_samples=chain_samples,
                          burn_in_samples=burn_in_samples,
                          ini_sys_conf=ini_sys_conf,
                          rng_seed=rng_seed,
                          fn_args=fn_args)

    obd_mean, obd_sqr, rate_a = acc_results
    print(obd_mean)


def test_accumulator():
    """

    :return:
    """
    delta_mh = 0.1 * LKP
    ini_sys_conf = model.init_sys_conf(random_=True)

    chain_samples = 20000
    burn_in_samples = 0
    rng_seed = np.random.randint(0, 10000)

    mc_sampler = jit_base_pdf_sampler(model)
    chain, rate_a = mc_sampler(delta_mh,
                               chain_samples=chain_samples,
                               burn_in_samples=burn_in_samples,
                               ini_sys_conf=ini_sys_conf,
                               rng_seed=rng_seed)

    fn_args = ()
    accumulator = jit_accumulator(model, KernelFN.L_E)
    acc_results = accumulator(chain, fn_args)

    print(acc_results)


def test_sf_sampler_accumulator():
    """

    :return:
    """
    delta_mh = 0.01 * LKP
    ini_sys_conf = model.init_sys_conf(random_=True)

    sf_fn_id = KernelFN.L_SF
    fn_args = (pi / 2,)
    chain_samples = 50000
    burn_in_samples = 1000
    rng_seed = 1

    sf_acc = jit_base_sampler_accumulator(model, sf_fn_id)
    acc_results = sf_acc(delta_mh,
                         chain_samples=chain_samples,
                         burn_in_samples=burn_in_samples,
                         ini_sys_conf=ini_sys_conf,
                         rng_seed=rng_seed,
                         fn_args=fn_args)

    sf_mean, sf_sqr, rate_a = acc_results
    print(sf_mean)


def test_sf_accumulator():
    """

    :return:
    """
    delta_mh = 0.1 * LKP
    ini_sys_conf = model.init_sys_conf(random_=True)

    mc_sampler = jit_base_pdf_sampler(model)
    chain_samples = 50000
    burn_in_samples = 1000
    rng_seed = 1

    chain, rate_a = mc_sampler(delta_mh,
                               chain_samples=chain_samples,
                               burn_in_samples=burn_in_samples,
                               ini_sys_conf=ini_sys_conf,
                               rng_seed=rng_seed)

    sf_acc = jit_accumulator(model, KernelFN.L_SF)
    fn_args = (pi / 2,)

    acc_results = sf_acc(chain, fn_args)
    print(acc_results)


def test_sf_guv_sampler_accumulator():
    """

    :return:
    """
    delta_mh = 0.1 * LKP
    ini_sys_conf = model.init_sys_conf(random_=True)

    sf_fn_id = KernelFN.L_SF_GUV
    # kz_array = 0.5 * LKP
    kz_array = 2 * pi * np.arange(1, 2 * sc_size + 1) / sc_size
    fn_args = (kz_array,)
    chain_samples = 2000
    burn_in_samples = 2000
    rng_seed = None

    sf_acc = CbCSamplerAccumulator(model, sf_fn_id, disable_jit=True)
    acc_results = sf_acc(delta_mh,
                         chain_samples=chain_samples,
                         burn_in_samples=burn_in_samples,
                         ini_sys_conf=ini_sys_conf,
                         rng_seed=rng_seed,
                         fn_args=fn_args)

    sf_mean, sf_sqr, rate_a = acc_results
    pprint(sf_mean / nop)


def test_obd_guv_sampler_accumulator():
    """

    :return:
    """
    delta_mh = 0.01 * LKP
    ini_sys_conf = model.init_sys_conf(random_=True)

    sf_fn_id = KernelFN.L_OBD_GUV
    # sz_array = 0.5 * LKP
    sz_array = np.linspace(0, 0.5, 30) * LKP
    fn_args = (sz_array,)
    chain_samples = 5000
    burn_in_samples = 100
    rng_seed = 1

    sf_acc = jit_base_sampler_accumulator(model, sf_fn_id)
    acc_results = sf_acc(delta_mh,
                         chain_samples=chain_samples,
                         burn_in_samples=burn_in_samples,
                         ini_sys_conf=ini_sys_conf,
                         rng_seed=rng_seed,
                         fn_args=fn_args)

    sf_mean, sf_sqr, rate_a = acc_results
    print(sf_mean)
