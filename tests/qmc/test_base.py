from multirods_pack.qmc import MultiRodsSystem, QMCDict
from multirods_pack.qmc.base import DictAttribute, dict_attribute

v0, r, gn = 100, 1, 1


def test_potential_func():
    """

    :return:
    """
    system = MultiRodsSystem(v0, r, gn)
    potential_func = system.potential_func

    # Basic test to check the potential func just works.
    z_delta = 1e-2
    assert potential_func(z=-z_delta) == system.lattice_depth
    assert potential_func(z=z_delta) == 0


def test_eq():
    """

    :return:
    """
    sys_1 = MultiRodsSystem(v0, r, gn)
    sys_2 = MultiRodsSystem(v0, r, gn)
    assert sys_1 == sys_2


def test_hash():
    """

    :return:
    """
    sys_1 = MultiRodsSystem(v0, r, gn)
    sys_2 = MultiRodsSystem(v0, r, gn)

    mapping = {
        sys_1: True
    }
    assert mapping[sys_2] == mapping[sys_1]


class TestKernel(QMCDict):
    """Test kernel"""

    const_a = DictAttribute('const_a', 'CONSTANT')

    # Attribute with an explicit name (alias)
    @dict_attribute('item_a')
    def func_a(self):
        print('>>> Calling func_a')
        return 'value_a'

    # Attribute without an explicit name
    @dict_attribute
    def func_b(self):
        print('>>> Calling func_b')
        value_a = self['item_a']
        return value_a, 'value_b'

    @dict_attribute('func_factory')
    def func_factory(self):
        return False


def test_qmc_kernel():
    """

    :return:
    """
    kernel = TestKernel()

    for k, v in kernel.items():
        print(k, v)

    assert kernel.const_a == kernel['const_a']
    assert kernel.func_a == kernel['item_a']
    assert kernel.func_b == kernel['func_b']
    assert kernel.func_factory == kernel['func_factory']

    item_b = kernel['func_b']
    print(item_b)
    assert item_b == ('value_a', 'value_b')
