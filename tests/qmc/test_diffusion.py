from datetime import datetime

import numpy as np
from numpy.random import random_sample

from multirods_pack.constants import ER, LKP
from multirods_pack.qmc.base import KernelFN
from multirods_pack.qmc.diffusion import DMCSampler, jit_metropolis_diffusion
from multirods_pack.qmc.models import PhononModel
from multirods_pack.qmc.variational import (
    jit_accumulator, jit_base_pdf_sampler
)

v0 = 0 * ER
r = 1
gn = 1e-2 * ER
nop = 25
sc_size = 50 * LKP
rm = 0.25 * LKP

boundaries = 0, sc_size

model = PhononModel(v0, r, gn, nop, sc_size, rm)


def init(chain_steps=5000):
    """

    :param chain_steps:
    :return:
    """
    delta_mh = 0.1 * LKP
    ini_sys_conf = model.init_sys_conf(random_=True)

    mc_sampler = jit_base_pdf_sampler(model)
    burn_in_steps = 0
    rng_seed = 1

    chain, rate_a = mc_sampler(delta_mh,
                               chain_steps=chain_steps,
                               burn_in_steps=burn_in_steps,
                               ini_sys_conf=ini_sys_conf,
                               rng_seed=rng_seed)

    return chain


def test_branching():
    """Test branching routine.
    :return:
    """
    nw, nop, pp = 10, 20, 3
    time_step = 0.1
    tsi = DMCSampler(model)

    sys_conf = np.random.random_sample((nw, nop, pp))
    lew_set = np.random.random_sample((nw, 2))

    branch_factors = np.array([2] * nw)
    branched_sys_conf, \
    branched_lew_set = tsi.branch(sys_conf, lew_set, branch_factors)

    try:
        # Catch the negative weights
        branch_factors, = np.array([-1] * nw)
        branched_sys_conf, \
        branched_lew_set = tsi.branch(sys_conf, lew_set, branch_factors)
    except ValueError:
        pass

    # All weights are zero
    branch_factors = np.array([0] * nw)
    branched_sys_conf, \
    branched_lew_set = tsi.branch(sys_conf, lew_set, branch_factors)

    assert np.allclose(branched_sys_conf, 0)


def test_diffuse():
    """"""

    nw, nop, pp = 10, 20, 4
    time_delta = 0.1
    ref_energy = 0.5
    # tsi = DMCSampler(time_delta)

    sys_conf = np.random.random_sample((nop, pp))
    diffuser = jit_metropolis_diffusion(model)

    next_conf = diffuser(time_delta, sys_conf, ref_energy, ref_energy)

    print(next_conf)


def test_diffuse_sys_conf():
    """

    :return:
    """
    chain = init()
    fn_args = ()
    accumulator = jit_accumulator(model, KernelFN.L_E)
    acc_results = accumulator(chain, fn_args)
    loc_energy_var, _ = acc_results

    print(loc_energy_var)

    time_delta = 0.1
    sys_conf = chain[-1]
    ref_energy = loc_energy_var

    sampler = jit_metropolis_diffusion(model)
    dmc_sampler_results = sampler(time_delta,
                                  sys_conf,
                                  loc_energy_var,
                                  ref_energy)
    # next_conf, le_next, wk, eff_time, acc = dmc_sampler_results
    next_conf, le_next, wk = dmc_sampler_results

    print(next_conf)
    print(le_next, wk)


def test_diffuse_sys_conf_set():
    """

    :return:
    """
    chain = init()
    fn_args = ()
    local_energy_fn = model.kernel[KernelFN.L_E]
    accumulator = jit_accumulator(model, KernelFN.L_E)
    acc_results = accumulator(chain, fn_args)
    loc_energy_var, _ = acc_results

    print()
    print(loc_energy_var)

    time_delta = 0.01
    sys_conf_set = chain[-1000:]
    ref_energy = loc_energy_var

    sampler = jit_metropolis_diffusion(model)

    dt = datetime.now()

    # weights = [1.0 for _ in range(len(sys_conf_set))]
    weights_set = []
    les_set = []
    for sys_conf in sys_conf_set:
        loc_energy = local_energy_fn(sys_conf)
        dmc_sampler_results = sampler(time_delta,
                                      sys_conf,
                                      loc_energy,
                                      ref_energy)
        next_conf, le_next, weight = dmc_sampler_results
        weights_set.append(weight)
        les_set.append(le_next)

    final = datetime.now()

    weights_set = np.array(weights_set)
    les_set = np.array(les_set)

    branch_factors = np.rint(weights_set + random_sample(weights_set.shape))
    print(branch_factors)

    le_avg = np.sum(weights_set * les_set) / weights_set.sum()

    print(weights_set)
    print(les_set)
    print(le_avg)

    print(final - dt)

    print()


def test_diffuse_sampler():
    """

    :return:
    """
    chain = init()
    fn_args = ()
    accumulator = jit_accumulator(model, KernelFN.L_E)
    acc_results = accumulator(chain, fn_args)
    loc_energy_var, _ = acc_results

    print()
    print(loc_energy_var)

    time_delta = 0.025
    sys_conf_set = chain[-100:]

    sampler = DMCSampler(model)
    energy_chain = sampler.exec(time_delta,
                                num_time_steps=500,
                                ini_conf_set=sys_conf_set)

    # final = datetime.now()
    print(energy_chain)


if __name__ == '__main__':
    test_diffuse_sampler()
