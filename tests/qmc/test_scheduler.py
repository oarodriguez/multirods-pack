from time import sleep

import numpy as np
from distributed import Client

from multirods_pack.config import (
    DISTRIBUTED, FUNC, MESH_ITEMS, SCHEDULE_STAGES
)
from multirods_pack.qmc.config import Config
from multirods_pack.scheduler import DistributedTask, Executor


def task_fn(data):
    sleep(0.5)
    v, _ = data
    return v


def test_task():
    """"""

    task_data = {
        'f1': np.linspace(0, 1, 100)
    }
    task_data = Config.get_stage_mesh(task_data)
    task = DistributedTask(task_fn, task_data)

    client = Client()
    task.submit(client)
    result = task.gather_results()
    for v in result:
        print(v)

    assert True


def test_task_with_mesh():
    """"""

    task_data = {
        'f1': np.linspace(0, 1, 100)
    }
    task_config = {
        'mesh_items': ['f1']
    }
    task_data = Config.get_stage_mesh(task_data, task_config)
    task = DistributedTask(task_fn, task_data)

    client = Client()
    task.submit(client)
    result = task.gather_results()
    for v in result:
        print(v)

    assert True


def test_task_with_mixed_mesh():
    """"""

    var = np.linspace(0, 1, 100)
    task_data = {
        'f1': var,
        'f2': var ** 2,
        'f3': ['A', 'B'],
    }
    task_config = {
        'mesh_items': [('f1', 'f2'), 'f3']
    }
    task_data = Config.get_stage_mesh(task_data, task_config)
    task = DistributedTask(task_fn, task_data)

    client = Client()
    task.submit(client)
    result = task.gather_results()
    for v in result:
        print(v)

    assert True


def task_fn_2(data):
    """"""
    sleep(0.5)
    v, _ = data
    return data


def test_task_with_children():
    """"""

    task_data_1 = {
        'f1': np.linspace(0, 1, 100)
    }
    task_data_1 = Config.get_stage_mesh(task_data_1)
    task_1 = DistributedTask(task_fn, task_data_1)

    task_data_2 = {
        'f2': ['A', 'B', 'C']
    }
    task_data_2 = Config.get_stage_mesh(task_data_2)
    task_2 = DistributedTask(task_fn_2, task_data_2)

    # Register the sub-task
    task_1.add_task(task_2)

    client = Client()
    task_1.submit(client)
    result = task_1.gather_results()
    for v in result:
        print(v)

    assert True


def test_task_with_mesh_and_children():
    """"""

    task_data_1 = {
        'f1': np.linspace(0, 1, 3)
    }
    task_config_1 = {
        'mesh_items': ['f1']
    }
    task_data_1 = Config.get_stage_mesh(task_data_1, task_config_1)
    task_1 = DistributedTask(task_fn, task_data_1)

    task_data_2 = {
        'F2': 'ABCDEFGHIJKLMNOQRSTUVWXYZ'
    }
    task_config_2 = {
        'mesh_items': ['F2']
    }

    task_data_22 = {
        'f2': 'abcdefghijklmnoqrstuvwxyz'
    }
    task_config_22 = {
        'mesh_items': ['f2']
    }

    # Register the sub-task
    task_data_2 = Config.get_stage_mesh(task_data_2, task_config_2)
    task_2 = DistributedTask(task_fn_2, task_data_2)
    task_1.add_task(task_2)

    # Register another the sub-task
    task_data_22 = Config.get_stage_mesh(task_data_22, task_config_22)

    task_22 = DistributedTask(task_fn_2, task_data_22)
    task_1.add_task(task_22)

    # cluster = LocalCluster(n_workers=2)
    print('> Starting local cluster and client...')
    client = Client()
    print('> Local cluster ready.')

    task_1.submit(client)

    print('> Starting execution.')

    task_1.submit(client)
    result = task_1.gather_results()

    print('> Finished')

    for v in result:
        print(v)
        print()

    assert True


def func_test_fn1(x):
    """"""
    sleep(0.5)
    d, *_ = x
    # print('Task 1')
    # print(v)
    # return 'Task1'
    return d


def func_test_fn2(v):
    """"""
    # print('Task 2')
    # print(v)
    # return 'Task2'
    d, *_ = v
    return d


def func_test_fn3(v):
    # """"""
    # print('Task 3')
    # print(d)
    # return 'Task3'
    d, *_ = v
    val = dict(d)
    x, y, z = val['x'], val['y'], val['z']
    return val['x'] + val['y'] + val['z']


def test_stage_unit():
    """"""
    stages_config = [[
        {
            FUNC: func_test_fn1,
            MESH_ITEMS: ['x'],
            DISTRIBUTED: True
        },
        {
            FUNC: func_test_fn2,
            MESH_ITEMS: ['y']
        },
        {
            FUNC: func_test_fn3,
            MESH_ITEMS: ['z']
        }]]

    data = {
        'x': [1],
        'y': ['A', 'B'],
        'z': ['Hi', 'Hola', 'Alo'],
        SCHEDULE_STAGES: stages_config
    }
    data = {
        'x': np.linspace(0, 1, 6),
        'y': np.linspace(0, 1, 900).reshape((30, 30)),
        'z': [1, 2, 3],
        SCHEDULE_STAGES: stages_config
    }

    config = ConfigBase(data)
    stages_config = config.get_schedule()

    exc_root = Executor(stages_config[0])

    client = Client()
    exc_root.submit(client)
    results = exc_root.gather_results()
    results = np.array(results)
    print(results.shape)

    for v in results:
        print(v)


if __name__ == '__main__':
    test_task_with_mesh_and_children()
