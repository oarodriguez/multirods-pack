# [MultiRods-Pack][repository]

A library to estimate and analyse the physical properties of an interacting
quantum many-body Bose gas within a multi-rods structure. It implements the
following methods:

- Mean-field theory, based on the solutions of the Gross-Pitaevskii equation.
- Variational an Diffusion Monte Carlo approaches.

It's written in pure Python. It uses [Numba][numba] to accelerate
performance-critical routines that execute CPU-intensive calculations, as well
as [Dask][dask] to distribute and handle the asynchronous execution of several
tasks in parallel.

The library is released under the BSD-3 License.

## Authors

Omar Abel Rodríguez López

- Github profile: [https://github.com/oarodriguez][ghp]
- Bitbucket profile: [https://bitbucket.org/oarodriguez][bbp]

[repository]: https://bitbucket.org/oarodriguez/multirods-pack/
[ghp]: https://github.com/oarodriguez
[bbp]: https://bitbucket.org/oarodriguez
[numba]: http://numba.pydata.org/
[dask]: http://dask.pydata.org/en/latest/
