import itertools
from collections import Callable, Iterator
from enum import Enum
from typing import Any, Sequence, Tuple, Union

import numpy as np
from numba import jit
from scipy.linalg import _flapack, solve


class Solvers(Enum):
    """"""
    NEWTON = 1
    SHERMAN_MORRISON = 2


class NewtonNonLinearSolver(Iterator):
    """"""

    def __init__(self, system_fn: Any,
                 initial_guess: np.ndarray,
                 system_fn_args: Any = None):
        """

        :param system_fn:
        :param initial_guess:
        :param system_fn_args:
        """
        system_fn_args = system_fn_args or ()
        fn_vector, jcb_matrix = system_fn(initial_guess,
                                          *system_fn_args)

        self.sys_fn = system_fn

        self.sys_fn_args = system_fn_args

        self.sys_fn_vector = fn_vector

        self.sys_jcb_matrix = jcb_matrix

        self.solution = np.copy(initial_guess)

    def update_state(self, fix):
        """

        :param fix:
        :return:
        """
        self.solution[:] += fix[:]
        fn_vec, jcb_mat = self.sys_fn(self.solution,
                                      *self.sys_fn_args)
        self.sys_fn_vector = fn_vec
        self.sys_jcb_matrix = jcb_mat

    def solve(self, jcb_matrix: np.ndarray,
              fn_vector: np.ndarray):
        """

        :param jcb_matrix:
        :param fn_vector:
        :return:
        """
        return solve(jcb_matrix, fn_vector)

    def __next__(self) -> Tuple[Any, np.ndarray]:
        """A single iteration of the Newton nonlinear solver.
        It takes the current phys_state_prev of the nonlinear
        system and the corresponding Jacobian matrix, and
        solves the linear system :math:`J y = -f`, where :math:`J`
        is the Jacobian matrix and :math:`f` is the nonlinear
        system evaluated in the current phys_state_prev.

        :return: The updated phys_state_prev.
        """
        fix = self.solve(self.sys_jcb_matrix,
                         -self.sys_fn_vector)
        self.update_state(fix)
        return self.solution, fix

    def __iter__(self):
        return self


#: Alias
NNLSolver = NewtonNonLinearSolver


class ShermanMorrisonSolver(NewtonNonLinearSolver):
    """
    """

    def __init__(self, system_fn: Callable,
                 initial_guess: Union[np.ndarray, Sequence],
                 system_fn_args: Any = None,
                 jcb_low_width: int = 1,
                 jcb_up_width: int = 1,
                 reduce_indices: Union[
                     dict, Sequence[dict]
                 ] = None):
        """

        :param jcb_low_width:
        :param jcb_up_width:
        :param reduce_indices:
        """
        super().__init__(system_fn, initial_guess, system_fn_args)

        #
        self.jcb_low_width = jcb_low_width

        #
        self.jcb_up_width = jcb_up_width

        #
        if reduce_indices is None:
            raise ValueError("expected a mapping or a sequence of mapping "
                             "for 'reduce_indices' argument.")

        self.reduce_indices = reduce_indices

    def solve(self, jcb_matrix: np.ndarray,
              eqs_vector: np.ndarray):
        """

        :param jcb_matrix:
        :param eqs_vector:
        :return:
        """
        up_width = self.jcb_up_width
        low_width = self.jcb_low_width
        reduce_indices = self.reduce_indices

        decompose = one_rank_decompose(jcb_matrix,
                                       reduce_indices,
                                       up_width=up_width,
                                       low_width=low_width)
        fix = sherman_morrison_solve(decompose, eqs_vector,
                                     low_width=up_width,
                                     up_width=low_width)
        return fix


#: Alias
SMNLSolver = ShermanMorrisonSolver


@jit(nopython=True)
def to_lapack_banded_storage(matrix: np.ndarray, low_width: int,
                             up_width: int) -> np.ndarray:
    """Converts a matrix given in full storage to banded storage.

    This routine is based in the source code of LINPACK,
    http://www.netlib.org/linpack/. See, for instance, the source code
    of http://www.netlib.org/linpack/sgbco.f routine.

    :param matrix:
    :param low_width:
    :param up_width:
    :return:
    """
    columns = matrix.shape[1]
    buffer_rows = 2 * low_width + up_width + 1
    band_width = low_width + up_width + 1
    matrix_banded = np.zeros((buffer_rows, columns), dtype=matrix.dtype)

    for jdx in range(1, columns + 1):
        id1 = max(1, jdx - up_width)
        id2 = min(columns, jdx + low_width)
        for idx in range(id1, id2 + 1):
            kdx = idx - jdx + band_width
            matrix_banded[kdx - 1, jdx - 1] = matrix[idx - 1, jdx - 1]

    return matrix_banded


def one_rank_decompose(matrix: np.ndarray,
                       reduce_indices: Union[
                           dict, Sequence[dict]
                       ] = None,
                       low_width: int = None,
                       up_width: int = None,
                       overwrite=False):
    """

    :param matrix:
    :param reduce_indices:
    :param low_width:
    :param up_width:
    :param overwrite:
    :return:
    """
    nst = matrix.shape[1]

    low_width = low_width or 0
    up_width = up_width or 0

    matrix_ = matrix if overwrite else matrix.copy()
    left_vec = np.zeros(nst, matrix.dtype)
    right_vec = np.zeros(nst, matrix.dtype)

    reduce_indices = list(reduce_indices)
    indexes = reduce_indices.pop()
    rows = indexes['rows']
    columns = indexes['columns']

    pairs = itertools.product(rows, columns)
    for idx, jdx in pairs:
        idx = idx if idx >= 0 else nst + idx
        jdx = jdx if jdx >= 0 else nst + jdx

        item = matrix_[idx, jdx]
        if jdx - idx > up_width or idx - jdx > low_width:
            left_vec[idx] = 1
            right_vec[jdx] = item

    pairs = itertools.product(rows, columns)
    for idx, jdx in pairs:
        matrix_[idx, jdx] -= left_vec[idx] * right_vec[jdx]

    if len(reduce_indices) > 0:
        sub_dec = one_rank_decompose(matrix_, reduce_indices,
                                     up_width, low_width, overwrite)
        return sub_dec, left_vec, right_vec, indexes

    return matrix_, left_vec, right_vec, indexes


def sherman_morrison_solve(decomposition: tuple,
                           vector: np.ndarray,
                           low_width: int = None,
                           up_width: int = None
                           ) -> object:
    """

    :param decomposition:
    :param vector:
    :param low_width:
    :param up_width:
    :return:
    """
    decomposition = list(decomposition)
    matrix, left_vec, right_vec, indexes = decomposition

    if isinstance(matrix, tuple):
        sub_dec = matrix
        s_left_vec = sherman_morrison_solve(sub_dec, left_vec, low_width,
                                            up_width)
        s_vec = sherman_morrison_solve(sub_dec, vector, low_width,
                                       up_width)
    else:
        matrix_banded = to_lapack_banded_storage(matrix, low_width=low_width,
                                                 up_width=up_width)
        matrix_lu_factor, piv, _ = _flapack.dgbtrf(matrix_banded, kl=low_width,
                                                   ku=up_width)
        s_left_vec, _ = _flapack.dgbtrs(matrix_lu_factor, kl=low_width,
                                        ku=up_width, b=left_vec, ipiv=piv)
        s_vec, _ = _flapack.dgbtrs(matrix_lu_factor, kl=low_width, ku=up_width,
                                   b=vector, ipiv=piv)
        # matrix_lu_factor, piv, _ = _flapack.dpbtrf(matrix_banded,
        # kd=low_width)
        # s_left_vec, _ = _flapack.dpbtrs(matrix_lu_factor, kd=low_width,
        #                                 b=left_vec, ipiv=piv)
        # s_vec, _ = _flapack.dpbtrs(matrix_lu_factor, kd=low_width,
        #                            b=vector, ipiv=piv)

    # rows = indexes['rows']
    columns = indexes['columns']

    mul_rl = np.sum(right_vec[(columns,)] * s_left_vec[(columns,)])
    mul_rs = np.sum(right_vec[(columns,)] * s_vec[(columns,)])
    s_ = s_vec - mul_rs * s_left_vec / (1 + mul_rl)
    # mul_rl = np.dot(right_vec, s_left_vec)
    # mul_rs = np.dot(right_vec, s_vec)
    # s_ = s_vec - np.dot(s_left_vec, mul_rs) / (1 + mul_rl)

    return s_
