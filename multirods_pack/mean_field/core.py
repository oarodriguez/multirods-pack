"""

"""

from abc import ABCMeta, abstractmethod


class ContextABC(metaclass=ABCMeta):
    """An ABC for numerical contexts"""
    __slots__ = ()

    @classmethod
    @abstractmethod
    def fields(cls):
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def defaults(cls):
        raise NotImplementedError


class SystemABC(metaclass=ABCMeta):
    """An ABC that represents a physical system."""

    __slots__ = (
        '_domain',
        '_potential',
        'interaction_strength'
    )

    @property
    @abstractmethod
    def domain_mesh(self):
        raise NotImplementedError

    @abstractmethod
    def potential(self, position):
        raise NotImplementedError

    @property
    @abstractmethod
    def context(self):
        raise NotImplementedError

    @context.setter
    @abstractmethod
    def context(self, ctx):
        raise NotImplementedError

    @abstractmethod
    def local_context(self):
        raise NotImplementedError
