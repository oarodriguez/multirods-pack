from enum import Enum
from typing import Tuple

import numpy as np
from scipy.linalg import eig, eig_banded, eigh, eigh_tridiagonal, norm
from scipy.sparse.linalg import eigs, eigsh

from multirods_pack.linalg import NNLSolver, SMNLSolver, Solvers

DENSE = 0
SPARSE = 1


class EigSolverType(Enum):
    """"""
    DENSE = 0
    SPARSE = 1


SOLVER_DEFAULTS = {
    # The order of the partition to be used for the finite-difference
    # approximation of the Gross-Pitaevskii equation.
    'partition_order': 7,

    # Maximum partition order of the finite difference.
    'max_partition_order': 12,

    # The multidimensional nonlinear solver to use.
    'nd_solver': Solvers.SHERMAN_MORRISON,

    # The target tolerance for the multidimensional Newton
    # solver for a system of nonlinear equations.
    'nd_newton_tol': 1e-2,

    # The target absolute tolerance for the multi-dimensional Newton
    # solver for a system of nonlinear equations.
    # TODO: Maybe it should be 1 / 4 nd_newton_tol
    'nd_newton_abs_tol': 1e-2,

    # Maximum number of iterations used in the multi-dimensional
    # Newton solver.
    'fd_solver_max_iter': 500,

    # Kind of algorithm used to get the eigenvalues and eigen-vectors
    # corresponding to a finite-difference approximation of the
    # Schrödinger equation.
    'fd_eig_solve_mode': EigSolverType.SPARSE
}


class SESolver1D(object):
    """Finite-difference solver for the 1D Schrödinger equation.
    The solver uses a three-point, second order centered
    finite-difference scheme. For non-periodic, finite domain with
    fixed boundary conditions, it solves a tridiagonal, non-linear
    system of equations that arises from the finite-difference
    approximation of the Gross-Pitaevskii equation.
    """

    # TODO: Need __slots__?

    __defaults__ = SOLVER_DEFAULTS

    def __init__(self, *, partition_order=None,
                 max_partition_order=None,
                 nd_solver=None,
                 nd_newton_tol=None,
                 nd_newton_abs_tol=None,
                 fd_solver_max_iter=None,
                 fd_eig_solve_mode=None):
        """

        :param partition_order:
        :param max_partition_order:
        :param nd_solver:
        :param nd_newton_tol:
        :param nd_newton_abs_tol:
        :param fd_solver_max_iter:
        :param fd_eig_solve_mode:
        """
        defaults = self.__defaults__

        self.nd_solver = nd_solver or defaults['nd_solver']

        self.nd_newton_tol = (
            nd_newton_tol or defaults['nd_newton_tol']
        )

        self.nd_newton_abs_tol = (
            nd_newton_abs_tol or defaults['nd_newton_abs_tol']
        )

        self.fd_solver_max_iter = (
            fd_solver_max_iter or defaults['fd_solver_max_iter']
        )

        self.fd_eig_solve_mode = (
            fd_eig_solve_mode or defaults['fd_eig_solve_mode']
        )

        self.max_partition_order = (
            max_partition_order or defaults['max_partition_order']
        )

        self.partition_order = (
            partition_order or defaults['partition_order']
        )

    @property
    def max_partition_size(self):
        """Gets the maximum number of elements  of the partition."""
        return 2 ** self.max_partition_order

    def unit_domain_mesh_spec(self) -> Tuple[int, float]:
        """"""
        ns = 2 ** self.partition_order
        dz = 1 / ns
        return ns, dz

    @classmethod
    def default(cls):
        """"""
        return cls(**cls.__defaults__)

    @classmethod
    def init_buffers(cls,
                     domain_mesh: np.ndarray,
                     dtype: object):
        """

        :param domain_mesh:
        :param dtype:
        :return:
        """
        n_eqs, *_ = domain_mesh.shape
        sys_matrix = np.zeros((n_eqs, n_eqs), dtype=dtype)
        return sys_matrix

    @classmethod
    def prepare_system(cls,
                       sys_matrix: np.ndarray,
                       funcs_mesh: Tuple[
                           np.ndarray,
                           np.ndarray,
                       ],
                       mesh_size: float):
        """

        :param sys_matrix:
        :param funcs_mesh:
        :param mesh_size:
        :return:
        """

        dz = mesh_size
        p_fn_mesh, q_fn_mesh = funcs_mesh

        # FIXME: These must be zero at the system walls.
        # alpha, beta = boundary_values
        ns, *_ = sys_matrix.shape

        # Lower boundary condition
        pfv = p_fn_mesh[0]
        qfv = q_fn_mesh[0]
        sys_matrix[0, 0] = -(2 - dz ** 2 * qfv)
        sys_matrix[0, 1] = 1 + dz * pfv / 2

        for n_ in range(1, ns - 1):
            pfn = p_fn_mesh[n_]
            qfn = q_fn_mesh[n_]
            sys_matrix[n_, n_ - 1] = 1 - dz * pfn / 2
            sys_matrix[n_, n_] = -(2 - dz ** 2 * qfn)
            sys_matrix[n_, n_ + 1] = 1 + dz * pfn / 2

        # Upper boundary condition
        pfv = p_fn_mesh[ns - 1]
        qfv = q_fn_mesh[ns - 1]

        sys_matrix[ns - 1, ns - 2] = 1 - dz * pfv / 2
        sys_matrix[ns - 1, ns - 1] = -(2 - dz ** 2 * qfv)

        # sys_matrix[:] /= -dz ** 2.
        # TODO: Change signs to eliminate this multiplication
        sys_matrix[:] *= -1.

    @classmethod
    def get_mesh_size(cls, domain_mesh):
        """

        :param domain_mesh:
        :return:
        """
        dz_array = np.diff(domain_mesh)
        cond = np.logical_and(dz_array, dz_array[0])
        assert np.alltrue(cond)
        return dz_array[0]

    @classmethod
    def get_sys_dtype(cls, funcs_mesh: Tuple[
        np.ndarray, np.ndarray
    ]) -> object:
        """

        :param funcs_mesh:
        :return:
        """
        p_fn_mesh, _ = funcs_mesh

        # The ``dtype`` of p_fn_mesh fixes the ``dtype`` of the
        # system matrix, as it is the only array that contains
        # complex values.
        # TODO: handle properly integer types.
        return p_fn_mesh.dtype

    @classmethod
    def solve(cls, domain_mesh: np.ndarray,
              funcs_mesh: Tuple[
                  np.ndarray,
                  np.ndarray
              ],
              strategy: EigSolverType = EigSolverType.DENSE,
              max_eig_states: int = None):
        """

        :param domain_mesh:
        :param funcs_mesh:
        :param strategy:
        :param max_eig_states:
        """
        mesh_size = cls.get_mesh_size(domain_mesh)
        sys_dtype = cls.get_sys_dtype(funcs_mesh)

        sys_matrix = cls.init_buffers(domain_mesh, sys_dtype)
        cls.prepare_system(sys_matrix, funcs_mesh, mesh_size)

        if strategy is EigSolverType.SPARSE:
            max_eig_states = max_eig_states or 10
            sys_solution = eigsh(
                sys_matrix, which='LM', sigma=0, k=max_eig_states
            )

        else:
            sys_solution = cls.solve_dense(sys_matrix)

        # Fix the correct value of the eigenvalues
        eigen_values, eigen_states = sys_solution
        eigen_values /= (mesh_size ** 2)

        return eigen_values, eigen_states

    @classmethod
    def solve_dense(cls, sys_matrix: np.ndarray):
        """

        :param sys_matrix:
        :return:
        """
        matrix_dtype = sys_matrix.dtype

        # TODO: change code...
        if matrix_dtype == np.float64:
            diagonal_ = np.diag(sys_matrix)
            upper_diagonal_ = np.diag(sys_matrix, k=1)
            return eigh_tridiagonal(diagonal_, upper_diagonal_)

        ns, *_ = sys_matrix.shape
        sys_matrix_banded = np.zeros((2, ns), dtype=matrix_dtype)
        sys_matrix_banded[0, 1:] = np.diag(sys_matrix, k=1)
        sys_matrix_banded[1, :] = np.diag(sys_matrix)
        return eig_banded(sys_matrix_banded)


class SESolverPeriodic1D(SESolver1D):
    """

    """

    @classmethod
    def init_buffers(cls,
                     domain_mesh: np.ndarray,
                     dtype: object):
        """Initializes the buffers for periodic boundary conditions.

        :param domain_mesh:
        :param dtype:
        :return:
        """
        n_eqs, *_ = domain_mesh.shape
        sys_matrix = np.zeros((n_eqs - 1, n_eqs - 1), dtype=dtype)
        return sys_matrix

    @classmethod
    def prepare_system(cls,
                       sys_matrix: np.ndarray,
                       funcs_mesh: Tuple[
                           np.ndarray,
                           np.ndarray,
                       ],
                       mesh_size: float):
        """

        :param sys_matrix:
        :param funcs_mesh:
        :param mesh_size:
        :return:
        """

        super().prepare_system(sys_matrix, funcs_mesh, mesh_size)

        dz = mesh_size
        p_fn_mesh, q_fn_mesh = funcs_mesh

        # FIXME: These must be zero at the system walls.
        # alpha, beta = boundary_values
        ns, *_ = sys_matrix.shape

        # System matrix has nonzero entries in the corners
        # due to the periodic boundary conditions.
        pfv = p_fn_mesh[0]
        sys_matrix[0, ns - 1] = -(1 - dz * pfv / 2)

        pfv = p_fn_mesh[ns - 1]
        sys_matrix[ns - 1, 0] = -(1 + dz * pfv / 2)

    @classmethod
    def solve_dense(cls, sys_matrix: np.ndarray):
        """Finds the eigen-values and eigen-vectors using strategies for
        dense matrices. Due to the corner elements in the matrix, we have
        to use the more general algorithm.

        :param sys_matrix:
        :return:
        """
        return eigh(sys_matrix)


class GPESolver1D(object):
    """

    """

    @classmethod
    def init_buffers(cls, domain_mesh: np.ndarray):
        """

        :param domain_mesh:
        :return:
        """
        ns_mesh, *_ = domain_mesh.shape
        dtype = np.float64

        # Matrix and vector have twice the elements because the Bloch
        # state has a real and an imaginary part.
        n_eqs = 2 * (ns_mesh - 2)
        sys_jcb_matrix = np.zeros((n_eqs, n_eqs), dtype=dtype)
        sys_fn_vector = np.zeros(n_eqs, dtype=dtype)

        return sys_fn_vector, sys_jcb_matrix

    @classmethod
    def prepare_system(cls,
                       state_vector: np.ndarray,
                       sys_fn_vector: np.ndarray,
                       sys_jcb_matrix: np.ndarray,
                       funcs_mesh: Tuple[
                           np.ndarray,
                           np.ndarray,
                           np.ndarray
                       ],
                       mesh_size: float = None):
        """

        :param sys_jcb_matrix:
        :param sys_fn_vector:
        :param funcs_mesh:
        :param state_vector:
        :param mesh_size:
        :return:
        """
        dz = mesh_size
        p_fn_mesh, q_fn_mesh, gn0_fn_mesh = funcs_mesh

        # FIXME: These must be zero at the system walls.
        ns, *_ = sys_jcb_matrix.shape
        ns_ = ns // 2

        # Fills the real part of the n-th equation of the GPE
        # finite-difference approximation.
        for n_ in range(0, ns_):
            # Indexes...
            nr = 2 * n_
            ni = 2 * n_ + 1

            pfn = p_fn_mesh[n_]
            qfn = q_fn_mesh[n_]
            gn0 = gn0_fn_mesh[n_]

            ukn, vkn = state_vector[nr], state_vector[ni]

            ukn_prev = state_vector[nr - 2] if n_ - 1 >= 0 else 0
            vkn_prev = state_vector[ni - 2] if n_ - 1 >= 0 else 0

            ukn_next = state_vector[nr + 2] if n_ + 1 < ns_ else 0
            vkn_next = state_vector[ni + 2] if n_ + 1 < ns_ else 0

            rfn = qfn - gn0 * (ukn ** 2 + vkn ** 2)
            duk = (ukn_next - ukn_prev) / (2 * dz)
            dvk = (vkn_next - vkn_prev) / (2 * dz)

            # Fills the real part of the n-th equation of the GPE
            # finite-difference approximation.
            sys_fn_vector[nr] = -ukn_prev + 2 * ukn - ukn_next + dz ** 2 * (
                pfn * dvk - rfn * ukn
            )
            # Fills the imaginary part of the n-th equation of the GPE
            # finite-difference approximation.
            sys_fn_vector[ni] = -vkn_prev + 2 * vkn - vkn_next + dz ** 2 * (
                -pfn * duk - rfn * vkn
            )

            # Fills the jacobian with the partial derivatives of the real
            # part of the n-th equation of the GPE finite-difference
            # approximation.
            if nr - 2 >= 0:
                sys_jcb_matrix[nr, nr - 2] = -1
                sys_jcb_matrix[nr, nr - 1] = -dz / 2 * pfn

            sys_jcb_matrix[nr, nr] = 2 + dz ** 2 * (
                2 * gn0 * ukn ** 2 - rfn
            )
            sys_jcb_matrix[nr, nr + 1] = dz ** 2 * 2 * gn0 * ukn * vkn

            if nr + 2 < ns:
                sys_jcb_matrix[nr, nr + 2] = -1
                sys_jcb_matrix[nr, nr + 3] = dz / 2 * pfn

            # Fills the jacobian with the partial derivatives of the
            # imaginary # part of the n-th equation of the GPE
            # finite-difference approximation.
            if ni - 3 >= 0:
                sys_jcb_matrix[ni, ni - 3] = dz / 2 * pfn
                sys_jcb_matrix[ni, ni - 2] = -1

            sys_jcb_matrix[ni, ni - 1] = dz ** 2 * 2 * gn0 * ukn * vkn
            sys_jcb_matrix[ni, ni] = 2 + dz ** 2 * (
                2 * gn0 * vkn ** 2 - rfn
            )

            if ni + 1 < ns:
                sys_jcb_matrix[ni, ni + 1] = -dz / 2 * pfn
                sys_jcb_matrix[ni, ni + 2] = -1

    @classmethod
    def solve(cls, domain_mesh: np.ndarray,
              funcs_mesh: Tuple[
                  np.ndarray,
                  np.ndarray,
                  np.ndarray
              ],
              initial_state: np.ndarray,
              solver: Solvers = None,
              max_iter: int = None,
              abs_tol: float = None):
        """

        :param funcs_mesh:
        :param domain_mesh:
        :param initial_state:
        :param solver:
        :param max_iter:
        :param abs_tol:
        """
        dz = mesh_size = cls.get_mesh_size(domain_mesh)
        sys_fn_vector, sys_jcb_matrix = cls.init_buffers(domain_mesh)
        state_vector = np.zeros_like(sys_fn_vector)

        max_iter = max_iter or SOLVER_DEFAULTS['fd_solver_max_iter']
        abs_tol = abs_tol or SOLVER_DEFAULTS['nd_newton_abs_tol']
        solver = solver or SOLVER_DEFAULTS['nd_solver']

        prepare_system = cls.prepare_system

        def system_fn(sln_approx):
            """Closure to find the solution of the GPE nonlinear
            equation system.

            :param sln_approx:
            :return:
            """
            prepare_system(sln_approx,
                           sys_fn_vector,
                           sys_jcb_matrix,
                           funcs_mesh,
                           mesh_size)

            return sys_fn_vector, sys_jcb_matrix

        # sys_args = funcs_mesh, sys_jcb_matrix, sys_fn_vector, mesh_size
        state_vector[::2] = initial_state.real[:-1]
        state_vector[1::2] = initial_state.imag[:-1]

        if solver is Solvers.NEWTON:
            nl_solver = NNLSolver(system_fn, state_vector)
        else:
            up_width = 3
            low_width = 3
            reduce_indices = [
                dict(rows=(0, -1), columns=(0, -1)),
                dict(rows=(0, -1), columns=(1, -2)),
                dict(rows=(1, -2), columns=(0, -1)),
                dict(rows=(1, -2), columns=(1, -2))
            ]

            nl_solver = SMNLSolver(system_fn,
                                   state_vector,
                                   jcb_low_width=low_width,
                                   jcb_up_width=up_width,
                                   reduce_indices=reduce_indices)

        sln_state = None
        for itk, (sln, fix) in enumerate(nl_solver):
            # fix_mag_ = dz1 * math.sqrt(np.sum(np.abs(fix) ** 2))
            # TODO: Correction `fix_mag` should be proportional to mesh size?
            fix_mag = norm(fix)

            # print("Iteration {:d}. Correction magnitude: {:.5G}".format(
            #         itk, fix_mag))

            if fix_mag < abs_tol:
                sln_state = sln
                break

            if itk >= max_iter:
                raise RuntimeError('tolerance not reached')

        if sln_state is None:
            raise RuntimeError('solver instance did not iterated')

        bloch_state = sln_state[::2] + 1j * sln_state[1::2]
        return bloch_state

    @classmethod
    def get_mesh_size(cls, domain_mesh: np.ndarray) -> float:
        """

        :param domain_mesh:
        :return:
        """
        dz_array = np.diff(domain_mesh)
        cond = np.logical_and(dz_array, dz_array[0])
        assert np.alltrue(cond)
        return dz_array[0]


class GPESolverPeriodic1D(GPESolver1D):
    """

    """

    @classmethod
    def init_buffers(cls, domain_mesh: np.ndarray):
        """Initializes the buffers for periodic boundary conditions.

        :param domain_mesh:
        :return:
        """
        ns_mesh, *_ = domain_mesh.shape
        dtype = np.float64

        # Matrix and vector have twice the elements because the Bloch
        # state has a real and an imaginary part.
        n_eqs = 2 * (ns_mesh - 1)
        sys_jcb_matrix = np.zeros((n_eqs, n_eqs), dtype=dtype)
        sys_fn_vector = np.zeros(n_eqs, dtype=dtype)

        return sys_fn_vector, sys_jcb_matrix

    @classmethod
    def prepare_system(cls,
                       state_vector: np.ndarray,
                       sys_fn_vector: np.ndarray,
                       sys_jcb_matrix: np.ndarray,
                       funcs_mesh: Tuple[
                           np.ndarray,
                           np.ndarray,
                           np.ndarray
                       ],
                       mesh_size: float = None):
        """

        :param state_vector:
        :param funcs_mesh:
        :param sys_jcb_matrix:
        :param sys_fn_vector:
        :param mesh_size:
        :return:
        """

        super().prepare_system(state_vector,
                               sys_fn_vector,
                               sys_jcb_matrix,
                               funcs_mesh,
                               mesh_size)

        dz = mesh_size
        p_fn_mesh, q_fn_mesh, gn0_fn_mesh = funcs_mesh

        # FIXME: These must be zero at the system walls.
        # alpha, beta = boundary_values
        n_eqs, *_ = sys_jcb_matrix.shape
        ns = n_eqs // 2
        nr_zero, ni_zero = 0, 1
        nr_last, ni_last = n_eqs - 2, n_eqs - 1

        # System matrix has nonzero entries in the corners
        # due to the periodic boundary conditions.
        pfv = pfn = p_fn_mesh[0]
        qfn = q_fn_mesh[0]
        gn0 = gn0_fn_mesh[0]

        uk_zero, vk_zero = state_vector[nr_zero], state_vector[ni_zero]
        uk_last, vk_last = state_vector[nr_last], state_vector[ni_last]

        ukn_prev, vkn_prev = uk_last, vk_last
        ukn, vkn = uk_zero, vk_zero
        ukn_next = state_vector[nr_zero + 2]
        vkn_next = state_vector[ni_zero + 2]

        rfn = qfn - gn0 * (ukn ** 2 + vkn ** 2)
        duk = (ukn_next - ukn_prev) / (2 * dz)
        dvk = (vkn_next - vkn_prev) / (2 * dz)

        # Fills the real part of the n-th equation of the GPE
        # finite-difference approximation.
        sys_fn_vector[nr_zero] = -ukn_prev + 2 * ukn - ukn_next + dz ** 2 * (
            pfn * dvk - rfn * ukn
        )
        # Fills the imaginary part of the n-th equation of the GPE
        # finite-difference approximation.
        sys_fn_vector[ni_zero] = -vkn_prev + 2 * vkn - vkn_next + dz ** 2 * (
            -pfn * duk - rfn * vkn
        )

        # Fills the upper corners of the Jacobian matrix
        sys_jcb_matrix[nr_zero, nr_last] = -1
        sys_jcb_matrix[nr_zero, ni_last] = -dz / 2 * pfv

        sys_jcb_matrix[ni_zero, nr_last] = dz / 2 * pfv
        sys_jcb_matrix[ni_zero, ni_last] = -1

        # ==================
        pfv = pfn = p_fn_mesh[ns - 1]
        qfn = q_fn_mesh[ns - 1]
        gn0 = gn0_fn_mesh[ns - 1]

        ukn_prev = state_vector[nr_last - 2]
        vkn_prev = state_vector[ni_last - 2]
        ukn, vkn = uk_last, vk_last
        ukn_next, vkn_next = uk_zero, vk_zero

        rfn = qfn - gn0 * (ukn ** 2 + vkn ** 2)
        duk = (ukn_next - ukn_prev) / (2 * dz)
        dvk = (vkn_next - vkn_prev) / (2 * dz)

        # Fills the real part of the n-th equation of the GPE
        # finite-difference approximation.
        sys_fn_vector[nr_last] = -ukn_prev + 2 * ukn - ukn_next + dz ** 2 * (
            pfn * dvk - rfn * ukn
        )
        # Fills the imaginary part of the n-th equation of the GPE
        # finite-difference approximation.
        sys_fn_vector[ni_last] = -vkn_prev + 2 * vkn - vkn_next + dz ** 2 * (
            -pfn * duk - rfn * vkn
        )

        sys_jcb_matrix[nr_last, nr_zero] = -1
        sys_jcb_matrix[nr_last, ni_zero] = dz / 2 * pfv

        sys_jcb_matrix[ni_last, nr_zero] = -dz / 2 * pfv
        sys_jcb_matrix[ni_last, ni_zero] = -1


class BdGSolver1D(object):
    """

    """

    @classmethod
    def init_buffers(cls,
                     domain_mesh: np.ndarray,
                     dtype: object):
        """

        :param domain_mesh:
        :param dtype:
        :return:
        """
        ns_mesh, *_ = domain_mesh.shape
        n_eqs = 2 * (ns_mesh - 2)
        sys_matrix = np.zeros((n_eqs, n_eqs), dtype=dtype)
        return sys_matrix

    @classmethod
    def prepare_system(cls,
                       macro_state: np.ndarray,
                       funcs_mesh: Tuple[
                           np.ndarray,
                           np.ndarray,
                           np.ndarray
                       ],
                       mesh_size: float,
                       sys_matrix: np.ndarray):
        """

        :param macro_state:
        :param sys_matrix:
        :param funcs_mesh:
        :param mesh_size:
        :return:
        """

        dz = mesh_size
        p_fns_mesh, q_fns_mesh, gn0_fn_mesh = funcs_mesh

        # FIXME: These must be zero at the system walls.
        # alpha, beta = boundary_values
        ns, *_ = sys_matrix.shape
        ns_ = ns // 2

        # Lower boundary condition
        pun, pvn = p_fns_mesh[0]
        qun, qvn = q_fns_mesh[0]
        gn0 = gn0_fn_mesh[0]
        phi_sqr = macro_state[0] ** 2
        nkv = np.absolute(macro_state[0]) ** 2

        sys_matrix[0, 0] = 2 - dz ** 2 * (qun - 2 * gn0 * nkv)
        sys_matrix[0, 1] = dz ** 2 * gn0 * phi_sqr
        sys_matrix[0, 2] = -1 - dz / 2 * pun

        sys_matrix[1, 0] = -dz ** 2 * gn0 * phi_sqr.conjugate()
        sys_matrix[1, 1] = -2 + dz ** 2 * (qvn - 2 * gn0 * nkv)
        sys_matrix[1, 3] = 1 + dz / 2 * pvn

        for n_ in range(1, ns_ - 1):
            nu = 2 * n_
            nv = 2 * n_ + 1

            pun, pvn = p_fns_mesh[n_]
            qun, qvn = q_fns_mesh[n_]
            gn0 = gn0_fn_mesh[n_]
            phi_sqr = macro_state[n_] ** 2
            nkv = np.absolute(macro_state[n_]) ** 2

            sys_matrix[nu, nu - 2] = -1 + dz / 2 * pun
            sys_matrix[nu, nu] = 2 - dz ** 2 * (qun - 2 * gn0 * nkv)
            sys_matrix[nu, nu + 1] = dz ** 2 * gn0 * phi_sqr
            sys_matrix[nu, nu + 2] = -1 - dz / 2 * pun

            sys_matrix[nv, nv - 2] = 1 - dz / 2 * pvn
            sys_matrix[nv, nv - 1] = -dz ** 2 * gn0 * phi_sqr.conjugate()
            sys_matrix[nv, nv] = -2 + dz ** 2 * (qvn - 2 * gn0 * nkv)
            sys_matrix[nv, nv + 2] = 1 + dz / 2 * pvn

        # Upper boundary condition
        pun, pvn = p_fns_mesh[ns_ - 1]
        qun, qvn = q_fns_mesh[ns_ - 1]
        gn0 = gn0_fn_mesh[ns_ - 1]
        phi_sqr = macro_state[ns_ - 1] ** 2
        nkv = np.absolute(macro_state[ns_ - 1]) ** 2

        nu = 2 * (ns_ - 1)
        nv = 2 * (ns_ - 1) + 1

        sys_matrix[nu, nu - 2] = -1 + dz / 2 * pun
        sys_matrix[nu, nu] = 2 - dz ** 2 * (qun - 2 * gn0 * nkv)
        sys_matrix[nu, nu + 1] = dz ** 2 * gn0 * phi_sqr

        sys_matrix[nv, nv - 2] = 1 - dz / 2 * pvn
        sys_matrix[nv, nv - 1] = -dz ** 2 * gn0 * phi_sqr.conjugate()
        sys_matrix[nv, nv] = -2 + dz ** 2 * (qvn - 2 * gn0 * nkv)

        # sys_matrix[:] /= -dz ** 2.
        # TODO: Change signs to eliminate this multiplication
        # sys_matrix[:] *= -1.

    @classmethod
    def solve(cls, domain_mesh: np.ndarray,
              macro_state: np.ndarray,
              funcs_mesh: Tuple[
                  np.ndarray,
                  np.ndarray,
                  np.ndarray
              ],
              strategy: EigSolverType = EigSolverType.DENSE,
              max_eig_states: int = None):
        """

        :param domain_mesh:
        :param funcs_mesh:
        :param macro_state:
        :param strategy:
        :param max_eig_states:
        :return:
        """
        mesh_size = cls.get_mesh_size(domain_mesh)

        sys_matrix = cls.init_buffers(domain_mesh, np.complex128)
        cls.prepare_system(macro_state, funcs_mesh, mesh_size, sys_matrix)

        if strategy is EigSolverType.SPARSE:
            max_eig_states = max_eig_states or 10
            sys_solution = eigs(
                sys_matrix, which='LM', sigma=0, k=max_eig_states
            )

        else:
            sys_solution = cls.solve_dense(sys_matrix)

        eig_values, eig_states = sys_solution
        eig_values[:] /= (mesh_size ** 2)

        return eig_values, eig_states

    @classmethod
    def solve_dense(cls, sys_matrix):
        """

        :param sys_matrix:
        :return:
        """
        return eig(sys_matrix)

    @classmethod
    def get_mesh_size(cls, domain_mesh):
        """

        :param domain_mesh:
        :return:
        """
        dz_array = np.diff(domain_mesh)
        cond = np.logical_and(dz_array, dz_array[0])
        assert np.alltrue(cond)
        return dz_array[0]

    @classmethod
    def get_sys_dtype(cls, funcs_mesh: Tuple[
        np.ndarray, np.ndarray
    ]) -> object:
        """

        :param funcs_mesh:
        :return:
        """
        p_fn_mesh, _ = funcs_mesh

        # The ``dtype`` of p_fn_mesh fixes the ``dtype`` of the
        # system matrix, as it is the only array that contains
        # complex values.
        # TODO: handle properly integer types.
        return p_fn_mesh.dtype


class BdGSolverPeriodic1D(BdGSolver1D):
    """"""

    @classmethod
    def init_buffers(cls,
                     domain_mesh: np.ndarray,
                     dtype: object):
        """

        :param domain_mesh:
        :param dtype:
        :return:
        """
        ns_mesh, *_ = domain_mesh.shape
        n_eqs = 2 * (ns_mesh - 1)
        sys_matrix = np.zeros((n_eqs, n_eqs), dtype=dtype)
        return sys_matrix

    @classmethod
    def prepare_system(cls,
                       macro_state: np.ndarray,
                       funcs_mesh: Tuple[
                           np.ndarray,
                           np.ndarray,
                           np.ndarray
                       ],
                       mesh_size: float,
                       sys_matrix: np.ndarray):
        """

        :param macro_state:
        :param funcs_mesh:
        :param mesh_size:
        :param sys_matrix:
        :return:
        """

        super().prepare_system(macro_state,
                               funcs_mesh,
                               mesh_size,
                               sys_matrix)

        dz = mesh_size
        p_fns_mesh, q_fns_mesh, gn0_fn_mesh = funcs_mesh

        # FIXME: These must be zero at the system walls.
        # alpha, beta = boundary_values
        ns, *_ = sys_matrix.shape
        ns_ = ns // 2

        # System matrix has nonzero entries in the corners
        # due to the periodic boundary conditions.
        # Lower boundary condition
        pun, pvn = p_fns_mesh[0]
        sys_matrix[0, -2] = -1 + dz / 2 * pun
        sys_matrix[1, -1] = 1 - dz / 2 * pvn

        # Upper boundary condition
        nu = 2 * (ns_ - 1)
        nv = 2 * (ns_ - 1) + 1
        pun, pvn = p_fns_mesh[ns_ - 1]
        sys_matrix[nu, 0] = -1 - dz / 2 * pun
        sys_matrix[nv, 1] = 1 + dz / 2 * pvn
