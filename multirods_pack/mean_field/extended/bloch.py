"""
Implements the routines and classes to calculate the Bloch states
of the Gross-Pitaevskii equation for a Bose gas in a multi-rods
structure.

Copyright (c) 2018, Omar Abel Rodríguez-López
"""

from abc import ABCMeta, abstractmethod
from typing import Any, Generator, Sequence, Tuple, Union

import numpy as np
from numba import vectorize
from scipy import integrate, optimize

from multirods_pack.mean_field.fdiff import (
    BdGSolverPeriodic1D, GPESolverPeriodic1D,
    SESolverPeriodic1D
)
from .model import (
    Boundaries, MultiRods, potential_fn
)


def ideal_eigen_states(system: MultiRods, *,
                       momentum: float,
                       max_eigen_states: int = 100):
    """

    :param system:
    :param momentum:
    :param max_eigen_states:
    :return: An instance of :class:`IdealEigenStatesFamily`.
    """
    return IdealEigenStatesFamily(system,
                                  momentum=momentum,
                                  max_eigen_states=max_eigen_states)


class BlochState(metaclass=ABCMeta):
    """Represents a Bloch state."""

    # TODO: Is `_energy` slot still necessary?
    __slots__ = (
        'system',
        'momentum',
        'bloch_band',
        '_energy',
        'wave_fn',
        '_state_data'
    )

    def __init__(self, system: MultiRods,
                 momentum: float,
                 bloch_band: int):
        """Represent a Bloch state

        :param system:
        :param momentum:
        """

        self.system = system

        self.momentum = momentum

        # FIXME: We need more than an assertion.
        assert bloch_band >= 1

        self.bloch_band = bloch_band

        # FIXME: Avoid direct instantiation.
        # self.resolve()

        self._energy = None

        self.wave_fn = None

        self._state_data = None

    @property
    def energy(self):
        """

        :return:
        """
        return self._energy

    @abstractmethod
    def from_data(self, *args, **kwargs):
        # TODO: Is this method necessary?
        raise NotImplementedError

    @abstractmethod
    def resolve(self):
        raise NotImplementedError

    @classmethod
    def _fix_state(cls, state_fn_data: np.ndarray,
                   domain_mesh: np.ndarray,
                   fixed_state_buffer: np.ndarray = None) -> np.ndarray:
        """

        :param state_fn_data:
        :param domain_mesh:
        :param fixed_state_buffer:
        :return:
        """
        nms, *_ = domain_mesh.shape
        nsw, *_ = state_fn_data.shape

        # Just checking...
        assert nms - 1 == nsw

        if fixed_state_buffer is None:
            fixed_state_buffer = np.zeros(nms, dtype=state_fn_data.dtype)
        else:
            # Avoid cast warnings.
            assert state_fn_data.dtype == fixed_state_buffer.dtype

        # Fill and adjust to periodic boundary conditions.
        fixed_state_buffer[:-1] = state_fn_data[:]
        fixed_state_buffer[-1] = state_fn_data[0]

        return fixed_state_buffer


class BlochStateSet(object):
    """Represents a set of :class:`BlochState` instances
    ordered by its lattice momentum, not necessarily belonging
    to.
    """

    __slots__ = (
        'data'
    )

    def __init__(self,
                 states: Union[
                     Generator[BlochState, Any, None],
                     Sequence[BlochState],
                 ],
                 reciprocal_origin: float = 0):
        """

        :param states:
        :param reciprocal_origin:
        """
        self.data = self.setup_data(states, reciprocal_origin)

    @property
    def momentum(self):
        return self.data['momentum']

    @property
    def states(self) -> Sequence[BlochState]:
        return self.data['states']

    @property
    def energy(self):
        return np.array([state.energy for state in self.states])

    @staticmethod
    def setup_data(states: Union[
        Generator[BlochState, Any, None],
        Sequence[BlochState]
    ], reciprocal_origin: float):
        """

        :return:
        """
        data_dtype = [
            ('momentum', np.float64),
            ('states', np.object)
        ]

        states_data = []
        for state in states:
            momentum = state.momentum + reciprocal_origin
            states_data.append((momentum, state))

        # NOTICE: Sorting in-place, by order, may result in error.
        # The problem presents when we sort using the quicksort # algorithm
        # to sort the following array in-place. Using mergesort or heapsort
        # seems more reliable, but not 100 percent sure.
        # TODO: Maybe we should sort using `argsort` and return a copy.
        states_data = np.array(states_data, data_dtype)
        states_data.sort(order='momentum', kind='mergesort')

        return states_data


class IdealState(BlochState):
    """Represents a Bloch state of the single-particle Schrödinger
    equation.
    """

    __slots__ = ()

    # TODO: Add method to create empty instances.

    @classmethod
    def from_data(cls, system: MultiRods,
                  momentum: float,
                  energy: float,
                  wave_fn_data: np.ndarray,
                  bloch_band: int):
        """

        :param system:
        :param momentum:
        :param energy:
        :param wave_fn_data:
        :param bloch_band:
        :return:
        """
        fixed_wave_fn = cls._fix_state(wave_fn_data,
                                       system.domain_mesh)

        state = cls(system, momentum, bloch_band)
        state._energy = energy
        state.wave_fn = cls.normalize(fixed_wave_fn, system.domain_mesh)

        return state

    def resolve(self):
        """Calculates the energy and the function for the periodic part
        of the state.

        :return:
        """
        raise NotImplementedError("direct estimation of specific Bloch "
                                  "state is not currently implemented. Use "
                                  "find_all method instead")

    @staticmethod
    def normalize(wave_fn: np.ndarray,
                  domain_mesh: np.ndarray,
                  copy: bool = False) -> np.ndarray:
        """

        :param wave_fn:
        :param domain_mesh:
        :param copy:
        :return:
        """
        n_const = integrate.trapz(np.absolute(wave_fn) ** 2, x=domain_mesh)
        if copy:
            wave_fn = wave_fn.copy() / np.sqrt(n_const)
        else:
            wave_fn[:] /= np.sqrt(n_const)

        return wave_fn


class IdealEigenStatesFamily(object):
    """Represents a family of Bloch states that are eigen-states of
    the one-particle Schrödinger equation. These eigen-states belong
    to the same system and share the same crystal quasi-momentum.
    """

    __slots__ = (
        'system',
        'momentum',
        'max_eigen_states',
        '_states_data'
    )

    def __init__(self, system: MultiRods, *,
                 momentum: float,
                 max_eigen_states: int = None):
        """

        :param system:
        :param momentum:
        """
        # NOTICE: The reciprocal vector of the lattice is zero.
        # TODO: Add option to choose a nonzero reciprocal lattice vector.
        self.system = system

        self.momentum = momentum

        # TODO: Raise a warning if needed (see text bellow).
        # Raise warning if max_eigen_states is greater than
        # half ``ns_mesh``, as this is the maximum number of
        # eigen-states obtained from the numerical approximations.
        ns_mesh, *_ = system.domain_mesh.shape
        self.max_eigen_states = min(max_eigen_states or 10, ns_mesh // 2)

        self._states_data = self.compute()

    @property
    def states(self) -> Sequence[IdealState]:
        return self._states_data['states']

    @property
    def energy(self):
        return self._states_data['energy']

    @property
    def chemical_potential(self):
        return self.energy

    def compute(self):
        """

        :return:
        """
        system = self.system
        momentum = self.momentum
        max_eigen_states = self.max_eigen_states

        context = system.context
        assert context.boundary_type is Boundaries.PERIODIC

        v0 = float(system.lattice_depth)
        r = float(system.lattice_ratio)
        kz = float(momentum)
        z = domain_mesh = system.domain_mesh

        # These meshes depend on the location of the origin of the
        # z coordinate respect the geometry of the potential.
        # TODO: How the mesh depends on the origin?
        p_fn_mesh = p_fn(kz, z)
        q_fn_mesh = q_fn(v0, r, kz, z)

        if kz == 0:
            funcs_mesh = p_fn_mesh.astype(np.float64), q_fn_mesh
        else:
            funcs_mesh = p_fn_mesh, q_fn_mesh

        # NOTICE: Maybe we should store the solver data...
        solver_data = SESolverPeriodic1D.solve(
            domain_mesh,
            funcs_mesh,
            strategy=context.fd_eig_solve_mode,
            max_eig_states=max_eigen_states
        )

        energy_data, amplitude = solver_data

        data_dtype = [
            ('energy', energy_data.dtype),
            ('states', np.object)
        ]

        states_data = []
        states_data_zip = enumerate(zip(energy_data, amplitude.T),
                                    start=1)
        for band_index, state_data in states_data_zip:
            energy, wave_fn_data = state_data
            state = IdealState.from_data(system,
                                         momentum,
                                         energy,
                                         wave_fn_data,
                                         band_index)
            states_data.append((energy, state))

        state_data = np.array(states_data, dtype=data_dtype)
        state_data.sort(order='energy')

        return state_data

    def try_compute(self):
        """Tries to compute the eigen-states if they have not been
        computed previously. Otherwise it skips the process.
        """
        if self._states_data is None:
            self.compute()

    def get(self, bloch_band):
        """

        :param bloch_band:
        :return:
        """
        return self.states[bloch_band - 1]


class GPEState(BlochState):
    """Represents a Bloch state type solution of the Gross-Pitaevskii
    equation.
    """

    __slots__ = (
        'chemical_potential',
    )

    def __init__(self, system: MultiRods, *,
                 momentum: float,
                 bloch_band: int = None):
        """
        FIXME: currently does nothing more than allocate attributes.

        :param system:
        :param momentum:
        """
        # FIXME: This is not optimal.
        bloch_band = bloch_band or 1
        super().__init__(system, momentum, bloch_band)

        self.chemical_potential = None

    @property
    def energy(self):
        """Calculates the internal energy per boson of the system.
        TODO: Check if we can memoize this property.

        :return: The energy per boson.
        """
        domain_mesh = self.system.domain_mesh
        gn0 = self.system.interaction_strength
        wave_fn = self.wave_fn
        mu_kz = self.chemical_potential

        int_energy = integrate.trapz(np.absolute(wave_fn) ** 4, x=domain_mesh)

        return mu_kz - gn0 / 2 * int_energy

    def from_data(self, *args, **kwargs):
        # TODO: Implement...
        pass

    def resolve(self):
        """Finds the Bloch state that satisfy the GPE equation.

        :return:
        """
        super().resolve()

    @classmethod
    def from_state(cls, state: BlochState,
                   momentum: float,
                   system: MultiRods = None,
                   bloch_band: int = None):
        """Approximates the solution of the Gross-Pitaevskii equation
        starting from an approximated state ``state``.

        NOTICE: It is not obvious which the bloch band should be.
        FIXME: Remove ``bloch_band`` if is not needed.

        :param system:
        :param momentum:
        :param bloch_band:
        :param state:
        :return:
        """
        # Grab data from state if not specified in arguments.
        system = system or state.system
        bloch_band = bloch_band or state.bloch_band

        return cls.solve_from_state(state,
                                    momentum,
                                    system,
                                    bloch_band)

    @classmethod
    def solve_from_state(cls, state: BlochState,
                         momentum: float,
                         system: MultiRods = None,
                         bloch_band: int = None):
        """
        TODO: It is not obvious which the bloch band should be.
        FIXME: Remove ``bloch_band`` if is not needed.

        :param state:
        :param system:
        :param momentum:
        :param bloch_band:
        :return:
        """
        # TODO: We need more checks related to numerical contexts.
        # TODO: Implement a more robust test.
        assert isinstance(state, (BlochState, GPEState))

        system = system or state.system
        bloch_band = bloch_band or state.bloch_band

        if isinstance(state, GPEState):
            initial_energy = state.chemical_potential
        else:
            initial_energy = state.energy

        initial_wave_fn = state.wave_fn
        domain_mesh = system.domain_mesh

        fixed_bloch_state = np.zeros(initial_wave_fn.shape,
                                     dtype=np.complex128)

        def norm_offset_fn(energy):
            """
            NOTICE: What is better: closure or method?
            :param energy:
            :return:
            """
            nonlocal fixed_bloch_state
            wave_fn_ = cls.solve_finite_diff(system,
                                             momentum,
                                             initial_wave_fn,
                                             energy)

            cls._fix_state(wave_fn_, domain_mesh, fixed_bloch_state)
            return cls.normalization_offset(fixed_bloch_state, domain_mesh)

        # TODO: Set max iterations and tolerance from variables.
        chemical_potential = optimize.newton(norm_offset_fn,
                                             initial_energy,
                                             maxiter=500,
                                             tol=1e-2)
        wave_fn = cls.solve_finite_diff(system,
                                        momentum,
                                        initial_wave_fn,
                                        chemical_potential)

        gpe_state = cls(system,
                        momentum=momentum,
                        bloch_band=bloch_band)

        cls._fix_state(wave_fn, domain_mesh, fixed_bloch_state)
        gpe_state.chemical_potential = chemical_potential
        gpe_state.wave_fn = fixed_bloch_state

        return gpe_state

    @classmethod
    def solve_finite_diff(cls,
                          system: MultiRods,
                          momentum: float,
                          initial_wave_fn: np.ndarray,
                          chemical_potential: float
                          ) -> np.ndarray:
        """
        :param chemical_potential:
        :param momentum:
        :param system:
        :param initial_wave_fn:
        :return:
        """
        v0 = float(system.lattice_depth)
        r = float(system.lattice_ratio)
        gn0 = float(system.interaction_strength)
        kz = float(momentum)
        z = domain_mesh = system.domain_mesh
        mu_kz = float(chemical_potential)

        # Avoid problems...
        assert initial_wave_fn.shape == domain_mesh.shape

        # TODO: (Same as before) How the mesh depends on the origin?
        p_fn_mesh = p_gpe_fn(kz, z)
        q_fn_mesh = q_gpe_fn(v0, r, kz, mu_kz, z)
        gn0_fn_mesh = gn0_gpe_fn(gn0, domain_mesh)

        funcs_mesh = p_fn_mesh, q_fn_mesh, gn0_fn_mesh
        return GPESolverPeriodic1D.solve(domain_mesh,
                                         funcs_mesh,
                                         initial_wave_fn)

    @classmethod
    def normalization_offset(cls, bloch_state, domain_mesh):
        """

        :param bloch_state:
        :param domain_mesh:
        :return:
        """
        nrv = integrate.trapz(np.absolute(bloch_state) ** 2, x=domain_mesh)
        return nrv - 1.


class GPEStateSet(BlochStateSet):
    """Represents a collection of :class:`GPEState` instances."""

    @property
    def states(self) -> Sequence[GPEState]:
        return self.data['states']

    @property
    def chemical_potential(self):
        return np.array([state.chemical_potential for state in self.states])


class BdGEigenMode(BlochState):
    """"""

    __slots__ = (
        'macro_state',
    )

    @classmethod
    def from_data(cls, macro_state: GPEState,
                  momentum: float,
                  energy: float,
                  mode_fn_data: np.ndarray,
                  bloch_band: int) -> 'BdGEigenMode':
        """

        :param macro_state:
        :param momentum:
        :param energy:
        :param mode_fn_data:
        :param bloch_band:
        :return:
        """
        # TODO: Add reference to complement mode.
        system = macro_state.system
        fixed_fn_data = cls._fix_state(mode_fn_data, system.domain_mesh)

        # Bare instance, no initialization.
        eig_mode = cls.__new__(cls)

        eig_mode.macro_state = macro_state
        eig_mode.system = system
        eig_mode.momentum = momentum

        # NOTICE: We only store the real part of the energy
        eig_mode._energy = float(energy)

        eig_mode.bloch_band = bloch_band
        eig_mode.wave_fn = fixed_fn_data
        eig_mode._state_data = mode_fn_data

        return eig_mode

    def resolve(self):
        """"""
        pass


class BdGEigenState(object):
    """Represents a Bloch state eigen-mode of the
    Bogoliubov-de Gennes equations.
    """

    __slots__ = (
        'macro_state',
        'momentum',
        'bloch_band',
        'energy',
        'eig_modes'
    )

    def __init__(self, macro_state: GPEState,
                 momentum: float,
                 bloch_band: int):
        """

        :param macro_state:
        :param momentum:
        :param bloch_band:
        """
        self.macro_state = macro_state

        self.momentum = momentum

        self.bloch_band = bloch_band

        # TODO: Implement direct calculation.
        self.energy = None

        self.eig_modes = None

    @classmethod
    def from_data(cls, macro_state: GPEState,
                  momentum: float,
                  energy: float,
                  modes_fn_data: Tuple[np.ndarray, np.ndarray],
                  bloch_band: int):
        """

        :param macro_state:
        :param momentum:
        :param energy:
        :param modes_fn_data:
        :param bloch_band:
        :return:
        """
        uqz_fn_data, vqz_fn_data = modes_fn_data
        uqz_mode = BdGEigenMode.from_data(macro_state, momentum,
                                          energy, uqz_fn_data, bloch_band)
        vqz_mode = BdGEigenMode.from_data(macro_state, momentum,
                                          energy, vqz_fn_data, bloch_band)

        eig_modes_data = uqz_mode.wave_fn, vqz_mode.wave_fn
        eig_modes_fn = cls.normalize(eig_modes_data,
                                     macro_state.system.domain_mesh)
        uqz_wave_fn, vqz_wave_fn = eig_modes_fn

        uqz_mode.wave_fn = uqz_wave_fn
        vqz_mode.wave_fn = vqz_wave_fn

        bdg_state = cls.__new__(cls)
        bdg_state.macro_state = macro_state
        bdg_state.momentum = momentum
        bdg_state.bloch_band = bloch_band
        bdg_state.energy = energy
        bdg_state.eig_modes = uqz_mode, vqz_mode

        return bdg_state

    @staticmethod
    def normalize(eig_modes_fn: Tuple[np.ndarray, np.ndarray],
                  domain_mesh: np.ndarray,
                  copy: bool = False):
        """

        :param eig_modes_fn:
        :param domain_mesh:
        :param copy:
        :return:
        """
        uqz_mode_fn, vqz_mode_fn = eig_modes_fn

        # Normalization constant, accordingly to the biorthogonality of the
        # eigen-modes of the Bogoliubov-de Gennes equations.
        n_const = integrate.trapz(
            np.absolute(uqz_mode_fn) ** 2 - np.absolute(vqz_mode_fn) ** 2,
            x=domain_mesh
        ) + 0j  # Need this imaginary zero to avoid NaNs.

        # Normalize the modes.
        if copy:
            uqz_mode_fn = uqz_mode_fn.copy() / np.sqrt(n_const)
            vqz_mode_fn = vqz_mode_fn.copy() / np.sqrt(n_const)
        else:
            uqz_mode_fn /= np.sqrt(n_const)
            vqz_mode_fn /= np.sqrt(n_const)

        return uqz_mode_fn, vqz_mode_fn


class BdGEigenModesFamily(object):
    """"""

    __slots__ = (
        'macro_state',
        'momentum',
        'max_eig_modes',
        '_states_data'
    )

    def __init__(self, macro_state: GPEState,
                 momentum: float,
                 max_eig_modes: int = None):
        """

        :param macro_state:
        :param momentum:
        """
        # NOTICE: The reciprocal vector of the lattice is zero.
        # TODO: Add option to choose a nonzero reciprocal lattice vector.
        self.macro_state = macro_state

        self.momentum = momentum

        # TODO: Raise a warning if needed (see text bellow).
        # Raise warning if max_eig_modes is greater than
        # half ``ns_mesh``, as this is the maximum number of
        # eigen-states obtained from the numerical approximations.
        system = macro_state.system
        ns_mesh, *_ = system.domain_mesh.shape
        self.max_eig_modes = min(max_eig_modes or 10, ns_mesh // 2)

        self._states_data = self.compute()

    @property
    def positive_states(self) -> Sequence[BdGEigenState]:
        return self._states_data[0]['states']

    @property
    def negative_states(self) -> Sequence[BdGEigenState]:
        return self._states_data[1]['states']

    @property
    def energy(self):
        return self._states_data[0]['energy']

    def compute(self):
        """

        :return:
        """
        macro_state = self.macro_state
        momentum = self.momentum
        max_eig_modes = self.max_eig_modes

        system = macro_state.system
        macro_momentum = macro_state.momentum
        macro_chem_potential = macro_state.chemical_potential
        context = system.context

        assert context.boundary_type is Boundaries.PERIODIC

        # TODO: Manage momenta different than zero.
        assert macro_momentum == 0

        v0 = float(system.lattice_depth)
        r = float(system.lattice_ratio)
        kz = float(macro_momentum)
        gn0 = float(system.interaction_strength)
        z = domain_mesh = system.domain_mesh

        mu_kz = float(macro_chem_potential)
        qz = float(momentum)

        kp, km = kz + qz, kz - qz

        pu_fn_mesh = p_fn(kp, z)
        pv_fn_mesh = p_fn(-km, z)

        # TODO: (Again :-)) How the mesh depends on the origin?
        qu_fn_mesh = q_gpe_fn(v0, r, kp, mu_kz, z)
        qv_fn_mesh = q_gpe_fn(v0, r, km, mu_kz, z)

        p_fns_mesh = np.stack((pu_fn_mesh, pv_fn_mesh), axis=-1)
        q_fns_mesh = np.stack((qu_fn_mesh, qv_fn_mesh), axis=-1)
        gn0_fn_mesh = gn0_gpe_fn(gn0, domain_mesh)

        funcs_mesh = p_fns_mesh, q_fns_mesh, gn0_fn_mesh

        solver_data = BdGSolverPeriodic1D.solve(
            domain_mesh,
            macro_state.wave_fn,
            funcs_mesh,
            strategy=context.fd_eig_solve_mode,
            max_eig_states=max_eig_modes
        )

        energy_data, modes_data = solver_data

        data_dtype = [
            ('energy', np.float64),
            ('states', np.object)
        ]

        states_data = []
        states_data_zip = enumerate(zip(energy_data, modes_data.T),
                                    start=1)

        for band_index, state_data in states_data_zip:
            energy, mode_fn_data = state_data
            uqz_mode_data = mode_fn_data[:-1:2]
            vqz_mode_data = mode_fn_data[1::2]
            modes_fn_data = uqz_mode_data, vqz_mode_data
            state = BdGEigenState.from_data(macro_state,
                                            momentum,
                                            energy,
                                            modes_fn_data,
                                            band_index)
            states_data.append((energy, state))

        states_data = np.array(states_data, dtype=data_dtype)
        states_data.sort(order='energy')
        sign = np.sign(states_data['energy']).astype(np.int64)

        return states_data[sign == 1], states_data[sign == -1]

    def get(self, bloch_band):
        """

        :param bloch_band:
        :return:
        """
        # TODO: Return both positive and negative states.
        # TODO: Fix the states for first Bloch Band
        return self.positive_states[bloch_band - 1]


def bogoliubov_eigen_modes(macro_state: GPEState,
                           momentum: float,
                           max_eig_modes: int = None):
    """

    :param macro_state:
    :param momentum:
    :param max_eig_modes:
    :return: An instance of :class:`BdGEigenModesFamily`.
    """
    return BdGEigenModesFamily(macro_state,
                               momentum,
                               max_eig_modes)


@vectorize(nopython=True)
def p_fn(lattice_momentum, position):
    # type: (...) -> np.ndarray
    """Evaluates the function

    :param lattice_momentum:
    :param position:
    :rtype: np.ndarray
    :return:
    """
    kz, z = lattice_momentum, position
    return 2 * 1j * kz


@vectorize(nopython=True)
def q_fn(lattice_depth,
         lattice_ratio,
         lattice_momentum,
         position):
    # type: (...) -> np.ndarray
    """

    :param lattice_depth:
    :param lattice_ratio:
    :param lattice_momentum:
    :param position:
    :rtype: np.ndarray
    :return:
    """
    v0, r = lattice_depth, lattice_ratio
    kz, z = lattice_momentum, position
    vz = potential_fn(v0, r, z)
    return -(vz + kz ** 2)


@vectorize(nopython=True)
def p_gpe_fn(lattice_momentum, position):
    # type: (...) -> np.ndarray
    """

    :param lattice_momentum:
    :param position:
    :return:
    """
    kz, z = lattice_momentum, position
    return 2 * kz


@vectorize(nopython=True)
def q_gpe_fn(lattice_depth,
             lattice_ratio,
             lattice_momentum,
             chemical_potential,
             position):
    # type: (...) -> np.ndarray
    """

    :param lattice_depth:
    :param lattice_ratio:
    :param lattice_momentum:
    :param chemical_potential:
    :param position:
    :rtype: np.ndarray
    :return:
    """
    v0, r = lattice_depth, lattice_ratio
    kz, mu_kz, z = lattice_momentum, chemical_potential, position
    vz = potential_fn(v0, r, z)
    return -(vz + kz ** 2 - mu_kz)


@vectorize(nopython=True)
def gn0_gpe_fn(interaction_strength, position):
    # type: (...) -> np.ndarray
    """

    :param interaction_strength:
    :param position:
    :return:
    """
    gn0, z = interaction_strength, position
    return gn0
