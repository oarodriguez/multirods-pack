"""
Implements the main routines and symbols to model a MultiRods
system in one dimension.

Copyright (c) 2018, Omar Abel Rodríguez-López
"""

import math
from enum import Enum
from fractions import Fraction
from typing import Any, Tuple, Union

import numpy as np
from numba import float64, jit, vectorize

from multirods_pack.constants import LKP
from multirods_pack.mean_field.fdiff import (
    SESolverPeriodic1D, SOLVER_DEFAULTS as SLV_DEF
)

# TODO: report possible bug!
# In particular, using an alias for ``Context`` class seems to trigger a
# bug in PyCharm. If the import has an alias, e.g.
#
#   from mean_field.fdiff import Context as BaseContext
#
# some warnings about the inspection profile detects an invalid definition
# of __slots__ in a class. Removing the alias removes the warnings.


__all__ = (
    'MultiRods',
    'SESolver',
    'potential_fn',
    'ER', 'RECOIL_ENERGY',
    'Boundaries',
    'Origin',
    'K_OPT'
)


class Boundaries(Enum):
    """"""
    PERIODIC = 0
    BOUNDED = 1
    UNBOUNDED = 2


class Origin(Enum):
    """"""
    EVEN = 0
    INTERFACE = 1


# TODO: Separate constants in a different module.
K_OPT = math.pi
ER = RECOIL_ENERGY = K_OPT ** 2

SOLVER_DEFAULTS = {
    'origin_type': Origin.INTERFACE,
    'boundary_type': Boundaries.PERIODIC,
    'supercell_size': 1
}
SOLVER_DEFAULTS.update(SLV_DEF)


class MultiRods(object):
    """"""

    __slots__ = (
        'lattice_depth',
        'lattice_ratio',
        'interaction_strength',
        'supercell_size'
    )

    def __init__(self, lattice_depth: float,
                 lattice_ratio: float,
                 interaction_strength: float,
                 supercell_size: int = 1):
        """

        :param lattice_depth:
        :param lattice_ratio:
        :param interaction_strength:
        """
        self.lattice_depth = lattice_depth

        self.lattice_ratio = lattice_ratio

        self.interaction_strength = interaction_strength

        self.supercell_size = supercell_size

    @property
    def lattice_mesh_spec(self):
        ratio = self.lattice_ratio
        return SESolver.commensurate_mesh_spec(ratio)

    def potential(self, position):
        """

        :return:
        """
        v0 = float(self.lattice_depth)
        r = float(self.lattice_ratio)
        return potential_fn(v0, r, position)

    @property
    def domain_mesh(self):
        """

        :return:
        """
        nlc = self.supercell_size
        (ns1, ns2), _ = self.lattice_mesh_spec
        ns = (ns1 + ns2) * nlc

        if self.context.origin_type is Origin.EVEN:
            z_start, z_end = -0.5 * nlc, 0.5 * nlc
        else:
            z_start, z_end = 0 * nlc, 1. * nlc

        # It is ok to store the endpoint of the domain mesh even if the
        # system has periodic boundary conditions. This requires an
        # additional element in the mesh.
        return np.linspace(z_start, z_end, num=ns + 1, endpoint=True)


class SESolver(SESolverPeriodic1D):
    """Schrödinger equation solver for MultiRods systems"""

    __defaults__ = SOLVER_DEFAULTS

    def __init__(self, origin_type=None,
                 boundary_type=None,
                 supercell_size=None, *args, **kwargs):
        """

        :param origin_type:
        :param boundary_type:
        :param supercell_size:
        :param args:
        :param kwargs:
        """

        super().__init__(*args, **kwargs)

        defaults = self.__defaults__

        self.boundary_type = boundary_type or defaults['boundary_type']

        self.origin_type = origin_type or defaults['origin_type']

        self.supercell_size = supercell_size or defaults['supercell_size']

    def uniform_mesh_spec(self, lattice_ratio: Union[float, Fraction]) -> \
        Tuple[
            int, float
        ]:
        """

        :return:
        """
        r = lattice_ratio
        ord_, ord_min = self.partition_order, 2
        ord_adj = math.ceil(ord_min - math.log2(r / (1 + r)))
        if ord_adj > ord_:
            ns_ = 2 ** int(ord_)
            # TODO: improve message.
            raise ValueError('small order, increase: {}'.format(ns_))

        ns = 2 ** int(max(ord_, ord_adj))
        dz = 1 / ns

        return ns, dz

    def commensurate_mesh_spec(self, lattice_ratio: Union[
        float, Fraction]) -> (
        Tuple[Tuple[int, int], float]
    ):
        """Calculates the number of elements for the finite-difference
        domain partition and the size of the partition element. It
        calculates two numbers: one for the region with zero potential,
        and a second one for the region with nonzero potential. Both
        numbers are calculated such that they are commensurable to an
        approximation of the multi-rods lattice ratio as a rational
        ``Fraction`` instance.

        :param lattice_ratio: The multi-rods lattice ratio.
        :return: The lattice domain partition specification. It is
            a two tuple value: the first tuple contains the number of
            partition elements for both regions. The second tuple contains
            the size of the partition elements.
        """
        max_size = self.max_partition_size

        if not isinstance(lattice_ratio, Fraction):
            lattice_ratio = self.make_fraction(lattice_ratio)

        rp = lattice_ratio.numerator
        rq = lattice_ratio.denominator
        if rq > max_size or rp > max_size:
            raise ValueError(
                "numerator or denominator value '{}' exceeds "
                "the maximum allowed value of "
                "'{}'".format(rq if rq > max_size else rp, max_size)
            )

        ns, dz = self.unit_domain_mesh_spec()
        s_ = math.log2(max(rp, rq))
        m_adj = math.floor(s_)
        nsf = 2 ** int(-m_adj) * ns

        ns_min = max(4, nsf)
        ns1, ns2 = int(ns_min * rq), int(ns_min * rp)
        dz = 1 / (ns1 + ns2)

        return (ns1, ns2), dz

    def make_fraction(self, value: Any):
        """Tries to convert the input ``value`` to a ``fractions.Fraction``
        instance compatible with the current context.

        :param value: The value to be converted.
        :return: The ``Fraction`` instance that approximate ``value``.
        """
        max_size = self.max_partition_size
        return Fraction(value).limit_denominator(max_size)

    def commensurate_mesh(self, lattice_ratio):
        """

        :return:
        """
        r = self.make_fraction(lattice_ratio)
        scs = self.supercell_size
        (ns1, ns2), _ = self.commensurate_mesh_spec(r)
        ns = (ns1 + ns2) * scs
        z_ini, z_end = -0.5 * scs, 0.5 * scs

        # It is ok to store the endpoint of the domain mesh even
        # if the system has periodic boundary conditions.
        return np.linspace(z_ini, z_end, num=ns + 1, endpoint=True)


signatures = [float64(float64, float64, float64)]


@jit(nopython=True)
def _potential_fn(lattice_depth, lattice_ratio, position):
    """Multi-rods potential function with the origin fixed at
    the interface between a barrier (left side) and empty space
    (right side).

    :param lattice_depth:
    :param lattice_ratio:
    :param position:
    :return:
    """
    v0, r, z = lattice_depth, lattice_ratio, position
    za = 1 / (1 + r) * LKP
    z_unit = z % LKP
    return v0 if za - z_unit * LKP + LKP <= LKP else 0.


@jit(nopython=True)
def _potential_even_fn(lattice_depth, lattice_ratio, position):
    """Multi-rods potential function with the origin fixed at
    the midpoint of the region with zero potential.

    :param lattice_depth:
    :param lattice_ratio:
    :param position:
    :return:
    """
    v0, r, z = lattice_depth, lattice_ratio, position
    zb = r / (1 + r) * LKP
    ns = z // LKP
    return v0 if -zb / 2 <= z - (ns + 1 / 2) < zb / 2 else 0


@vectorize(nopython=True)
def potential_fn(lattice_depth, lattice_ratio, position):
    """Vectorized function for the potential

    :param lattice_depth:
    :param lattice_ratio:
    :param position:
    :return:
    """
    return _potential_fn(lattice_depth, lattice_ratio, position)
