"""
Contains routines and symbols to compute the energy bands
spectrum of a one-dimension gas.

Copyright (c) 2018, Omar Abel Rodríguez-López
"""

import copy
from enum import Enum
from itertools import islice
from math import fabs, pi, sqrt
from typing import Iterator, Sequence, Tuple

import numpy as np
from progressbar import (
    AdaptiveETA, Bar, FormatCustomText, Percentage, ProgressBar
)

from multirods_pack.constants import K_OPT
from .bloch import (
    BdGEigenModesFamily, BdGEigenState, BlochState, BlochStateSet,
    GPEState, GPEStateSet, IdealEigenStatesFamily, ideal_eigen_states
)
from .model import MultiRods


class StateIterMode(Enum):
    """"""
    USE_PREVIOUS = 1
    NORMAL = 2


class BZScanOrigin(Enum):
    """Represents the momenta allowed as starting values to
    scan the first Brillouin zone of the energy spectrum.
    """

    # TODO: adding a RIGHT member could be useful.
    LEFT = -pi
    CENTER = 0


class GPEBlochStateType(Enum):
    """Represents the type of a Gross-Pitaevskii equation
    Bloch state. The type of the state fixes the initial
    momentum for the scan of the states in the first
    Brillouin zone.
    """

    NORMAL = 1
    SADDLE = 2


SIMode = StateIterMode
GPESType = GPEBlochStateType


class EnergyBandDriver(object):
    """Handles the computation of the Bloch states that belong to
    a particular Bloch energy band.
    """

    @classmethod
    def advance(cls, system: MultiRods,
                momentum_range: np.ndarray,
                bloch_band: int):
        """Computes the Gross-Pitaevskii Bloch states of the energy
        band specified by `bloch_band` parameter.

        :param bloch_band:
        :param system:
        :param momentum_range:
        :return:
        """
        momentum_range = np.asarray(momentum_range)

        kz_start = momentum_range[0]
        ideal_states = ideal_eigen_states(system,
                                          momentum=kz_start,
                                          max_eigen_states=bloch_band + 2)

        startup_state = ideal_states.get(bloch_band)
        return cls.advance_from_state(startup_state,
                                      momentum_range,
                                      system)

    @classmethod
    def advance_from_state(cls, state: BlochState,
                           momentum_range: np.ndarray,
                           system: MultiRods = None,
                           exclude_initial: bool = True):
        """Computes the Gross-Pitaevskii Bloch states for the energy
        specified by `start_start_up_state` :class:`BlochState` instance.

        :param system:
        :param momentum_range:
        :param state:
        :param exclude_initial:
        :return:
        """
        system = system or state.system

        if not exclude_initial:
            yield state

        momentum_range = np.asarray(momentum_range)
        current_state = state

        for idk, kz in enumerate(momentum_range):
            try:
                gpe_state = GPEState.from_state(current_state,
                                                momentum=kz,
                                                system=system)
                current_state = gpe_state
                yield gpe_state
            except (RuntimeError, RuntimeWarning):
                # TODO: Maybe we should yield a special value.
                break

    @staticmethod
    def get_scan_spec(states_per_zone: int,
                      extent: int = 0,
                      end_state: bool = True) -> Tuple[int, int]:
        """

        :param states_per_zone:
        :param extent:
        :param end_state:
        :return:
        """
        nbz = extent + 1
        upper_limit = nbz * K_OPT
        number_of_states = nbz * states_per_zone
        if end_state:
            number_of_states += 1

        return number_of_states, upper_limit

    @classmethod
    def scan_first_bz(cls, system: MultiRods,
                      bloch_band: int,
                      scan_spec: Tuple[int, int],
                      start_from: BZScanOrigin = BZScanOrigin.CENTER,
                      use_time_reversal: bool = True):
        """Computes the Gross-Pitaevskii Bloch states of the energy
        band specified by `bloch_band` parameter. The computed states
        lie within the first Brillouin zone.

        TODO: Allow to specify a starting state.
        This may be necessary if system interaction is relatively strong.

        :param system:
        :param bloch_band:
        :param scan_spec:
        :param start_from:
        :param use_time_reversal:
        :return:
        """
        if start_from not in BZScanOrigin:
            raise ValueError("{} is not allowed as a starting "
                             "origin specifier.".format(start_from))

        # TODO: Only accept even number of steps.
        kzs = initial_momentum = start_from.value
        z = system.domain_mesh

        ideal_states = ideal_eigen_states(system,
                                          momentum=initial_momentum,
                                          max_eigen_states=bloch_band + 5)

        start_up_state = ideal_states.get(bloch_band)
        # FIXME: We shouldn't have to calculate the initial state twice.
        # start_up_state = GPEState.from_state(system,
        #                                           initial_momentum,
        #                                           start_up_state)
        # Advance to the right direction
        nkz, kzf = scan_spec
        kz_range = np.linspace(0, kzf, nkz)
        state_walker = cls.advance_from_state(start_up_state,
                                              kzs + kz_range,
                                              system)

        def reversed_(state_: BlochState) -> BlochState:
            """

            :param state_:
            :return:
            """
            # TODO: Test for possible problems.
            reversed_state = copy.copy(state_)

            # Account for the phase change.
            reversed_state.momentum = -state_.momentum + 2 * kzs
            wave_fn_rev = state_.wave_fn * np.exp(- 2j * pi * z)
            reversed_state.wave_fn = wave_fn_rev.conjugate()

            return reversed_state

        # The first state is never reversed.
        skip_next = False
        current = next(state_walker)
        yield current

        dkz = np.diff(kz_range)[0]

        # TODO: Add progress bar.
        if use_time_reversal:
            for state in islice(state_walker, 0, nkz - 2):
                if state is None:
                    skip_next = True
                    break

                if cls.scan_bz_accept_test(state, current, dkz):
                    current = state
                    yield reversed_(state)
                    yield state
                else:
                    skip_next = True
                    break

            # Only return the reversed state at the end.
            if current is None or skip_next:
                return
            else:
                state = next(state_walker)
                if cls.scan_bz_accept_test(state, current, dkz):
                    yield reversed_(state)
                    yield state
                else:
                    return

        else:
            # TODO: needs tests as time-reversal symmetry is not satisfied.
            # Bloch states must satisfy time-reversal symmetry, i.e.,
            # energy symmetry. Numerically it is hard to attain this due
            # to intrinsic errors of the numerical algorithms.
            raise NotImplementedError("direct calculation of energy and "
                                      "wave function of time-reversed states "
                                      "is not currently implemented")

    @staticmethod
    def scan_bz_accept_test(next_state: BlochState,
                            prev_state: BlochState,
                            dkz: float) -> bool:
        """Tests if the `next_state` state should be accepted
        as part of the current band.

        :param next_state:
        :param prev_state:
        :param dkz:
        :return:
        """
        e_kz = prev_state.energy

        # TODO: Maybe we need a more robust test...
        delta_e = next_state.energy - e_kz
        dek = delta_e / dkz
        sgn = 1 if delta_e > 0 else -1
        e_kz += delta_e

        # FIXME: We only accept states in increasing energy bands.
        if sgn < 0:
            return False
        # FIXME: We only accept states in the current energy band.
        if fabs(dek) <= 2. * sqrt(fabs(e_kz)):
            return True
        # Other cases.
        return False


class IdealSpectrum(object):
    """Calculates the Bloch states and the energy spectrum
    in the first Brillouin zone of the reciprocal space.
    """

    __slots__ = (
        'system',
        'number_of_states',
        'upper_limit',
        'max_bands',
        'reciprocal_origin',
        'use_time_reversal',
        'states_data'
    )

    def __init__(self, system: MultiRods,
                 number_of_states: int,
                 upper_limit: float = None,
                 max_bands: int = None,
                 reciprocal_origin: float = None,
                 use_time_reversal: bool = True):
        """

        :param system:
        :param number_of_states:
        :param upper_limit:
        :param max_bands:
        :param reciprocal_origin:
        :param use_time_reversal:
        """
        self.system = system

        self.number_of_states = number_of_states

        # TODO: Could be useful to add a lower limit?
        self.upper_limit = upper_limit or pi

        self.max_bands = max_bands

        # TODO: Put that zero in some enum...
        # TODO: Implement case for nonzero reciprocal lattice vector.
        self.reciprocal_origin = reciprocal_origin or 0

        self.use_time_reversal = use_time_reversal

        self.states_data = self.compute()

    @property
    def families(self) -> Sequence[IdealEigenStatesFamily]:
        return self.states_data['states_family']

    def compute(self):
        """

        :return:
        """
        ns = self.number_of_states
        kzs, kzf = 0, self.upper_limit

        kz_range = iter(np.linspace(kzs, kzf, ns + 1))
        return self.compute_range(kz_range)

    def compute_range(self, momentum_range: Iterator) -> np.ndarray:
        """

        :param momentum_range:
        :return:
        """
        kz_range = momentum_range
        bloch_bands = self.max_bands

        data_dtype = [
            ('momentum', np.float64),
            ('states_family', np.object)
        ]

        def get_family(kz_):
            """

            :param kz_:
            :return:
            """
            return IdealEigenStatesFamily(
                self.system,
                momentum=kz_,
                max_eigen_states=np.max(bloch_bands)
            )

        kz = next(kz_range)
        states_families = [(kz, get_family(kz))]

        for kz in kz_range:
            family = get_family(kz)
            states_families.append((kz, family))

        states_families = np.array(states_families, dtype=data_dtype)
        # NOTICE: Is sorting necessary?
        order = np.argsort(states_families['momentum'])

        return states_families[order]

    def get(self, bloch_band):
        """

        :param bloch_band:
        :return:
        """
        families = iter(self.families)
        z = self.system.domain_mesh

        # NOTICE: For now, we only handle zero reciprocal lattice vector.
        kzr = 0

        def reversed_(state_: BlochState) -> BlochState:
            """

            :param state_:
            :return:
            """
            # TODO: Test for possible problems.
            rev_state = copy.copy(state_)

            # Update momentum and account for wave
            # function phase change.
            rev_state.momentum = -state_.momentum + kzr
            wave_fn_rev = state_.wave_fn * np.exp(-2j * kzr * z)
            rev_state.wave_fn = wave_fn_rev.conjugate()

            return rev_state

        family = next(families)

        band_states = [family.get(bloch_band)]

        for family in families:
            state = family.get(bloch_band)
            band_states.append(reversed_(state))
            band_states.append(state)

        return BlochStateSet(band_states, kzr)


class GPESpectrum(object):
    """Implements routines to calculate the states that form the
    band energy spectrum of the Gross-Pitaevskii equation. Both
    normal and saddle states can be calculated.
    """

    def __init__(self, system: MultiRods,
                 states_per_zone: int,
                 max_bands: int,
                 extent: int = 2,
                 half_only: bool = False):
        """

        :param system:
        :param max_bands:
        :param states_per_zone:
        :param extent:
        """
        self.system = system

        # Fix number of states such that the reciprocal
        # origin is always evaluated.
        spz = states_per_zone
        self.states_per_zone = spz + 1 if spz % 2 == 0 else spz

        self.max_bands = max_bands

        self.extent = extent

        self.half_only = half_only

    @property
    def limits(self):
        """"""
        ext = self.extent
        kzf = (2 * ext + 1) * K_OPT
        return np.array([-kzf, kzf])

    @classmethod
    def advance_range_from(cls, initial_state: BlochState,
                           momentum_range: np.ndarray,
                           system: MultiRods = None,
                           yield_initial: bool = False):
        """Computes the Gross-Pitaevskii Bloch states for the energy
        specified by `start_start_up_state` :class:`BlochState` instance.

        :param system:
        :param momentum_range:
        :param initial_state:
        :param yield_initial:
        :return:
        """
        system = system or initial_state.system

        # TODO: If system is given, check consistency with initial_state
        if yield_initial:
            yield initial_state

        momentum_range = np.asarray(momentum_range)
        current_state = initial_state

        for idk, kz in enumerate(momentum_range):
            try:
                gpe_state = GPEState.from_state(current_state,
                                                momentum=kz,
                                                system=system)
                current_state = gpe_state
                yield gpe_state
            except (RuntimeError, RuntimeWarning):
                # TODO: Maybe we should yield a special value.
                break

    def advance(self, momentum_range: np.ndarray,
                bloch_band: int):
        """Computes the Gross-Pitaevskii Bloch states of the energy
        band specified by `bloch_band` parameter.

        :param bloch_band:
        :param momentum_range:
        :return:
        """
        system = self.system
        momentum_range = np.asarray(momentum_range)

        kz_start = momentum_range[0]
        ideal_states = ideal_eigen_states(system,
                                          momentum=kz_start,
                                          max_eigen_states=bloch_band + 2)

        startup_state = ideal_states.get(bloch_band)
        return self.advance_range_from(startup_state,
                                       momentum_range,
                                       system)

    @staticmethod
    def get_scan_spec(states_per_zone: int,
                      extent: int = 0,
                      end_state: bool = True) -> Tuple[int, int]:
        """

        :param states_per_zone:
        :param extent:
        :param end_state:
        :return:
        """
        # Accept only odd number of states.
        assert states_per_zone % 2

        spz, ext = states_per_zone, extent
        shz = (spz + 1) // 2  # Number of states in half Brillouin zone
        number_of_states = shz + ext * (spz - 1)
        upper_limit = (2 * ext + 1) * K_OPT
        if not end_state:
            number_of_states -= 1

        return number_of_states, upper_limit

    @classmethod
    def scan_zones(cls, system: MultiRods,
                   states_per_zone: int,
                   extent: int,
                   bloch_band: int,
                   scan_origin: BZScanOrigin = BZScanOrigin.CENTER,
                   use_time_reversal: bool = True):
        """Computes the Gross-Pitaevskii Bloch states of the energy
        band specified by `bloch_band` parameter. The computed states
        lie within the first Brillouin zone.

        TODO: Allow to specify a starting state.
        This may be necessary if system interaction is relatively strong.

        :param states_per_zone:
        :param extent:
        :param system:
        :param bloch_band:
        :param scan_origin:
        :param use_time_reversal:
        :return:
        """
        if scan_origin not in BZScanOrigin:
            raise ValueError("{} is not allowed as a starting "
                             "origin specifier.".format(scan_origin))

        # TODO: Only accept even number of steps.
        kzs = initial_momentum = scan_origin.value
        z = system.domain_mesh

        # TODO: We could store these states for later use...
        ideal_states = ideal_eigen_states(system,
                                          momentum=initial_momentum,
                                          max_eigen_states=bloch_band + 5)

        start_up_state = ideal_states.get(bloch_band)
        # FIXME: We shouldn't have to calculate the initial state twice.
        # start_up_state = GPEState.from_state(system,
        #                                           initial_momentum,
        #                                           start_up_state)
        # Advance to the right direction
        nkz, kzf = cls.get_scan_spec(states_per_zone, extent)
        kz_range = np.linspace(0, kzf, nkz)
        state_walker = cls.advance_range_from(start_up_state,
                                              kzs + kz_range,
                                              system)

        def reversed_(state_: BlochState) -> BlochState:
            """

            :param state_:
            :return:
            """
            # TODO: Test for possible problems.
            reversed_state = copy.copy(state_)

            # Account for the phase change.
            reversed_state.momentum = -state_.momentum + 2 * kzs
            wave_fn_rev = state_.wave_fn * np.exp(- 2j * pi * z)
            reversed_state.wave_fn = wave_fn_rev.conjugate()

            return reversed_state

        # The first state is never reversed.
        skip_next = False
        current = next(state_walker)
        yield current

        dkz = np.diff(kz_range)[0]

        # TODO: Add progress bar.
        if use_time_reversal:
            for state in islice(state_walker, 0, nkz - 2):
                if state is None:
                    skip_next = True
                    break

                if cls.scan_bz_accept_test(state, current, dkz):
                    current = state
                    yield reversed_(state)
                    yield state
                else:
                    skip_next = True
                    break

            # Only return the reversed state at the end.
            if current is None or skip_next:
                return
            else:
                state = next(state_walker)
                if cls.scan_bz_accept_test(state, current, dkz):
                    yield reversed_(state)
                    yield state
                else:
                    return

        else:
            # TODO: needs tests as time-reversal symmetry is not satisfied.
            # Bloch states must satisfy time-reversal symmetry, i.e.,
            # energy symmetry. Numerically it is hard to attain this due
            # to intrinsic errors of the numerical algorithms.
            raise NotImplementedError("direct calculation of energy and "
                                      "wave function of time-reversed states "
                                      "is not currently implemented")

    @staticmethod
    def scan_bz_accept_test(next_state: BlochState,
                            prev_state: BlochState,
                            dkz: float) -> bool:
        """Tests if the `next_state` state should be accepted
        as part of the current band.

        :param next_state:
        :param prev_state:
        :param dkz:
        :return:
        """
        e_kz = prev_state.energy

        # TODO: Maybe we need a more robust test...
        delta_e = next_state.energy - e_kz
        dek = delta_e / dkz
        sgn = 1 if delta_e > 0 else -1
        e_kz += delta_e

        # FIXME: We only accept states in increasing energy bands.
        if sgn < 0:
            return False
        # FIXME: We only accept states in the current energy band.
        if fabs(dek) <= 2. * sqrt(fabs(e_kz)):
            return True
        # Other cases.
        return False

    @staticmethod
    def extend_state_set(state_set: BlochStateSet,
                         extend_to_zone: int):
        """

        :param state_set:
        :return:
        :param extend_to_zone:
        """
        assert extend_to_zone >= 0

        bands = []
        nbz = extend_to_zone
        kzs_range = 2 * K_OPT * np.arange(1, nbz + 1, 1)
        for kzs in -kzs_range[::-1]:
            band = GPEStateSet(state_set.states,
                               reciprocal_origin=kzs)
            bands.append(band)

        bands.append(state_set)

        for kzs in kzs_range:
            band = GPEStateSet(state_set.states,
                               reciprocal_origin=kzs)
            bands.append(band)

        return bands

    def scan_all_states(self, state_type: GPESType = GPESType.NORMAL):
        """

        :return:
        """
        max_bands = self.max_bands
        extent = self.extent
        half_only = self.half_only

        extent = extent + 1 if not half_only else int(extent // 2) + 1
        if state_type is GPESType.SADDLE:
            extent += 1

        for bloch_band in range(1, max_bands + 1):
            states = self.scan_band(bloch_band, state_type)
            state_set = GPEStateSet(states)

            yield self.extend_state_set(state_set, extent)

    def scan_band(self, bloch_band: int, state_type: GPESType,
                  extent: int = None):
        """

        :return:
        """
        system = self.system
        spz = self.states_per_zone
        extent = extent or self.extent

        is_odd = True if bloch_band % 2 else False
        if state_type is GPESType.NORMAL:
            start_from = BZScanOrigin.CENTER if is_odd else BZScanOrigin.LEFT
        elif state_type is GPESType.SADDLE:
            start_from = BZScanOrigin.LEFT if is_odd else BZScanOrigin.CENTER
        else:
            raise ValueError

        msg = FormatCustomText(
            'State momentum: %(momentum).3G k_OPT',
            mapping=dict(momentum=0)
        )
        widgets = [
            'Progress: ', Percentage(),
            # ' - ', msg,
            ' - ',
            # ' ', Bar(marker=AnimatedMarker(markers='🦀🐢')),
            ' ', Bar(marker='🦀'),
            ' ', AdaptiveETA()
        ]

        bar = ProgressBar(widgets=widgets, left_justify=False)

        nkz, _ = self.get_scan_spec(spz, extent)
        band_scanner = self.scan_zones(system, spz, extent,
                                       bloch_band=bloch_band,
                                       scan_origin=start_from)

        # The point is to silence every numpy warning while we display
        # the progress bar.
        defaults = np.seterr(all='warn')

        for state in bar(band_scanner, max_value=2 * nkz):
            # TODO: Think about how to report progress...
            # kz = state.momentum
            # msg.update_mapping(momentum=kz / K_OPT)
            # TODO: Check this except clause.
            try:
                yield state
            except Warning:
                # Do not print numpy warnings.
                continue

        # Restore numpy error handling.
        np.seterr(**defaults)


class BdGSpectrum(object):
    """Calculates the Bloch states and the energy spectrum
    in the first Brillouin zone of the reciprocal space.
    """

    __slots__ = (
        'macro_state',
        'number_of_states',
        'upper_limit',
        'max_bands',
        'reciprocal_origin',
        'use_time_reversal',
        'states_data'
    )

    def __init__(self, macro_state: GPEState,
                 number_of_states: int,
                 upper_limit=None,
                 max_bands: int = None,
                 reciprocal_origin: float = None,
                 use_time_reversal: bool = True):
        """

        :param macro_state:
        :param number_of_states:
        :param upper_limit:
        :param max_bands:
        :param reciprocal_origin:
        :param use_time_reversal:
        """
        self.macro_state = macro_state

        self.number_of_states = number_of_states

        # TODO: Could be useful to add a lower limit?
        self.upper_limit = upper_limit or K_OPT

        self.max_bands = max_bands

        # TODO: Put that zero in some enum...
        # TODO: Implement case for nonzero reciprocal lattice vector.
        self.reciprocal_origin = reciprocal_origin or 0

        self.use_time_reversal = use_time_reversal

        self.states_data = self.compute()

    @property
    def families(self) -> Sequence[IdealEigenStatesFamily]:
        return self.states_data['states_family']

    def compute(self):
        """

        :return:
        """

        spz = self.number_of_states
        kzs, kzf = 0, self.upper_limit

        qz_range = iter(np.linspace(kzs, kzf, spz + 1))
        return self.compute_range(qz_range)

    def compute_range(self, momentum_range: Iterator):
        """

        :param momentum_range:
        :return:
        """
        qz_range = momentum_range
        bloch_bands = self.max_bands

        data_dtype = [
            ('momentum', np.float64),
            ('states_family', np.object)
        ]

        def get_family(momentum):
            """

            :param momentum:
            :return:
            """
            return BdGEigenModesFamily(
                self.macro_state,
                momentum=momentum,
                max_eig_modes=np.max(bloch_bands)
            )

        qz = next(qz_range)
        states_families = [(qz, get_family(qz))]

        for qz in qz_range:
            family = get_family(qz)
            states_families.append((qz, family))

        states_families = np.array(states_families, dtype=data_dtype)
        # NOTICE: Is sorting necessary?
        order = np.argsort(states_families['momentum'])

        return states_families[order]

    def get(self, bloch_band):
        """

        :param bloch_band:
        :return:
        """
        families = iter(self.families)
        z = self.macro_state.system.domain_mesh

        # NOTICE: For now, we only handle zero reciprocal lattice vector.
        qzr = 0

        def reversed_(state_: BdGEigenState) -> BdGEigenState:
            """

            :param state_:
            :return:
            """
            # TODO: Test for possible problems.
            rev_state = copy.copy(state_)

            # Update momentum and account for wave
            # function phase change.
            rev_state.momentum = -state_.momentum + qzr

            # TODO: Fix wave function for eigen-modes.
            # wave_fn_rev = state_.wave_fn * np.exp(-2j * qzr * z)
            # rev_state.wave_fn = wave_fn_rev.conjugate()

            return rev_state

        family = next(families)

        band_states = [family.get(bloch_band)]

        for family in families:
            state = family.get(bloch_band)
            band_states.append(reversed_(state))
            band_states.append(state)

        return BlochStateSet(band_states, qzr)
