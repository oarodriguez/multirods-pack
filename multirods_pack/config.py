import pprint
from copy import copy
from typing import (
    Any, List, MutableMapping, Sequence, Tuple
)

import numpy as np
import yaml

from multirods_pack.utils import items_to_mesh

PRE_SUBMIT_FUNC = 'pre_submit_func'

SCHEDULE = 'schedule'
STAGES = 'stages'
SCHEDULE_STAGES = 'schedule.stages'

ID = 'id'
FUNC = 'func'
MESH_ITEMS = 'mesh_items'
MESH_PARAMS = 'mesh_params'
EXT_DATA = 'ext_data'
DISTRIBUTED = 'distributed'


class BaseConfig(MutableMapping):
    """"""

    def __init__(self, *data, **extra):
        """

        :param data:
        """
        self._data = dict()
        self.update(*data, **extra)

    @classmethod
    def from_file(cls, filename) -> 'BaseConfig':
        """

        :param filename:
        :return:
        """
        # Filename and config_data
        with open(filename, 'r') as fp:
            data = yaml.load(fp)
        return cls(data)

    @staticmethod
    def get_group(group: str,
                  config: MutableMapping[str, Any],
                  drop_gid: bool = False):
        """

        :param group:
        :param config:
        :param drop_gid:
        :return:
        """
        group_data = {}
        gid = group + '.'
        for k, v in config.items():
            if k.startswith(gid):
                k_ = k.split(gid, maxsplit=1)[-1] if drop_gid else k
                group_data[k_] = v
        return group_data

    def get_groups(self, groups: Sequence[str],
                   config: MutableMapping[str, Any],
                   drop_gid: bool = False):
        """

        :param groups:
        :param config:
        :param drop_gid:
        :return:
        """
        groups_data = {}
        for group in groups:
            data = self.get_group(group, config, drop_gid)
            groups_data.update(data)
        return groups_data

    @staticmethod
    def get_zip_field(fields: Tuple[str, str],
                      config: MutableMapping[str, Any]):
        """

        :param fields:
        :param config:
        :return:
        """
        data = []
        for f in fields:
            v = config.pop(f)
            data.append(v)
        return list(zip(*data))

    def get_schedule(self, groups=None):
        """

        :return:
        """
        self_data = dict(self)  # Shallow copy.
        stages_config = self_data.pop(SCHEDULE_STAGES, [])
        if groups is None:
            data = self_data
        else:
            data = self.get_groups(groups, self_data)

        stages = []
        for config in stages_config:
            if isinstance(config, Sequence):
                multi_stage = []
                for cfg in config:
                    stage = self.get_stage_config(data, cfg)
                    multi_stage.append(stage)
            else:
                multi_stage = [self.get_stage_config(data, config)]

            stages.append(multi_stage)

        return stages

    @classmethod
    def stage_mesh_builder(cls, data, config=None):
        """"""

        get_stage_mesh = cls.get_stage_mesh

        def mesh_builder(fixed=None):
            fixed = dict(fixed or [])  # Shallow copy.
            return get_stage_mesh(data, config, fixed=fixed)

        return mesh_builder

    @classmethod
    def get_stage_mesh(cls, data, stage_config=None, fixed=None):
        """

        :param data:
        :param stage_config:
        :param fixed:
        :return:
        """
        # Shallow copy.
        data = copy(data)
        stage_config = stage_config or {}
        mesh_params = stage_config.get(MESH_ITEMS, [])

        if not stage_config or not mesh_params:
            fixed = fixed or {}
            data_items = list(fixed.items())
            mesh_data = iter([data_items])
        else:
            mesh_items = []
            for field in mesh_params:
                if isinstance(field, (Tuple, List)):
                    field = tuple(field)
                    field_data = cls.get_zip_field(field, data)
                else:
                    field_data = data.pop(field)
                mesh_items.append((field, field_data))

            # fixed = None if fixed else data
            mesh_data = items_to_mesh(mesh_items, fixed=fixed)

        # TODO: Remove data from mesh_params
        return mesh_data

    def get_stage_config(self, data, config):
        """

        :param config:
        :param data:
        :return:
        """
        ext_data = self.get_ext_data(data, config)
        mesh_params = config.get(MESH_ITEMS, None)
        mesh_builder = self.stage_mesh_builder(data, config)

        config_ = {
            ID: config.get(ID, None),
            FUNC: config.get(FUNC, None),
            PRE_SUBMIT_FUNC: config.get(PRE_SUBMIT_FUNC, None),
            MESH_PARAMS: mesh_params,
            MESH_ITEMS: mesh_builder,
            EXT_DATA: dict(ext_data),
            DISTRIBUTED: config.get(DISTRIBUTED, True)
        }
        return config_

    def get_ext_data(self, data, config):
        """

        :param data:
        :param config:
        :return:
        """
        ext_data = config.get(EXT_DATA, [])
        if isinstance(ext_data, MutableMapping):
            return ext_data

        items = []
        for k in ext_data:
            # TODO: consider case where k could be a tuple
            if k.endswith('.*'):
                gp = self.get_group(k[:-2], data)
                items.extend(gp.items())
            else:
                # Raise error if not k
                v = data[k]
                items.append((k, v))
            ext_data = dict(items)

        return ext_data

    def describe(self):
        """Displays the configuration object in a "pretty-print"
        fashion.
        """
        # TODO: Allow customization of numpy print options.
        # TODO: Show a better message than pprint.
        p_opts = np.get_printoptions()
        np.set_printoptions(threshold=50, edgeitems=10, precision=3)
        pprint.pprint(self._data, indent=4)
        np.set_printoptions(**p_opts)

    def __setitem__(self, key, value):
        """"""
        self._data[key] = value

    def __delitem__(self, key):
        """"""
        del self._data[key]

    def __getitem__(self, key):
        """"""
        return self._data[key]

    def __len__(self):
        """"""
        return len(self._data)

    def __iter__(self):
        """"""
        return iter(self._data)
