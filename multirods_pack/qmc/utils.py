import numpy as np
from numpy import random as random

from multirods_pack.constants import LKP

MAIN_SLOT = 0
AUX_SLOT = 1


def allocate_conf(*, space_size):
    """

    :param space_size:
    :return:
    """
    return np.zeros((space_size, 2), dtype=np.float64)


def make_commensurate_conf(lattice_ratio, boson_number, boundaries):
    """

    :param lattice_ratio:
    :param boson_number:
    :return:
    """
    r, nop = lattice_ratio, boson_number
    z_min, z_max = boundaries
    box_size = z_max - z_min

    assert nop == int(box_size / LKP)

    z_a = 1 / (1 + r) * LKP
    z_drift_conf = allocate_conf(space_size=nop)
    for j in range(nop):
        z_drift_conf[j, 0] = z_min + j * LKP + 0.5 * z_a

    return z_drift_conf


def make_random_conf(boson_number, box_size=LKP):
    """

    :param boson_number:
    :param box_size:
    :return:
    """
    nop = boson_number
    z_drift_conf = allocate_conf(space_size=nop)
    for idx in range(nop):
        z_drift_conf[idx, 0] = box_size * random.rand()

    return z_drift_conf
