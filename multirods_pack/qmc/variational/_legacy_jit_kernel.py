import math
from typing import Callable

import numpy as np
from numba import jit
from numpy import random as random

from multirods_pack.qmc.base import KernelFN
from multirods_pack.qmc.kernel import MultiRodsModel

VAL_SLOT = 0
SQR_SLOT = 1


def jit_pdf_sampler_(model: MultiRodsModel) -> 'Callable':
    """Builds a JIT compiled function that executes a Quantum Monte Carlo
    integration over a many body quantum fluid.

    :param model:
    :return: The JIT compiled function that execute the Monte Carlo
        integration.
    """
    pdf = model.kernel[KernelFN.PDF_LOG]
    advance_conf = model.kernel[KernelFN.ADV_CFG]
    css = model.conf_space_size

    @jit(nopython=True, cache=True)
    def sampler_kernel(displace_params,
                       ini_sys_conf,
                       samples, transient_samples,
                       burnout_samples,
                       seed=False):
        """Samples the probability distribution function
        :meth:`distribution_function` using the Metropolis-Hastings
        algorithm.

        :param displace_params:
        :param ini_sys_conf: The buffer to store the positions and
                             drift velocities of the particles. It
                             is assumed that it contains the initial
                             configuration of the particles.
        :param samples:
        :param transient_samples:
        :param burnout_samples:
        :param seed:
        :return:
        """
        # Do not allow invalid parameters.
        assert burnout_samples >= 0
        assert transient_samples >= 1

        # Avoid `Untyped global name error` when executing the code in a
        # multiprocessing pool.
        rand = random.rand
        log = math.log

        # Feed the numba random number generator with the given seed.
        if seed:
            random.seed(seed)

        # TODO: What is better: allocate or pass a pre-allocated buffer?
        mcs = (samples + 1, css, 2)
        # ini_sys_conf = np.zeros((ss, 2, 2), dtype=np.float64)

        # TODO: store multiplicity of a given configuration.
        # When a random move is rejected, is unnecessary to store the
        # configuration data once more.
        markov_chain = np.zeros(mcs, dtype=np.float64)
        # conf_freq = np.zeros(samples + 1, dtype=np.int32)

        # sys_conf = ini_sys_conf
        sys_conf = np.zeros_like(ini_sys_conf)
        next_conf = np.zeros_like(ini_sys_conf)

        # Initial iteration.
        sys_conf[:] = ini_sys_conf[:]
        pdf_actual = pdf(sys_conf=sys_conf)

        # Initialize accepted move variable.
        accepted = 0

        # The first random moves do not correctly sample the probability
        # distribution function (in principle). They are discarded.
        for idx in range(burnout_samples):

            advance_conf(*displace_params, sys_conf, next_conf)
            pdf_proposed = pdf(sys_conf=next_conf)

            # Metropolis condition
            if pdf_proposed > 0.5 * log(rand()) + pdf_actual:
                # Accept the movement
                sys_conf[:] = next_conf[:]
                pdf_actual = pdf_proposed
                accepted += 1

        # Save first value.
        markov_chain[0, :, 0] = sys_conf[:, 0]

        # These random moves will actually sample the probability
        # function of the system. They can be used to calculate
        # some physical property.
        for idx in range(1, samples + 1):

            # Transient samples. They are not stored, but only realized
            # in order to reduce the correlation between consecutive
            # samples.
            for jdx in range(transient_samples):

                # NOTICE: Can this section be included in the model kernel?
                advance_conf(*displace_params, sys_conf, next_conf)

                pdf_proposed = pdf(sys_conf=next_conf)

                # Accept the movement.
                if pdf_proposed > 0.5 * log(rand()) + pdf_actual:
                    sys_conf[:] = next_conf[:]
                    pdf_actual = pdf_proposed
                    accepted += 1

            # Store markov chain data.
            markov_chain[idx, :, 0] = sys_conf[:, 0]

        # TODO: Should we account burnout and transient moves?
        accept_rate = (accepted / (
                burnout_samples + transient_samples * samples
        ))

        return markov_chain, accept_rate

    return sampler_kernel


@jit(nopython=True)
def eval_drift(drift_parts, drift_buffer):
    """

    :param drift_parts:
    :param drift_buffer:
    :return:
    """
    # Number of particles goes here.
    nss = drift_parts.shape[0]

    for idx in range(nss):
        #
        drift_idx = 0.

        one_body_fn_idx_lnd = drift_parts[idx, idx]

        # One body term contribution.
        drift_idx += one_body_fn_idx_lnd

        for jdx in range(nss):
            if jdx == idx:
                # Diagonal terms do not contribute.
                continue

            # Two-body term contributions.
            two_body_fn_idx_jdx_lnd = drift_parts[idx, jdx]
            drift_idx += two_body_fn_idx_jdx_lnd

        drift_buffer[idx] = drift_idx


def jit_estimator(property_fn,
                  property_shape):
    """

    :param property_fn:
    :param property_shape:
    :return:
    """

    @jit(nopython=True)
    def integrator_kernel(property_params,
                          pdf_sample,
                          tolerance,
                          keep_markov_chain=False):
        """

        :param property_params:
        :param pdf_sample:
        :param tolerance:
        :param keep_markov_chain:
        :return:
        """
        ns = pdf_sample.shape[0]

        if keep_markov_chain:
            mcs = (ns + 1,) + property_shape
        else:
            mcs = (2,) + property_shape

        property_buffer = np.zeros(property_shape, dtype=np.float64)
        estimator_chain = np.zeros(mcs, dtype=np.float64)

        for idx in range(ns):

            z_drift_conf = pdf_sample[idx]
            property_fn(*property_params, z_drift_conf, property_buffer)

            if keep_markov_chain:
                # Copy property_fn data.
                estimator_chain[idx, :] = property_buffer[:]
            else:
                # Accumulate property_fn data.
                estimator_chain[VAL_SLOT, :] += property_buffer[:]
                estimator_chain[SQR_SLOT, :] += property_buffer[:] ** 2

        if not keep_markov_chain:
            # Return averages.
            estimator_chain[VAL_SLOT, :] /= (ns + 1)
            estimator_chain[SQR_SLOT, :] /= (ns + 1)

        return estimator_chain

    return integrator_kernel
