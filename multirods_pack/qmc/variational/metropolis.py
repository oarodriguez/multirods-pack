import os
from multiprocessing import current_process
from time import time
from typing import Any, Tuple

import numpy as np

from multirods_pack.qmc.base import KernelFN
from multirods_pack.qmc.kernel import MultiRodsModel
from .jit_cbc_kernel import (
    jit_accumulator, jit_base_pdf_sampler, jit_base_sampler_accumulator
)

UNIFORM_MOVE = 0
GRADIENT_DESCENT_MOVE = 1


class CbCSampler(object):
    """Samples the probability distribution function ``pdf`` using a
    *configuration-by-configuration* sampling scheme . It uses
    random numbers generated from a uniform distribution. The
    Metropolis-Hastings algorithm is used to generate the Markov chain.
    """

    # The maximum number of steps used for the Markov chain.
    chain_samples = 25000

    # The number of steps to discard from the beginning of the Markov chain.
    burn_in_samples = 12500

    def __init__(self, model: MultiRodsModel):
        """

        :param model:
        """
        self.model = model

        self.kernel = self.jit_kernel(model)

    @classmethod
    def jit_kernel(cls, model):
        """Builds a JIT compiled function that executes a Quantum Monte
        Carlo integration over a many body quantum fluid.

        :return: The JIT compiled function that execute the Monte
                 Carlo integration.
        """
        # The function used to compile the CPU-intensive routines.
        return jit_base_pdf_sampler(model)

    def exec(self, move_delta: float,
             ini_sys_conf: np.ndarray,
             chain_samples: int = None,
             burn_in_samples: int = None,
             rng_seed: int = None):
        """Executes the Monte Carlo quadrature for the current simulation.

        :param move_delta:
        :param ini_sys_conf:
        :param chain_samples: The maximum number or random moves of the
            random walk. Optional.
        :param burn_in_samples:
        :param rng_seed:
        :return: A tuple of three values: the mean energy, the mean of the
            energy squared, and the number of accepted random moves.
        """
        chain_samples = chain_samples or self.chain_samples
        burn_in_samples = burn_in_samples or self.burn_in_samples

        # If no seed is specified, seed the numpy RNG with a process-based
        # integer plus the current time in seconds, so the integer is
        # different for every process when working with the
        # ``multiprocessing`` module.
        if rng_seed is None:
            np.random.seed(
                    current_process().pid + int(time())
            )

            # Get a seed for the numba RNG. This will be passed to the
            # compiled routines.
            rng_seed = np.random.randint(0, high=2 ** 24)

        # Seed the numpy random number generator.
        # TODO: Is this necessary?
        np.random.seed(rng_seed)

        return self.kernel(move_delta,
                           chain_samples,
                           burn_in_samples,
                           ini_sys_conf,
                           rng_seed)

    def __call__(self, *args, **kwargs):
        """Look in :meth:`exec` method for method arguments."""
        return self.exec(*args, **kwargs)


class CbCSamplerAccumulator(object):
    """Samples a probability distribution function and accumulates the
    value of a function over the configurations of the Markov chain,
    as well as its square.
    """

    # The maximum number of steps used for the Markov chain.
    chain_samples = 25000

    # The number of steps to discard from the beginning of the Markov chain.
    burn_in_samples = 12500

    def __init__(self, model: MultiRodsModel,
                 fn_id: KernelFN,
                 disable_jit: bool = False):
        """Creates a new accumulator instance.

        :param model: The :class:`MultiRodsModel` model instance. The Markov
            chain is generated from  p.d.f. of this model.
        :param fn_id: The name/identifier of the accumulated function.
            This function should be contained in the ``model.kernel``
            attribute of the model.
        :param disable_jit: If ``True``, disables the JIT-compilation of the
            accumulator routine. It does not disable the compilation of
            any routine in the kernel of the ``model``. Defaults to
            ``False``.
        """
        self.model = model

        self.fn_id = fn_id

        self.disable_jit = disable_jit

        if disable_jit:
            os.environ['NUMBA_DISABLE_JIT'] = '1'

        self.kernel = self.jit_kernel(model, fn_id)

        if disable_jit:
            os.environ['NUMBA_DISABLE_JIT'] = '0'

    @classmethod
    def jit_kernel(cls, model, fn_id):
        """

        :return:
        """
        # The function used to compile the CPU-intensive routines.
        return jit_base_sampler_accumulator(model, fn_id)

    def exec(self, move_delta: float,
             ini_sys_conf: np.ndarray,
             fn_args: Tuple[Any, ...],
             chain_samples: int = None,
             burn_in_samples: int = None,
             rng_seed: int = None):
        """Executes the Monte Carlo quadrature for the current simulation.

        :param move_delta:
        :param ini_sys_conf:
        :param fn_args:
        :param chain_samples: The maximum number or random moves of the
            random walk. Optional.
        :param burn_in_samples:
        :param rng_seed:
        :return: A tuple of three values: the mean energy, the mean of the
            energy squared, and the number of accepted random moves.
        """
        chain_samples = chain_samples or self.chain_samples
        burn_in_samples = burn_in_samples or self.burn_in_samples

        # If no seed is specified, seed the numpy RNG with a process-based
        # integer plus the current time in seconds, so the integer is
        # different for every process when working with the
        # ``multiprocessing`` module.
        if rng_seed is None:
            np.random.seed(
                    current_process().pid + int(time())
            )

            # Get a seed for the numba RNG. This will be passed to the
            # compiled routines.
            rng_seed = np.random.randint(0, high=2 ** 24)

        # Seed the numpy random number generator.
        # TODO: Is this necessary?
        np.random.seed(rng_seed)

        return self.kernel(move_delta,
                           chain_samples,
                           burn_in_samples,
                           ini_sys_conf,
                           rng_seed,
                           fn_args)

    def __call__(self, *args, **kwargs):
        """Look in :meth:`exec` method for method arguments."""
        return self.exec(*args, **kwargs)


class Estimator(object):
    """Evaluates a physical property using over a markov chain sampled
    from a probability distribution function.
    """

    # The minimum relative tolerance to attain at the end of
    # the Monte Carlo integration.
    tolerance = 1e-2

    # The function used to compile the CPU-intensive routines.
    __jit_factory_func__ = jit_accumulator

    def __init__(self, model: MultiRodsModel,
                 fn_id: KernelFN):
        """

        :param model: The wave function used as probability
            distribution function (pdf).
        :param fn_id:
        """
        self.model = model

        self.fn_id = fn_id

        self.kernel = self.jit_compile()

    def jit_compile(self):
        """Builds a JIT compiled function that executes a Quantum Monte
        Carlo integration over a many body quantum fluid.

        :return: The JIT compiled function that execute the Monte
                 Carlo integration.
        """
        return self.__jit_factory_func__(self.model, self.fn_id)

    def __call__(self, property_params,
                 markov_chain,
                 tolerance=None,
                 keep_markov_chain=False):
        """Executes the Monte Carlo quadrature for the current simulation.

        :return:
        :param property_params:
        :param markov_chain:
        :param tolerance:
        :param keep_markov_chain:
        :return: A tuple of three values: the mean energy, the mean of the
            energy squared, and the number of accepted random moves.
        """
        tolerance = tolerance or self.tolerance

        return self.kernel(markov_chain, property_params)
