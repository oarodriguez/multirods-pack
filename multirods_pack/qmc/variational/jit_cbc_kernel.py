import math
from typing import Any, Callable, Tuple

import numpy as np
from numba import jit
from numpy import random as random

from multirods_pack.qmc.base import KernelFN, recast_to_supercell
from multirods_pack.qmc.kernel import MultiRodsModel, POS_SLOT

STAT_REJECTED = 0
STAT_ACCEPTED = 1


def jit_advance_full_conf(model: MultiRodsModel):
    """

    :param model:
    :return:
    """
    r_sc_func = recast_to_supercell
    nop = model.boson_number
    z_min, z_max = model.boundaries

    # TODO: Do more tests about this subject.
    @jit(['void(f8[:,:],f8[:,:],f8)'], nopython=True, cache=True)
    def advance_full_conf(sys_conf: np.ndarray,
                          next_conf: np.ndarray,
                          move_delta: float):
        """Move the current configuration of the system. The moves are
        displacements of the original position plus a term sampled from
        a uniform distribution.

        :param sys_conf: The current configuration.
        :param next_conf: Auxiliary configuration.
        :param move_delta: The maximum amplitude of the displacement..
        """
        # Avoid `Untyped global name error` when executing the code in a
        # multiprocessing pool.
        # TODO: Make tests with the symbols imported globally
        rand = random.rand

        # TODO: Check/update location of POS_SLOT and DRIFT_SLOT
        for k_ in range(nop):
            z_k = sys_conf[k_, POS_SLOT]
            rnd_spread = (rand() - 0.5) * move_delta

            # Boundary conditions are periodic then the position
            # must be recast into the simulation box but from the
            # opposite side.
            z_k_sc = r_sc_func(z_k + rnd_spread, z_min, z_max)
            next_conf[k_, POS_SLOT] = z_k_sc

    return advance_full_conf


def jit_gaussian_advance_func(model: MultiRodsModel):
    """

    :param model:
    :return:
    """
    r_sc_func = recast_to_supercell
    nop = model.boson_number
    z_min, z_max = model.boundaries

    # TODO: Do more tests about this subject.
    @jit(['void(f8[:,:],f8[:,:],f8)'], nopython=True, cache=True)
    def advance_conf(sys_conf: np.ndarray,
                     next_conf: np.ndarray,
                     move_spread: float):
        """Move the current configuration of the system. The moves are
        displacements of the original position plus a term sampled from
        a uniform distribution.

        :param sys_conf: The current configuration.
        :param next_conf: Auxiliary configuration.
        :param move_spread: The maximum amplitude of the displacement..
        """
        # Avoid `Untyped global name error` when executing the code in a
        # multiprocessing pool.
        # TODO: Make tests with the symbols imported globally
        normal = random.normal
        var = np.sqrt(move_spread)

        # TODO: Check/update location of POS_SLOT and DRIFT_SLOT
        for k_ in range(nop):
            z_k = sys_conf[k_, POS_SLOT]
            rnd_spread = normal(0, var)

            # Boundary conditions are periodic then the position
            # must be recast into the simulation box but from the
            # opposite side.
            z_k_sc = r_sc_func(z_k + rnd_spread, z_min, z_max)
            next_conf[k_, POS_SLOT] = z_k_sc

    return advance_conf


def jit_pdf_sampler_generator(model: MultiRodsModel):
    """

    :param model:
    :return:
    """
    pdf = model.kernel[KernelFN.PDF_LOG]
    advance_full_conf = jit_advance_full_conf(model)

    @jit(nopython=True, cache=True)
    def sampler_iter(ini_sys_conf, move_delta, rng_seed):
        """Generator-based sampler of the probability distribution
        function ``pdf``. It uses random numbers generated from a
        uniform distribution. The Metropolis-Hastings algorithm is
        used to generate the Markov chain. Each time a new
        configuration is tested, the generator yields it, as well
        as the status of the test: if the move was accepted, the
        status is ``STAT_ACCEPTED``, otherwise is ``STAT_REJECTED``.

        :param move_delta: The maximum size of a random move around
            the position of the current configuration.
        :param ini_sys_conf: The buffer to store the positions and
             drift velocities of the particles. It should contain
             the initial configuration of the particles.
        :param rng_seed: The seed used to generate the random numbers.
            It is mandatory.
        :return:
        """
        # Avoid `Untyped global name error` when executing the code in a
        # multiprocessing pool.
        rand = random.rand
        log = math.log

        # Feed the numba random number generator with the given seed.
        random.seed(rng_seed)

        main_conf = np.zeros_like(ini_sys_conf)
        aux_conf = np.zeros_like(ini_sys_conf)

        # Initial configuration
        sys_conf, next_conf = main_conf, aux_conf
        sys_conf[:] = ini_sys_conf[:]

        # Initial value of the p.d.f.
        pdf_actual = pdf(sys_conf)

        while True:
            # Just keep advancing...
            advance_full_conf(sys_conf, next_conf, move_delta)
            pdf_next = pdf(next_conf)
            move_stat = STAT_REJECTED
            # Metropolis condition
            if pdf_next > 0.5 * log(rand()) + pdf_actual:
                # Accept the movement
                # main_conf[:] = aux_conf[:]
                sys_conf, next_conf = next_conf, sys_conf
                pdf_actual = pdf_next
                move_stat = STAT_ACCEPTED

            # NOTICE: Using a tuple creates a performance hit?
            yield sys_conf, move_stat

    return sampler_iter


def jit_base_pdf_sampler(model: MultiRodsModel) -> 'Callable':
    """Builds a JIT-compiled function to generate a Markov chain
    that samples the probability distribution of ``model``. The
    sampler uses a *configuration-by-configuration* scheme.

    :param model:
    :return: The JIT compiled function that execute the Monte Carlo
        integration.
    """
    nop = model.boson_number
    pdf = model.kernel[KernelFN.PDF_LOG]
    advance_full_conf = jit_advance_full_conf(model)

    @jit(nopython=True, cache=False)
    def mc_sampler(move_delta,
                   chain_samples,
                   burn_in_samples,
                   ini_sys_conf,
                   rng_seed):
        """Routine than samples the probability distribution function ``pdf``.
        It uses random numbers generated from a uniform distribution. The
        Metropolis-Hastings algorithm is used to generate the Markov chain.

        :param move_delta: The maximum size of a random move around the
            position of the current configuration.
        :param ini_sys_conf: The buffer to store the positions and drift
            velocities of the particles. It should contain the initial
            configuration of the particles.
        :param chain_samples: The maximum number of samples of the Markov chain.
        :param burn_in_samples: The number of initial samples to discard.
        :param rng_seed: The seed used to generate the random numbers. It is
            mandatory.
        :return: An array with the Markov chain configurations, and the
            acceptance rate.
        """
        # Do not allow invalid parameters.
        assert chain_samples >= 1
        assert burn_in_samples >= 0

        ncs = chain_samples
        bis = burn_in_samples

        # Avoid `Untyped global name error` when executing the code in a
        # multiprocessing pool.
        rand = random.rand
        log = math.log

        # Feed the numba random number generator with the given seed.
        random.seed(rng_seed)

        # TODO: What is better: allocate or pass a pre-allocated buffer?
        mcs = (ncs + 1, nop, 2)
        markov_chain = np.zeros(mcs, dtype=np.float64)

        main_conf = np.zeros_like(ini_sys_conf)
        aux_conf = np.zeros_like(ini_sys_conf)

        # Initial configuration
        sys_conf, next_conf = main_conf, aux_conf
        sys_conf[:] = ini_sys_conf[:]

        # Store first element of the Markov chain.
        markov_chain[0, :, 0] = sys_conf[:, 0]
        accepted = 0
        ts = bis + ncs

        # Initial value of the p.d.f.
        pdf_actual = pdf(sys_conf)

        for cj_ in range(1, ts + 1):
            # Metropolis-Hastings iterator.
            advance_full_conf(sys_conf, next_conf, move_delta)
            pdf_next = pdf(next_conf)
            move_stat = STAT_REJECTED
            # Metropolis condition
            if pdf_next > 0.5 * log(rand()) + pdf_actual:
                # Accept the movement
                # sys_conf[:] = aux_conf[:]
                sys_conf, next_conf = next_conf, sys_conf
                pdf_actual = pdf_next
                move_stat = STAT_ACCEPTED

            # TODO: Should we account only after burn-in stage?
            accepted += move_stat
            if cj_ <= bis:
                continue
            markov_chain[cj_ - bis, :, 0] = sys_conf[:, 0]

        accept_rate = accepted / ts
        return markov_chain, accept_rate

    return mc_sampler


def jit_gen_pdf_sampler(model: MultiRodsModel) -> 'Callable':
    """Builds a JIT-compiled function to generate a Markov chain
    that samples the probability distribution of ``model``. The
    sampler uses a *configuration-by-configuration* scheme.

    :param model:
    :return: The JIT compiled function that execute the Monte Carlo
        integration.
    """
    nop = model.boson_number
    sampler_iter = jit_pdf_sampler_generator(model)

    @jit(nopython=True, cache=True)
    def mc_sampler(move_delta,
                   chain_samples,
                   burn_in_samples,
                   ini_sys_conf,
                   rng_seed):
        """Routine than samples the probability distribution function ``pdf``.
        It uses random numbers generated from a uniform distribution. The
        Metropolis-Hastings algorithm is used to generate the Markov chain.

        :param move_delta: The maximum size of a random move around the
            position of the current configuration.
        :param ini_sys_conf: The buffer to store the positions and drift
            velocities of the particles. It should contain the initial
            configuration of the particles.
        :param chain_samples: The maximum number of samples of the Markov chain.
        :param burn_in_samples: The number of initial samples to discard.
        :param rng_seed: The seed used to generate the random numbers. It is
            mandatory.
        :return: An array with the Markov chain configurations, and the
            acceptance rate.
        """
        # Do not `allow invalid parameters.
        assert chain_samples >= 1
        assert burn_in_samples >= 0

        ncs = chain_samples
        bis = burn_in_samples

        # Feed the numba random number generator with the given seed.
        random.seed(rng_seed)

        # TODO: What is better: allocate or pass a pre-allocated buffer?
        mcs = (ncs + 1, nop, 2)
        markov_chain = np.zeros(mcs, dtype=np.float64)

        # Store first element of the Markov chain.
        markov_chain[0, :, 0] = ini_sys_conf[:, 0]
        accepted = 0
        ts = bis + ncs

        sampler = sampler_iter(ini_sys_conf, move_delta, rng_seed)

        for cj_, iter_values in enumerate(sampler, 1):
            # Metropolis-Hastings iterator.
            # TODO: Test use of next() function.
            sys_conf, move_stat = iter_values
            accepted += move_stat
            if cj_ <= bis:
                continue
            markov_chain[cj_ - bis, :, 0] = sys_conf[:, 0]
            # Stop next iteration if the end has been reached.
            if cj_ + 1 > ts:
                break

        # TODO: Should we account burnout and transient moves?
        accept_rate = accepted / ts
        return markov_chain, accept_rate

    return mc_sampler


def jit_base_sampler_accumulator(model: MultiRodsModel,
                                 fn_id: KernelFN):
    """Builds a JIT-compiled function that samples the probability
    distribution function of ``model``. Then, with every generated
    configuration of this Markov chain, evaluates some scalar function
    and accumulates the results.

    :param model: The :class:`MultiRodsModel` model instance. The Markov
        chain is generated from  p.d.f. of this model.
    :param fn_id: The name/identifier of the accumulated function.
        This function should be contained in the ``model.kernel``
        attribute of the model.
    :return: The JIT-compiled accumulator routine.
    """
    pdf = model.kernel[KernelFN.PDF_LOG]
    advance_conf = model.kernel[KernelFN.ADV_CFG]
    fn = model.kernel[fn_id]

    def accumulator(move_delta: float,
                    chain_samples: int,
                    burn_in_samples: int,
                    ini_sys_conf: np.ndarray,
                    rng_seed: int,
                    fn_args: Tuple[Any, ...]):
        """

        :return:
        """
        # Do not allow invalid parameters.
        assert chain_samples >= 2
        assert burn_in_samples >= 0

        ncs = chain_samples
        bis = burn_in_samples

        # Avoid `Untyped global name error` when executing the code in a
        # multiprocessing pool.
        rand = random.rand
        log = math.log

        # Feed the numba random number generator with the given seed.
        random.seed(rng_seed)

        sys_conf = np.zeros_like(ini_sys_conf)
        next_conf = np.zeros_like(ini_sys_conf)

        # Initial configuration
        sys_conf[:] = ini_sys_conf[:]
        pdf_actual = pdf(sys_conf)

        tot_fnv = 0.
        tot_fnv_sqr = 0.
        accepted = 0
        ts = bis + ncs
        if ncs:
            # Accumulate for the first element of the Markov chain.
            fnv = fn(*fn_args, sys_conf)
            tot_fnv += fnv
            tot_fnv_sqr += fnv * fnv

        for j_ in range(1, ts):
            # Metropolis-Hastings iterator.
            advance_conf(move_delta, sys_conf, next_conf)
            pdf_next = pdf(next_conf)
            move_stat = STAT_REJECTED
            # Metropolis condition
            if pdf_next > 0.5 * log(rand()) + pdf_actual:
                # Accept the movement
                sys_conf[:] = next_conf[:]
                pdf_actual = pdf_next
                move_stat = STAT_ACCEPTED

            accepted += move_stat
            if j_ < bis:
                continue
            elif j_ >= ts:
                break
            # NOTICE: Would this work with functions that return arrays?
            fnv = fn(*fn_args, sys_conf)
            tot_fnv += fnv
            tot_fnv_sqr += fnv * fnv
            accepted += move_stat

        tot_fnv /= ncs  # Mean
        tot_fnv_sqr /= ncs  # Mean

        # TODO: Should we account burn-in steps?
        accept_rate = accepted / ts if accepted else 0.
        return tot_fnv, tot_fnv_sqr, accept_rate

    return accumulator


def jit_accumulator(model: MultiRodsModel,
                    fn_id: KernelFN):
    """Builds a JIT-compiled function that evaluates some scalar function
    over the configurations of an existing Markov chain, and accumulates
    the results.

    :param model: The :class:`MultiRodsModel` model instance. The Markov
        chain is generated from the p.d.f. of this model.
    :param fn_id: The name/identifier of the accumulated function.
        This function should be contained in the ``model.kernel``
        attribute of the model.
    :return: The JIT-compiled accumulator routine.
    """
    fn = model.kernel[fn_id]

    def accumulator(markov_chain: np.ndarray,
                    fn_args: Tuple[Any, ...]):
        """

        :return:
        """
        ncs = markov_chain.shape[0]
        tot_fnv = 0.
        tot_fnv_sqr = 0.
        for j_ in range(ncs):
            sys_conf = markov_chain[j_]
            fnv = fn(*fn_args, sys_conf)
            tot_fnv += fnv
            tot_fnv_sqr += fnv * fnv

        tot_fnv /= ncs  # Mean
        tot_fnv_sqr /= ncs  # Mean

        # TODO: Should we account burn-in steps?
        return tot_fnv, tot_fnv_sqr

    return accumulator
