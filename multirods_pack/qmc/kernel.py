from abc import ABCMeta, abstractmethod
from collections import OrderedDict
from math import cos, exp, fabs, log, sin, sqrt
from typing import Callable

import numpy as np
import numpy.random as random
from numba import guvectorize, jit

from multirods_pack.constants import ER
from multirods_pack.qmc.base import KernelFN
from .base import min_distance, recast_to_supercell, sign

POS_SLOT = 0
DRIFT_SLOT = 1
NUM_SLOTS = 2


class MultiRodsModel(metaclass=ABCMeta):
    """"""

    def __init__(self, lattice_depth,
                 lattice_ratio,
                 interaction_strength,
                 boson_number,
                 supercell_size, *,
                 energy_unit=None):
        """

        :param lattice_depth:
        :param lattice_ratio:
        :param interaction_strength:
        :param boson_number:
        :param supercell_size:
        """
        self.lattice_depth = lattice_depth

        self.lattice_ratio = lattice_ratio

        self.interaction_strength = interaction_strength

        self.boson_number = boson_number

        self.supercell_size = supercell_size

        self.energy_unit = energy_unit or ER

        self._kernel = self.jit_kernel()

    @property
    def is_free_system(self):
        """"""
        v0 = self.lattice_depth
        r = self.lattice_ratio

        if v0 <= 1e-10:
            return True
        if r <= 1e-10:
            return True
        return False

    @property
    def is_ideal_system(self):
        """"""
        gn = self.interaction_strength
        if gn <= 1e-10:
            return True
        return False

    @property
    def well_width(self):
        """"""
        r = self.lattice_ratio
        return 1 / (1 + r)

    @property
    def barrier_width(self):
        """"""
        r = self.lattice_ratio
        return r / (1 + r)

    @property
    def conf_space_size(self):
        return self.boson_number

    @property
    @abstractmethod
    def boundaries(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def potential_fn(self) -> 'Callable':
        raise NotImplementedError

    @property
    @abstractmethod
    def one_body_fn(self) -> 'Callable':
        raise NotImplementedError

    @property
    @abstractmethod
    def two_body_fn(self) -> 'Callable':
        raise NotImplementedError

    @property
    @abstractmethod
    def one_body_fn_log_dz(self) -> 'Callable':
        raise NotImplementedError

    @property
    @abstractmethod
    def two_body_fn_log_dz(self) -> 'Callable':
        raise NotImplementedError

    @property
    @abstractmethod
    def one_body_fn_log_dz2(self) -> 'Callable':
        raise NotImplementedError

    @property
    @abstractmethod
    def two_body_fn_log_dz2(self) -> 'Callable':
        raise NotImplementedError

    @property
    def kernel(self):
        """"""
        return self._kernel

    def jit_kernel(self):
        """"""
        return jit_kernel(self)

    def init_sys_conf(self, random_=True):
        """"""
        nop = self.boson_number
        z_min, z_max = self.boundaries
        sys_conf = np.zeros((nop, NUM_SLOTS), dtype=np.float64)

        if random_:
            delta = z_max - z_min
            rnd_spread = delta * random.random_sample(nop)
            sys_conf[:, POS_SLOT] = z_min + rnd_spread

        return sys_conf


def jit_kernel(model: MultiRodsModel):
    # IMPORTANT: In order to avoid compiler errors (with llvmlite and/or
    # numba as ``value token required``) we have to convert these arguments to
    # floats and integers.
    """Returns JIT compiled functions used to execute a Quantum Monte Carlo
    simulation over a many body quantum fluid.

    The quantum system corresponds to an 1D interacting Bose gas constrained
    by a multi-slabs structure. The multi-slabs structure is modeled through
    a Kronig-Penney potential of magnitude :math:`V_0`, whose potential
    barriers have a width :math:`b` and a separation :math:`a` between any
    two adjacent barriers. The interaction of any two bosons is assumed to
    be a contact potential, :math:`g \delta(z_1 - z_2)`.

    The returned routines calculate an approximation for the energy of
    the ground state of a multi-slabs system using the variational method.

    :param model:
    :return: A dictionary with JIT compiled functions that calculates the
    approximation of
    the ground state energy.
    """
    # TODO: Split this function in several reusable pieces.

    nop = int(model.boson_number)
    sc_size = int(model.supercell_size)
    ue = float(model.energy_unit)

    # Simulation box boundary and size
    z_min, z_max = model.boundaries
    sc_half = 0.5 * sc_size

    # Indicates when a Bose gas is free (no external potential).
    gas_is_free = model.is_free_system

    # Indicates when a Bose gas is ideal (non-interacting particles).
    gas_is_ideal = model.is_ideal_system

    # Compiled functions
    potential_fn = model.potential_fn

    one_body_fn = model.one_body_fn
    one_body_fn_log_dz = model.one_body_fn_log_dz
    one_body_fn_log_dz2 = model.one_body_fn_log_dz2

    two_body_fn = model.two_body_fn
    two_body_fn_log_dz = model.two_body_fn_log_dz
    two_body_fn_log_dz2 = model.two_body_fn_log_dz2

    @jit(['f8(i8, f8[:,:])'], nopython=True, cache=True)
    def ith_pdf_log(i_, sys_conf):
        """Computes the variational wave function of a system of bosons in
        a specific configuration.

        :param i_:
        :param sys_conf:
        :return:
        """
        ith_pdf_log_ = 0.

        if not gas_is_free:
            # Gas subject to external potential.
            z_i = sys_conf[i_, POS_SLOT]
            obv = one_body_fn(z_i)
            ith_pdf_log_ += log(fabs(obv))

        if not gas_is_ideal:
            # Gas with interactions.
            z_i = sys_conf[i_, POS_SLOT]
            for j_ in range(i_ + 1, nop):
                z_j = sys_conf[j_, POS_SLOT]
                z_ij = min_distance(z_i, z_j, sc_size)
                tbv = two_body_fn(fabs(z_ij))

                ith_pdf_log_ += log(fabs(tbv))

        return ith_pdf_log_

    @jit(['f8(f8[:,:])'], nopython=True, cache=True)
    def pdf_log(sys_conf):
        """Computes the variational wave function of a system of bosons in
        a specific configuration.

        :param sys_conf:
        :return:
        """
        pdf_log_ = 0.

        if gas_is_free and gas_is_ideal:
            return pdf_log_

        for i_ in range(nop):
            pdf_log_ += ith_pdf_log(i_, sys_conf)

        return pdf_log_

    @jit('f8(f8[:,:])', nopython=True, cache=True)
    def pdf(sys_conf):
        """Computes the variational wave function of a system of
        bosons in a specific configuration.

        :param sys_conf:
        :return:
        """
        pdf_log_ = pdf_log(sys_conf)
        return exp(pdf_log_)

    @jit(['f8(i8,f8,f8[:,:])'], nopython=True, cache=True)
    def delta_pdf_log_kth_move(k_, z_k_delta, sys_conf):
        """Computes the change of the logarithm of the wave function
        after displacing the `k-th` particle by a distance ``z_k_delta``.

        :param k_:
        :param z_k_delta:
        :param sys_conf:
        :return:
        """
        delta_pdf_log = 0.

        if gas_is_free and gas_is_ideal:
            return delta_pdf_log

        z_k = sys_conf[k_, POS_SLOT]
        z_k_upd = z_k + z_k_delta

        if not gas_is_free:
            # Gas subject to external potential.
            obv = one_body_fn(z_k)
            obv_upd = one_body_fn(z_k_upd)
            delta_pdf_log += log(fabs(obv_upd / obv))

        if not gas_is_ideal:
            # Gas with interactions.
            for i_ in range(nop):
                if i_ == k_:
                    continue

                z_i = sys_conf[i_, POS_SLOT]
                r_ki = fabs(min_distance(z_k, z_i, sc_size))
                r_ki_upd = fabs(min_distance(z_k_upd, z_i, sc_size))

                tbv = two_body_fn(r_ki)
                tbv_upd = two_body_fn(r_ki_upd)
                delta_pdf_log += log(fabs(tbv_upd / tbv))

        return delta_pdf_log

    @jit('f8(i8,i8,f8,f8[:,:])', nopython=True, cache=True)
    def delta_ith_drift_kth_move(i_, k_, z_k_delta, sys_conf):
        """Computes the change of the i-th component of the drift
        after displacing the k-th particle by a distance ``z_k_delta``.

        :param i_:
        :param k_:
        :param z_k_delta:
        :param sys_conf:
        :return:
        """
        ith_drift = 0.

        if gas_is_free and gas_is_ideal:
            return ith_drift

        z_k = sys_conf[k_, POS_SLOT]
        z_k_upd = z_k + z_k_delta

        if i_ != k_:
            #
            if gas_is_ideal:
                return ith_drift

            z_i = sys_conf[i_, POS_SLOT]
            z_ki_upd = min_distance(z_k_upd, z_i, sc_size)
            z_ki = min_distance(z_k, z_i, sc_size)

            # TODO: Move th sign to the function.
            sgn = sign(z_ki)
            ob_fn_ldz = two_body_fn_log_dz(fabs(z_ki)) * sgn

            sgn = sign(z_ki_upd)
            ob_fn_ldz_upd = two_body_fn_log_dz(fabs(z_ki_upd)) * sgn

            ith_drift += -(ob_fn_ldz_upd - ob_fn_ldz)
            return ith_drift

        if not gas_is_free:
            #
            ob_fn_ldz = one_body_fn_log_dz(z_k)
            ob_fn_ldz_upd = one_body_fn_log_dz(z_k_upd)

            ith_drift += ob_fn_ldz_upd - ob_fn_ldz

        if not gas_is_ideal:
            # Gas with interactions.
            for j_ in range(nop):
                #
                if j_ == k_:
                    continue

                z_j = sys_conf[j_, POS_SLOT]
                z_kj = min_distance(z_k, z_j, sc_size)
                z_kj_upd = min_distance(z_k_upd, z_j, sc_size)

                sgn = sign(z_kj)
                tb_fn_ldz = two_body_fn_log_dz(fabs(z_kj)) * sgn

                sgn = sign(z_kj_upd)
                tb_fn_ldz_upd = two_body_fn_log_dz(fabs(z_kj_upd)) * sgn

                ith_drift += (tb_fn_ldz_upd - tb_fn_ldz)

        return ith_drift

    # Writing a concrete signature may raise errors with the
    # profiler in PyCharm 2018.1.
    # TODO: Do more tests about this subject.
    @jit(['void(f8,f8[:,:],f8[:,:])'], nopython=True, cache=True)
    def advance_conf(move_delta, sys_conf, next_conf):
        """Move the current configuration of the system. The moves are
        displacements of the original position plus a term sampled from
        a uniform distribution.

        :param move_delta: The maximum amplitude of the displacement..
        :param sys_conf: The current configuration.
        :param next_conf: Auxiliary configuration.
        """
        # Symbol declaration must be here :)
        rand = random.rand

        for k_ in range(nop):
            z_k = sys_conf[k_, POS_SLOT]
            rnd_ = rand() - 0.5
            z_aux_k = z_k + rnd_ * move_delta

            # Boundary conditions are periodic then the position
            # must be recast into the simulation box but from the
            # opposite side.
            z_aux_k = recast_to_supercell(z_aux_k, z_min, z_max)
            next_conf[k_, POS_SLOT] = z_aux_k

    @jit(['void(f8,f8[:,:],f8[:,:])'], nopython=True, cache=True)
    def advance_diffuse_conf(time_delta, sys_conf, next_conf):
        """Move the current configuration of the system. The moves are
        displacements of the original position plus a term sampled from
        a normal distribution, plus an additional term that takes into
        account the drift velocity of the particles.

        :param time_delta: The maximum amplitude of the displacement..
        :param sys_conf: The current configuration.
        :param next_conf: Auxiliary configuration.
        """
        # Symbol declaration must be here :)
        normal = random.normal
        variance = 2 * time_delta

        for k_ in range(nop):
            z_k = sys_conf[k_, POS_SLOT]
            v_k = sys_conf[k_, DRIFT_SLOT]
            rnd_ = normal(0, sqrt(variance))
            z_aux_k = z_k + 2 * v_k * time_delta + rnd_

            # Boundary conditions are periodic then the position
            # must be recast into the simulation box but from the
            # opposite side.
            next_conf[k_, POS_SLOT] = z_min + z_aux_k % sc_size

    @jit(['f8(i8,f8[:,:])'], nopython=True, cache=True)
    def ith_local_energy(i_, sys_conf):
        """Computes the local energy for a given configuration of the
        position of the bodies. The kinetic energy of the hamiltonian is
        computed through central finite differences.

        :param i_:
        :param sys_conf: The current configuration of the positions of the
                       particles.
        :return: The local energy.
        """

        if gas_is_free and gas_is_ideal:
            return 0.

        # Unpack the parameters.
        kin_energy = 0.
        pot_energy = 0
        drift = 0.

        if not gas_is_free:
            # Case with external potential.
            z_i = sys_conf[i_, POS_SLOT]
            ob_fn_ldz2 = one_body_fn_log_dz2(z_i)
            ob_fn_ldz = one_body_fn_log_dz(z_i)

            kin_energy += (-ob_fn_ldz2 + ob_fn_ldz ** 2)
            pot_energy += potential_fn(z_i)
            drift += ob_fn_ldz

        if not gas_is_ideal:
            # Case with interactions.
            z_i = sys_conf[i_, POS_SLOT]

            for j_ in range(nop):
                # Do not account diagonal terms.
                if j_ == i_:
                    continue

                z_j = sys_conf[j_, POS_SLOT]
                z_ij = min_distance(z_i, z_j, sc_size)
                sgn = sign(z_ij)

                tb_fn_ldz2 = two_body_fn_log_dz2(fabs(z_ij))
                tb_fn_ldz = two_body_fn_log_dz(fabs(z_ij)) * sgn

                kin_energy += (-tb_fn_ldz2 + tb_fn_ldz ** 2)
                drift += tb_fn_ldz

        # Accumulate to the drift velocity squared magnitude.
        drift_mag = drift ** 2
        kin_energy -= drift_mag
        sys_conf[i_, DRIFT_SLOT] = drift

        return kin_energy + pot_energy

    @jit(['f8(f8[:,:])'], nopython=True, cache=True)
    def local_energy(sys_conf):
        """

        :param sys_conf:
        """
        energy = 0.
        for i_ in range(nop):
            ith_energy = ith_local_energy(i_, sys_conf)
            energy += ith_energy
        return energy

    @jit(['void(f8[:,:],f8[:])'], nopython=True, cache=True)
    def local_energy_buffer(sys_conf, result):
        """Computes the local energy for a given configuration of the
        position of the bodies.

        :param sys_conf:
        :param result:
        """
        result[0] = local_energy(sys_conf)

    @jit(['f8(i8,f8,f8[:,:])'], nopython=True, cache=True)
    def ith_local_one_body_density(i_, sz, sys_conf):
        """Computes the logarithm of the local one-body density matrix
        for a given configuration of the position of the bodies and for a
        specified particle index.

        :param i_:
        :param sz:
        :param sys_conf:
        :return:
        """
        if gas_is_free and gas_is_ideal:
            return 1.

        # The local one-body density matrix is calculated as the quotient
        # of the wave function with the ``idx`` particle shifted a distance
        # ``z_rel`` from its original position divided by the wave function
        # with the particles evaluated in their original positions. To improve
        # statistics, we average over all possible particle displacements.
        local_obd = 0.

        if not gas_is_free:

            z_i = sys_conf[i_, POS_SLOT]
            z_i_sft = z_i + sz

            ob_fn = one_body_fn(z_i)
            ob_fn_sft = one_body_fn(z_i_sft)
            local_obd += (log(ob_fn_sft) - log(ob_fn))

        if not gas_is_ideal:
            # Interacting gas.
            z_i = sys_conf[i_, POS_SLOT]
            z_i_sft = z_i + sz

            for j_ in range(nop):
                #
                if i_ == j_:
                    continue

                z_j = sys_conf[j_, POS_SLOT]
                z_ij = min_distance(z_i, z_j, sc_size)
                tb_fn = two_body_fn(fabs(z_ij))

                # Shifted difference.
                z_ij = min_distance(z_i_sft, z_j, sc_size)
                tb_fn_shift = two_body_fn(fabs(z_ij))

                local_obd += (log(tb_fn_shift) - log(tb_fn))

        return exp(local_obd)

    @jit(['f8(f8,f8[:,:])'], nopython=True, cache=True)
    def local_one_body_density(sz, sys_conf):
        """Computes the logarithm of the local one-body density matrix
        for a given configuration of the position of the bodies and for a
        specified particle index.

        :param sz:
        :param sys_conf:
        :return:
        """
        obd = 0.
        for i_ in range(nop):
            obd += ith_local_one_body_density(i_, sz, sys_conf)
        return obd / nop

    @jit('void(f8,f8[:,:],f8[:])', nopython=True, cache=True)
    def local_one_body_density_buffer(sz, sys_conf, result):
        """Computes the logarithm of the local one-body density matrix
        for a given configuration of the position of the bodies and for a
        specified particle index.

        :param sz:
        :param sys_conf:
        :param result:
        :return:
        """
        result[0] = local_one_body_density(sz, sys_conf)

    @guvectorize(['(f8[:],f8[:,:],f8[:])'], '(),(a,b)->()',
                 nopython=True, cache=True, target='parallel')
    def local_one_body_density_guv(sz, sys_conf, result):
        """Computes the logarithm of the local one-body density matrix
        for a given configuration of the position of the bodies and for a
        specified particle index.

        :param sz:
        :param sys_conf:
        :param result:
        :return:
        """
        for i_ in range(sz.shape[0]):
            result[i_] = local_one_body_density(sz[i_], sys_conf)

    @jit(nopython=True, cache=True)
    def local_two_body_correlation(params_set, sys_conf, result):
        """Computes the local two-body correlation function for a given
        configuration of the position of the bodies.

        :param params_set:
        :param sys_conf:
        :param result:
        :return:
        """
        _, crl_params = params_set
        lbs, rbs = crl_params

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        for i_ in range(nop):
            z_i = sys_conf[i_, POS_SLOT] % 1
            z_i_bin = int(z_i * lbs)

            for j_ in range(i_ + 1, nop):
                z_j = sys_conf[j_, POS_SLOT] % 1
                z_j_bin = int(z_j * rbs)
                # Count to bin.
                result[z_i_bin, z_j_bin, 0] += 1

    @jit(['f8(f8,f8[:,:])'], nopython=True)
    def local_structure_factor(kz, sys_conf):
        """Computes the local two-body correlation function for a given
        configuration of the position of the bodies.

        :param kz:
        :param sys_conf:
        :return:
        """
        s_sin = 0
        s_cos = 0
        for i_ in range(nop):
            z_i = sys_conf[i_, POS_SLOT]
            s_cos += cos(kz * z_i)
            s_sin += sin(kz * z_i)

        return s_cos ** 2 + s_sin ** 2

    @guvectorize(['(f8[:],f8[:,:],f8[:])'], '(),(a,b)->()',
                 nopython=True, cache=True, target='parallel')
    def local_structure_factor_guv(kz, sys_conf, result):
        """Computes the local two-body correlation function for a given
        configuration of the position of the bodies.

        :param kz:
        :param sys_conf:
        :param result:
        :return:
        """
        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        for i_ in range(kz.shape[0]):
            result[i_] = local_structure_factor(kz[i_], sys_conf)

    return OrderedDict({
        KernelFN.PDF: pdf,
        KernelFN.PDF_LOG: pdf_log,
        KernelFN.D_PDF_LOG_K_M: delta_pdf_log_kth_move,
        KernelFN.D_I_DRIFT_K_M: delta_ith_drift_kth_move,
        KernelFN.ADV_CFG: advance_conf,
        KernelFN.ADV_DIFF_CFG: advance_diffuse_conf,
        KernelFN.L_E: local_energy,
        KernelFN.L_E_BUFFER: local_energy_buffer,
        KernelFN.L_OBD: local_one_body_density,
        KernelFN.L_OBD_BUFFER: local_one_body_density_buffer,
        KernelFN.L_OBD_GUV: local_one_body_density_guv,
        KernelFN.L_TBC: local_two_body_correlation,
        KernelFN.L_SF: local_structure_factor,
        KernelFN.L_SF_GUV: local_structure_factor_guv
    })
