""""""
from enum import Enum

from multirods_pack.config import BaseConfig, STAGES
from multirods_pack.constants import ER, LKP


class SystemParams(Enum):
    """"""
    # Fields like this are not Enum members
    __group__ = 'system'

    lattice_depth = 2.5 * ER
    lattice_ratio = 1
    interaction_strength = 0.5 * ER
    boson_number = 25
    supercell_size = LKP
    two_body_cutoff = 0.25 * LKP

    @property
    def full_name(self):
        return self.__group__ + '.' + self.name


class SamplerParams(Enum):
    """"""
    __group__ = 'sampler'

    move_amplitude = 0.05 * LKP
    initial_conf = None
    samples = 25000
    transient_samples = 10
    burnout_samples = 5000
    seed = None

    @property
    def full_name(self):
        return self.__group__ + '.' + self.name


class Config(BaseConfig):
    """"""

    params_groups = ('system', 'sampler', 'estimator')

    @property
    def system_config(self):
        """"""
        group = self.get_group('system', self, drop_gid=True)
        for field in SystemParams:
            if field.name in group.keys():
                continue
            group[field.name] = field.value
        return group

    @property
    def sampler_config(self):
        """"""
        group = self.get_group('sampler', self, drop_gid=True)
        for field in SamplerParams:
            if field.name in group.keys():
                continue
            group[field.name] = field.value
        return group

    @property
    def schedule(self):
        """"""
        return self.get_schedule(groups=self.params_groups)

    @property
    def schedule_stages(self):
        return self.schedule[STAGES]
