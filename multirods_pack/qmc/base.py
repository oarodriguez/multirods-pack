from abc import ABCMeta, abstractmethod
from collections import OrderedDict
from collections.abc import Mapping
from enum import Enum
from math import copysign, fabs
from typing import Callable

from numba import jit
from numpy import random as random


class MultiRodsSystem(object):
    """"""

    def __init__(self, lattice_depth,
                 lattice_ratio,
                 interaction_strength):
        """

        :param lattice_depth:
        :param lattice_ratio:
        :param interaction_strength:
        """
        self.lattice_depth = lattice_depth

        self.lattice_ratio = lattice_ratio

        self.interaction_strength = interaction_strength

        self._jit_funcs = self._init_jit_funcs()

        v0 = lattice_depth
        r = lattice_ratio
        gn = interaction_strength
        self._system_key = v0, r, gn

    @property
    def is_free(self):
        """"""
        v0 = self.lattice_depth
        r = self.lattice_ratio

        if v0 <= 1e-10:
            return True
        if r <= 1e-10:
            return True
        return False

    @property
    def is_ideal(self):
        """"""
        gn = self.interaction_strength
        if gn <= 1e-10:
            return True
        return False

    @property
    def well_width(self):
        """"""
        r = self.lattice_ratio
        return 1 / (1 + r)

    @property
    def barrier_width(self):
        """"""
        r = self.lattice_ratio
        return r / (1 + r)

    @property
    def potential_func(self) -> 'Callable':
        """

        :return:
        """
        return self._jit_funcs['potential_func']

    def _init_jit_funcs(self):
        """

        :return:
        """
        return jit_system_funcs(self)

    def __eq__(self, other):
        """

        :param other:
        :return:
        """
        if not isinstance(other, MultiRodsSystem):
            return False
        return self._system_key == other._system_key

    def __hash__(self):
        """

        :return:
        """
        return hash(self._system_key)


class QMCModel(metaclass=ABCMeta):
    """Represents a Quantum Monte Carlo model for a physical
    quantum system.
    """

    # TODO: Do we need this property?
    @property
    @abstractmethod
    def boundaries(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def trial_funcs(self):
        raise NotImplementedError


class KernelFN(Enum):
    """`Enum` with the most common function names (possibly)
    available in a Quantum Monte Carlo Kernel.
    """

    PDF = 'pdf'
    I_PDF_LOG = 'ith_pdf_log'
    PDF_LOG = 'pdf_log'
    D_PDF_LOG_K_M = 'delta_pdf_log_kth_move'
    D_I_DRIFT_K_M = 'delta_ith_drift_kth_move'

    ADV_CFG = 'advance_conf'
    ADV_DIFF_CFG = 'advance_diffuse_conf'

    I_L_E = 'ith_local_energy'
    L_E = 'local_energy'
    L_E_BUFFER = 'local_energy_to_buffer'

    I_L_OBD = 'ith_local_one_body_density'
    L_OBD = 'local_one_body_density'
    L_OBD_BUFFER = 'local_one_body_density_to_buffer'
    L_OBD_GUV = 'local_one_body_density_guv'

    L_SF = 'local_structure_factor'
    L_SF_GUV = 'local_structure_factor_guv'

    L_TBC = 'local_two_body_correlation'


class DictAttribute(object):
    """An object to save the data to construct a kernel item from a
    factory function.
    """

    # TODO: Add __repr__

    def __init__(self, name, value):
        """

        :param name:
        :param value:
        """
        self.name = name
        self.value = value
        self.__factory__ = False

    def __get__(self, obj: 'QMCDict', type_=None):
        """

        :param obj:
        :param type_:
        :return:
        """
        if obj is None:
            return self
        return obj[self.name]

    def __set__(self, obj, value):
        """Make read-only data descriptor."""
        raise AttributeError


def dict_attribute(obj):
    """Creates a decorator to set a function as the value of a
    :class:`DictAttribute` instance.

    :param obj:
    :return:
    """
    # TODO: Improve decorator
    if isinstance(obj, Callable):
        # Take the name as the callable ``__name__`` attribute.
        name = obj.__name__
        attr = DictAttribute(name, obj)
        attr.__factory__ = True
        return attr

    def decorator(factory_func):
        """Creates a new :class:`DictAttribute` instance, and sets its
        attribute :attr:`DictAttribute.__factory__` to ``True``.

        :param factory_func:
        :return:
        """
        attr = DictAttribute(obj, factory_func)
        attr.__factory__ = True
        return attr

    return decorator


class QMCDictMeta(ABCMeta):
    """Metaclass for the :class:`QMCDict` type. It picks every attribute
    that is a :class:`DictAttribute` instance and stores them in a special
    class attribute.
    """

    def __new__(mcs, name, bases, namespace):
        """

        :param name:
        :param bases:
        :param namespace:
        :return:
        """
        cls = super().__new__(mcs, name, bases, namespace)

        items, factories = [], []
        for name_ in dir(cls):
            obj = getattr(cls, name_)
            if isinstance(obj, DictAttribute):
                if obj.__factory__:
                    factories.append(obj)
                else:
                    items.append(obj)

        cls.__factories__ = tuple(factories)
        cls.__items__ = tuple(items)

        return cls


class QMCDict(Mapping, metaclass=QMCDictMeta):
    """Base implementation of a Quantum Monte Carlo execution mapping.

    QMCDict instances are mapping-like objects which store other
    objects, possibly optimized, numba jit-compiled functions, which
    serve to estimate the main properties of a quantum system, like
    the wave function and the energy, for instance.
    """
    __slots__ = ('_data',)

    def __init__(self):
        """The QMCDict initializer function."""
        self._data = OrderedDict({})

        for item in self.__items__:
            item_id = item.name
            self._data[item_id] = item.value, item, True

        for factory in self.__factories__:
            item_id = factory.name
            self._data[item_id] = None, factory, False

    def init_item(self, item_id, obj):
        """

        :param item_id:
        :param obj:
        :return:
        """
        if isinstance(obj, DictAttribute):
            if obj.__factory__:
                item = obj.value(self)
            else:
                item = obj.value
        else:
            raise ValueError

        self._data[item_id] = item, obj, True

    def __getitem__(self, item_id):
        """"""
        if item_id not in self._data:
            msg = "there is no kernel item '{}'".format(item_id)
            raise KeyError(msg)

        item, obj, ready = self._data[item_id]
        if ready:
            return item

        self.init_item(item_id, obj)

        # NOTE: We could use self[item_id]
        item, *_ = self._data[item_id]
        return item

    def __len__(self):
        """"""
        return len(self._data)

    def __iter__(self):
        """"""
        return iter(self._data)


@jit('f8(f8,f8,f8)', nopython=True, cache=True)
def potential_fn(z, v0, r):
    """Calculates the potential energy of the Bose gas due to the
     external potential.

     :param v0:
     :param r:
     :param z: The current configuration of the positions of the
               particles.
     :return:
    """
    z_a, z_b = 1 / (1 + r), r / (1 + r)
    z_cell = z % 1.
    return v0 if z_a < z_cell else 0.


def jit_system_funcs(system: MultiRodsSystem):
    """

    :param system:
    :return:
    """
    v0 = system.lattice_depth
    r = system.lattice_ratio

    @jit('f8(f8)', nopython=True, cache=True)
    def potential_func(z):
        """"""
        return potential_fn(z, v0, r)

    system_funcs = {
        'potential_func': potential_func
    }

    return system_funcs


@jit(['f8(f8)'], nopython=True, cache=True)
def sign(v):
    """Retrieves the sign of a floating point number.

    :param v:
    :return:
    """
    return copysign(1., v)


@jit(['f8(f8,f8,f8)'], nopython=True, cache=True)
def min_distance(z_i, z_j, sc_size):
    """Calculates the minimum distance between the particle at
    ``z_i`` and all of the images of the particle at ``z_j``,
    including this. The minimum distance is always less than
    half of the size of the simulation supercell ``sc_size``.

    :param z_i:
    :param z_j:
    :param sc_size:
    :return:
    """
    sc_half = 0.5 * sc_size
    z_ij = z_i - z_j
    if fabs(z_ij) > sc_half:
        # Take the image.
        return -sc_half + (z_ij + sc_half) % sc_size
    return z_ij


@jit(['void(i8)'], nopython=True)
def numba_seed(seed):
    """Seeds the numba RNG

    :param seed:
    :return:
    """
    random.seed(seed)


@jit(['f8(f8,f8,f8)'], nopython=True, cache=True)
def recast_to_supercell(z, z_min, z_max):
    """Gets the position of the particle at ``z`` within the simulation
    supercell with boundaries ``z_min`` y ``z_max``. If the particle is
    outside the supercell, it returns the position of its closest image.

    :param z:
    :param z_min:
    :param z_max:
    :return:
    """
    sc_size = (z_max - z_min)
    return z_min + (z - z_min) % sc_size
