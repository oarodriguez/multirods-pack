from collections import OrderedDict
from math import cos, cosh, exp, fabs, log, pi, sin, sinh, sqrt, tan, tanh

from numba import jit

from multirods_pack.constants import LKP
from multirods_pack.ideal import eigen_energy


def jit_kernel(lattice_depth,
               lattice_ratio,
               interaction_strength,
               particles_number,
               boundaries):
    """Returns JIT compiled functions used to execute a Quantum Monte Carlo
    simulation over a many body quantum fluid.

    The quantum system corresponds to an 1D interacting Bose gas constrained
    by a multi-slabs structure. The multi-rods structure is modeled through
    a Kronig-Penney potential of magnitude :math:`V_0`, whose potential
    barriers have a width :math:`b` and a separation :math:`a` between any
    two adjacent barriers. The interaction of any two bosons is assumed to
    be a contact potential, :math:`g_\mathrm{1D} \delta(z_1 - z_2)`.

    The returned routines calculate an approximation for the energy of
    the ground state of a multi-slabs system using the variational method.

    :param lattice_depth: The potential magnitude in dimension-less
                          units.
    :param lattice_ratio: The ratio width/separation of the barriers,
                          i.e. :math:`b / a`.
    :param interaction_strength: The interaction potential strength
                                 :math:`\gamma`
    :param particles_number: The density of bosons within a unit cell of the
                             system.
    :param boundaries:
    :return: A dictionary with JIT compiled functions that calculates the
             approximation of the ground state energy.
    """

    # IMPORTANT: In order to avoid compiler errors (with llvmlite and/or
    # numba as ``value token required``) we have to convert these arguments to
    # floats and integers.
    v0 = float(lattice_depth)
    r = float(lattice_ratio)
    gn0 = float(interaction_strength)
    n0 = int(particles_number)

    z_a, z_b = 1 / (1 + r) * LKP, r / (1 + r) * LKP
    e0 = float(eigen_energy(v0, r))

    k1 = sqrt(e0)
    kp1 = sqrt(v0 - e0)

    # Simulation box boundary and size
    z_min, z_max = boundaries
    box_size = z_max - z_min

    # Indicates when a Bose gas is free (no external potential).
    gas_is_free = (v0 < 1e-10) or (r < 1e-10)

    # Indicates when a Bose gas is ideal (non-interacting particles).
    gas_is_ideal = (gn0 == 0)

    @jit(nopython=True)
    def wave_function(parameters_set, z_conf):
        """Computes the variational wave function of a system of
        bosons in a specific configuration.

        :param parameters_set:
        :param z_conf:
        :return:
        """
        wf_log = wave_function_log(parameters_set, z_conf)
        return exp(wf_log)

    @jit(nopython=True)
    def wave_function_log(parameters_set, z_drift_conf):
        """Computes the variational wave function of a system of bosons in
        a specific configuration.

        :param parameters_set:
        :param z_drift_conf:
        :return:
        """
        # Unpack the parameters.
        wf_params = parameters_set

        # Unpack the variational parameters.
        # ``rm`` The value of the variational parameter :math:`r_m`.
        # ``k2`` The value of the parameter :math:`k_{2b}`.
        rm, rl, k2 = wf_params

        wf_log = 0

        # Stop summing contributions from one-body terms when the
        # external potential is small enough. This condition corresponds
        # to a uniform Bose gas.
        if gas_is_free and gas_is_ideal:
            # For a free Bose gas with no interactions and no external
            # potential the drift velocity contribution due to the one-body
            # terms and two-body terms of the wave function is zero.
            return wf_log

        for idx in range(n0):

            # Do not add contributions from one-body terms if the external
            # potential is zero.
            if not gas_is_free:

                # Position in units of simulation box length.
                z_box_idx = z_drift_conf[idx, 0]

                # Position in units of potential period.
                z_lat_idx = (z_box_idx * rl) % 1

                # Region where the potential is nonzero.
                if 1 / (1 + r) < z_lat_idx:

                    # One-body term.
                    one_body_fn = fabs(
                        cosh(kp1 * (z_lat_idx - 1 + 0.5 * r / (1 + r)))
                    )

                # Region where the potential is zero.
                else:
                    one_body_fn = fabs(
                        sqrt(1 + v0 / e0 * sinh(
                            0.5 * kp1 * r / (1. + r)
                        ) ** 2.0) * cos(
                            k1 * (z_lat_idx - 0.5 / (1 + r))
                        )
                    )

                wf_log += log(one_body_fn)

            # Do not add contributions from two-body terms if the interaction
            # magnitude is zero.
            if not gas_is_ideal:

                # Total contribution to wave function from two-body terms.
                two_body_wf_log = 0.

                # Position in units of simulation box length.
                z_box_idx = z_drift_conf[idx, 0]

                # For the particles with ``jdx > idx`` there is contribution
                # to both the wave function and the drift velocity.
                for jdx in range(idx + 1, n0):

                    # Position in units of simulation box length.
                    z_box_jdx = z_drift_conf[jdx, 0]

                    # Relative distance between particles.
                    z_ij = z_box_idx - z_box_jdx
                    r_ij = fabs(z_ij)

                    # Fix relative distance.
                    if r_ij > 0.5 * box_size:
                        r_ij = box_size - r_ij

                    if r_ij < fabs(rm):
                        # Wave function contribution.
                        two_body_term = fabs(cos(k2 * (r_ij - rm)))

                        # Add contributions to both wave function and drift
                        # velocity.
                        two_body_wf_log += log(two_body_term)

                wf_log += two_body_wf_log

        return wf_log

    @jit(nopython=True)
    def calculate_drift(parameters_set, z_drift_conf):
        """

        :param parameters_set:
        :param z_drift_conf:
        :return:
        """
        # Unpack the parameters.
        wf_params = parameters_set

        # Unpack the variational parameters.
        # ``rm`` The value of the variational parameter :math:`r_m`.
        # ``k2`` The value of the parameter :math:`k_{2b}`.
        rm, rl, k2 = wf_params

        # Stop summing contributions from one-body terms when the
        # external potential is small enough. This condition corresponds
        # to a uniform Bose gas.
        if gas_is_free and gas_is_ideal:
            # For a free Bose gas with no interactions and no external
            # potential the drift velocity contribution due to the one-body
            # terms and two-body terms of the wave function is zero.
            return

        # Drift velocity for the ``idx`` particle.
        drift_velocity_idx = 0

        for idx in range(n0):

            # Do not add contributions from one-body terms
            # if the external potential is zero.
            if not gas_is_free:

                # Position in units of simulation box length.
                z_box_idx = z_drift_conf[idx, 0]

                # Position in units of potential period.
                z_lat_idx = (z_box_idx * rl) % 1

                # Region where the potential is nonzero.
                if 1 / (1 + r) < z_lat_idx:
                    # One-body logarithmic derivative. As ``kp1`` is given in
                    # units of potential period length, we have ot multiply
                    # by ``rl`` in order to convert it to units of simulation
                    # box length.
                    one_body_fn_idx_lnd = kp1 * (
                        tanh(kp1 * (z_lat_idx - 1 + 0.5 * r / (1 + r)))
                    )

                # Region where the potential is zero.
                else:

                    # One-body logarithmic derivative in units of simulation
                    # box length.
                    one_body_fn_idx_lnd = (
                        -k1 * tan(k1 * (z_lat_idx - 0.5 / (1 + r)))
                    )

                drift_velocity_idx += one_body_fn_idx_lnd

            # Do not add contributions from two-body terms if the interaction
            # magnitude is zero.
            if not gas_is_ideal:

                # Position in units of simulation box length.
                z_box_idx = z_drift_conf[idx, 0]

                for jdx in range(n0):

                    if jdx == idx:
                        continue

                    # Position in units of simulation box length.
                    z_box_jdx = z_drift_conf[jdx, 0]

                    # Relative distance between particles.
                    z_ij = z_box_idx - z_box_jdx
                    sgn = -1 if z_ij < 0 else 1
                    r_ij = fabs(z_ij)
                    sgn = -sgn if r_ij > 0.5 else sgn

                    # If the ``jdx`` particle is outside the simulation box
                    # around the particle ``idx`` then the magnitude
                    # ``r_ij`` # is greater that ``0.5``. In this case we
                    # take the "image" # of particle ``jdx``, so the
                    # distance between particles is effectively
                    # ``1 - r_ij``.
                    if r_ij > 0.5:
                        r_ij = 1 - r_ij

                    if r_ij < fabs(rm):
                        # The drift velocity contribution is the
                        # logarithmic derivative of the corresponding
                        # two-body term.
                        drift_velocity_idx_jdx = (
                            -(k2 / rl) * tan(k2 * (r_ij - rm)) * sgn
                        )
                        drift_velocity_idx += drift_velocity_idx_jdx

            z_drift_conf[idx, 1] = drift_velocity_idx

    @jit(nopython=True)
    def potential(rl, z_conf):
        """Calculates the potential energy of the Bose gas due to the
         external potential.

         :param rl: The quotient between :math:`L/l` between the length
                    :math:`L` of the simulation box and the the potential
                    period :math:`l`.
         :param z_conf: The current configuration of the positions of the
                        particles.
         :return: The potential of the particle.
        """
        z_lat_idx = (z_conf * rl) % 1

        # We know the potential magnitude in units of potential period,
        # so we have to multiply by `rs ** 2` in order to convert it to
        # simulation box length units.
        # NOTICE: We do not convert to units of simulation box length.
        return v0 if z_a < z_lat_idx else 0

    @jit(nopython=True)
    def particle_local_energy_parts(index,
                                    parameters_set,
                                    z_drift_conf):
        """Computes the different contributions to the local energy for
        a given configuration of the position of the bodies. These
        contributions are the kinetic energy and the potential energy
        of each particle.

        :param index: The index of the current particle.
        :param parameters_set:
        :param z_drift_conf: The current configuration of the positions of the
                       particles.
        :return: The different contributions to the local energy from the
                 given particle.
        """
        one_body_kin_energy = 0.
        two_body_kin_energy = 0.
        drift_magnitude = 0.
        potential_energy = 0.

        # Unpack the parameters.
        wf_params = parameters_set

        # Unpack the variational parameters.
        # ``rm`` The value of the variational parameter :math:`r_m`.
        # ``k2`` The value of the parameter :math:`k_{2b}`.
        rm, rl, k2 = wf_params

        # Stop summing contributions from one-body terms when the
        # external potential is small enough. This condition corresponds
        # to a uniform Bose gas.
        if gas_is_free and gas_is_ideal:
            return 0., 0.

        # Short alias.
        idx = index

        # Drift velocity for the ``idx`` particle.
        drift_idx = 0

        # Do not add contributions from one-body terms if the external
        # potential is zero.
        if not gas_is_free:

            # Position in units of simulation box.
            z_box_idx = z_drift_conf[idx, 0]

            # Position in units of potential period.
            z_lat_idx = (z_box_idx * rl) % 1

            if 1 / (1 + r) < z_lat_idx:
                # External potential is non-zero in this region.

                # One-body eigen-energy in units of simulation box length.
                # NOTICE: Do not convert.
                one_body_eigen_energy = e0 - v0

                # One-body logarithmic derivative. As `kp1` is given in
                # units of potential period length, we have ot multiply
                # by `rs` in order to convert it to units of simulation
                # box length.
                one_body_fn_idx_lnd = kp1 * (
                    tanh(kp1 * (z_lat_idx - 1 + 0.5 * r / (1 + r)))
                )

            else:
                # One-body eigen-energy in units of simulation box length.
                one_body_eigen_energy = e0

                # One-body logarithmic derivative in units of simulation
                # box length.
                one_body_fn_idx_lnd = (
                    -k1 * tan(k1 * (z_lat_idx - 0.5 / (1 + r)))
                )

            one_body_kin_energy += (
                one_body_eigen_energy + one_body_fn_idx_lnd ** 2
            )
            drift_idx += one_body_fn_idx_lnd

            # Accumulate the potential energy.
            potential_energy += potential(rl, z_box_idx)

        # Do not add contributions from two-body terms if the interaction
        # magnitude is zero.
        if not gas_is_ideal:

            # Position in units of simulation box.
            z_box_idx = z_drift_conf[idx, 0]

            for jdx in range(n0):
                # Do not account diagonal terms.
                if jdx == idx:
                    continue

                # Position in units of simulation box length.
                z_box_jdx = z_drift_conf[jdx, 0]

                # Relative distance between particles.
                z_ij = z_box_idx - z_box_jdx
                sgn = -1 if z_ij < 0 else 1
                r_ij = fabs(z_ij)
                # sgn = -sgn if r_ij > 0.5 else sgn

                # If the ``jdx`` particle is outside the simulation box
                # around the particle ``idx`` then the magnitude ``r_ij``
                # is greater that ``0.5``. In this case we take the "image"
                # of particle ``jdx``, so the distance between particles is
                # effectively ``1 - r_ij``.
                if r_ij > 0.5 * box_size:
                    r_ij = box_size - r_ij

                if r_ij < fabs(rm):
                    # Two-body eigen-energy in units of simulation box
                    # length.
                    two_body_eigen_energy = (k2 / rl) ** 2

                    # Logarithmic derivative of the two-body term.
                    two_body_fn_idx_jdx_lnd = (
                        -(k2 / rl) * tan(k2 * (r_ij - rm)) * sgn
                    )

                    # Contribution to the two-body kinetic energy from
                    # the par ``idx``-``jdx``.
                    two_body_kin_energy += (
                        two_body_eigen_energy +
                        two_body_fn_idx_jdx_lnd ** 2
                    )

                    # Add the contribution from the two-body terms to the
                    # drift velocity.
                    drift_idx += two_body_fn_idx_jdx_lnd

        # Save drift velocity.
        z_drift_conf[idx, 1] = drift_idx

        # Accumulate to the drift velocity squared magnitude.
        drift_magnitude += drift_idx ** 2

        kin_energy = (
            one_body_kin_energy + two_body_kin_energy -
            drift_magnitude
        )

        return kin_energy, potential_energy

    @jit(nopython=True)
    def local_energy_parts(parameters_set,
                           z_drift_conf):
        """Computes the different contributions to the local energy for
        a given configuration of the position of the bodies. These
        contributions are the kinetic energy and the potential energy
        of each particle.

        :param parameters_set:
        :param z_drift_conf: The current configuration of the positions of the
                       particles.
        :return: A tuple with the local energy contributions.
        """
        kin_energy = 0.
        pot_energy = 0.

        # Stop summing contributions from one-body terms when the
        # external potential is small enough. This condition corresponds
        # to a uniform Bose gas.
        if gas_is_free and gas_is_ideal:
            return 0., 0.

        # Add the contributions from all the particles.
        for idx in range(n0):
            energy_parts = particle_local_energy_parts(
                idx, parameters_set, z_drift_conf
            )
            kin_energy += energy_parts[0]
            pot_energy += energy_parts[1]

        return kin_energy, pot_energy

    @jit(nopython=True)
    def local_energy(parameters_set, z_drift_conf):
        """Computes the local energy for a given configuration of the
        position of the bodies.

        :param parameters_set:
        :param z_drift_conf:
        """
        total_energy = 0.
        for energy in local_energy_parts(parameters_set,
                                         z_drift_conf):
            total_energy += energy

        return total_energy

    @jit(nopython=True)
    def local_energy_to_buffer(parameters_set,
                               z_drift_conf,
                               result):
        """

        :param parameters_set:
        :param z_drift_conf:
        :param result:
        """
        result[0] = local_energy(parameters_set, z_drift_conf)

    @jit(nopython=True)
    def local_one_body_density(parameters_set, z_drift_conf, result):
        """Computes the logarithm of the local one-body density matrix
        for a given configuration of the position of the bodies and for a
        specified particle index.

        :param parameters_set:
        :param z_drift_conf:
        :param result:
        :return:
        """

        #
        wf_params, density_params = parameters_set

        # The tuple of physical parameters to evaluate the one-body local
        # density.
        z_rel, = density_params

        # ``rm``: The value of the variational parameter :math:`r_m`.
        # ``rl``: The value of the variational parameter :math:`r_l`.
        # ``k2``: The value of the parameter :math:`k_{2b}`.
        rm, rl, k2 = wf_params

        if gas_is_free and gas_is_ideal:
            result[0] = 1.
            return

        # The average natural logarithm of the density.
        local_density = 0.

        # The local one-body density matrix is calculated as the quotient
        # of the wave function with the ``idx`` particle shifted a distance
        # ``z_rel`` from its original position divided by the wave function
        # with the particles evaluated in their original positions. To improve
        # statistics, we average over all possible particle displacements.
        for idx in range(n0):

            # The natural logarithm of the density.
            local_density_idx_log = 0.

            if not gas_is_free:

                # Position in units of potential period.
                z_lat_idx = z_drift_conf[idx, 0] * rl

                # Relative distance in units of potential period.
                z_lat_rel = z_rel * rl

                one_body_amp = sqrt(1 + v0 / e0 * sinh(
                    0.5 * kp1 * r / (1 + r)
                ) ** 2.0)

                z_period_idx_cell = z_lat_idx % 1
                if 1 / (1 + r) < z_period_idx_cell:
                    one_body_fn_no_shift = cosh(kp1 * (
                        z_period_idx_cell - 1 + 0.5 * r / (1 + r)
                    ))
                else:
                    one_body_fn_no_shift = one_body_amp * cos(
                        k1 * (z_period_idx_cell - 0.5 / (1 + r))
                    )

                z_lat_idx_shift = (z_lat_idx + z_lat_rel) % 1
                if 1 / (1 + r) < z_lat_idx_shift:
                    one_body_fn_shift_left = cosh(kp1 * (
                        z_lat_idx_shift - 1 + 0.5 * r / (1 + r)
                    ))
                else:
                    one_body_fn_shift_left = one_body_amp * cos(
                        k1 * (z_lat_idx_shift - 0.5 / (1 + r))
                    )

                # Accumulate terms.
                local_density_idx_log += (
                    log(one_body_fn_shift_left) - log(one_body_fn_no_shift)
                )

            if not gas_is_ideal:

                # Position in units of simulation box size.
                z_box_idx = z_drift_conf[idx, 0]

                # Relocate the particle within the unitary cell.
                z_idx_shifted = (z_box_idx + z_rel) % 1

                for jdx in range(n0):
                    if idx == jdx:
                        continue

                    z_box_jdx = z_drift_conf[jdx, 0]

                    # Term with no shift
                    r_ij = fabs(z_box_idx - z_box_jdx)
                    if r_ij > 0.5:
                        r_ij = 1 - r_ij

                    if r_ij < fabs(rm):
                        two_body_fn_no_shift = cos(k2 * (r_ij - rm))
                    else:
                        two_body_fn_no_shift = 1

                    # Term with the ``idx`` particle shifted.
                    r_ij = fabs(z_idx_shifted - z_box_jdx)
                    if r_ij > 0.5:
                        r_ij = 1 - r_ij

                    if r_ij < fabs(rm):
                        two_body_fn_shift_left = cos(k2 * (r_ij - rm))
                    else:
                        two_body_fn_shift_left = 1

                    # Accumulate terms.
                    local_density_idx_log += (
                        log(two_body_fn_shift_left) - log(two_body_fn_no_shift)
                    )

            # Accumulate total.
            local_density += exp(local_density_idx_log)

        result[0] = local_density / n0

    @jit(nopython=True)
    def local_two_body_correlation(parameters_set, z_drift_conf, result):
        """Computes the local two-body correlation function for a given
        configuration of the position of the bodies.

        :param parameters_set:
        :param z_drift_conf:
        :param result:
        :return:
        """

        # Unpack the parameters.
        _, __, ___, correlation_params = parameters_set

        # The tuple of physical parameters to evaluate the two-body
        # correlation distribution.
        left_bins, right_bins = correlation_params

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        # This condition corresponds to a uniform ideal Bose gas.
        # if gas_is_free and gas_is_ideal:
        #     result[:] = 1.
        #     return

        # Reset the count.
        result[:] = 0.

        for idx in range(n0):

            z_idx = z_drift_conf[idx, 0] % 1
            z_idx_bin = int(z_idx * left_bins)

            for jdx in range(idx + 1, n0):
                z_jdx = z_drift_conf[jdx, 0] % 1
                z_jdx_bin = int(z_jdx * right_bins)

                # Accumulate the count in the bin that corresponds to both
                # the left and right positions.
                result[z_idx_bin, z_jdx_bin, 0] += 1

    @jit(nopython=True)
    def local_structure_factor(parameters_set, z_drift_conf, result):
        """Computes the local two-body correlation function for a given
        configuration of the position of the bodies.

        :param parameters_set:
        :param z_drift_conf:
        :param result:
        :return:
        """

        # Unpack the parameters.
        physical_parameters, __, ___, ____ = parameters_set

        # The tuple of physical parameters to evaluate the two-body
        # correlation distribution.
        nkz, = physical_parameters

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        # This condition corresponds to a uniform ideal Bose gas.
        # if gas_is_free and gas_is_ideal:
        #     result[:] = 1.
        #     return

        for ndx in range(1, nkz):
            # Initial contributions for the ``ndx`` momentum.
            sum_sin = 0
            sum_cos = 0

            k_ndx = 2 * pi * ndx
            # This is the value of the momentum. As we have a periodic system
            # we are constrained to momenta whose values are integer multiple
            # of 2 * pi (the simulation box size being the unit).

            for idx in range(n0):
                z_idx = z_drift_conf[idx, 0]

                # Accumulate both contributions to the structure factor.
                sum_cos += cos(k_ndx * z_idx)
                sum_sin += sin(k_ndx * z_idx)

            result[ndx, 0] = sum_cos ** 2 + sum_sin ** 2

    return OrderedDict({
        'WAVE_FUNCTION': wave_function,
        'WAVE_FUNCTION_LOG': wave_function_log,
        'DRIFT_PARTS': calculate_drift,
        'LOCAL_ENERGY': local_energy,
        'LOCAL_ENERGY_TO_BUFFER': local_energy_to_buffer,
        'LOCAL_ONE_BODY_DENSITY': local_one_body_density,
        'LOCAL_TWO_BODY_CORRELATION': local_two_body_correlation,
        'LOCAL_STRUCTURE_FACTOR': local_structure_factor
    })

# class DriftModel(LiebModel):
#     """Represents a many-body quantum system whose trial wave function is
#     built from a product of two-body trial functions of Lieb-Liniger type.
#     In addition, the core functions are compiled so the drift velocity
#     (the gradient of the wave function) is evaluated for the current
#     configuration of the system. This task is not realized by the core
#     functions of the :class:`LiebModel` as it is a relatively costly
#     operation.
#     """
#
#     def compile_core(self, evaluate_drift):
#         # Enable the evaluation of the drift velocity during compilation.
#         return jit_kernel(*self.params)
