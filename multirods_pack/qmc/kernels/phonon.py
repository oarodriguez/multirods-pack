from collections import OrderedDict
from math import (
    cos, cosh, exp, fabs, log, pi, sin, sinh, sqrt, tan,
    tanh
)

import numpy.random as random
from numba import float64, jit, vectorize

from multirods_pack.constants import LKP
from multirods_pack.ideal import eigen_energy


def jit_kernel(lattice_depth,
               lattice_ratio,
               interaction_strength,
               boson_number,
               boundaries):
    """Returns JIT compiled functions used to execute a Quantum Monte Carlo
    simulation over a many body quantum fluid.

    The quantum system corresponds to an 1D interacting Bose gas constrained
    by a multi-slabs structure. The multi-slabs structure is modeled through
    a Kronig-Penney potential of magnitude :math:`V_0`, whose potential
    barriers have a width :math:`b` and a separation :math:`a` between any
    two adjacent barriers. The interaction of any two bosons is assumed to
    be a contact potential, :math:`g_0 \delta(z_1 - z_2)`.

    The returned routines calculate an approximation for the energy of
    the ground state of a multi-slabs system using the variational method.

    :param lattice_depth: The potential magnitude in dimension-less
                                units.
    :param lattice_ratio: The ratio width/separation of the barriers,
                            i.e. :math:`b / a`.
    :param interaction_strength: The interaction potential strength :
                                 math:`\gamma`
    :param boson_number: The density of bosons within an unit cell of the
                         system.
    :param boundaries:
    :return: A dictionary with JIT compiled functions that calculates the
    approximation of
    the ground state energy.
    """

    # IMPORTANT: In order to avoid compiler errors (with llvmlite and/or
    # numba as ``value token required``) we have to convert these arguments to
    # floats and integers.
    v0 = float(lattice_depth)
    r = float(lattice_ratio)
    gn0 = float(interaction_strength)
    n0 = int(boson_number)

    z_a, z_b = 1 / (1 + r) * LKP, r / (1 + r) * LKP
    e0 = float(eigen_energy(v0, r))

    k1 = sqrt(e0)
    kp1 = sqrt(v0 - e0)

    # Simulation box boundary and size
    z_min, z_max = boundaries
    box_size = z_max - z_min

    # Indicates when a Bose gas is free (no external potential).
    gas_is_free = (v0 < 1e-10) or (r < 1e-10)

    # Indicates when a Bose gas is ideal (non-interacting particles).
    gas_is_ideal = (gn0 == 0)

    @vectorize([float64(float64, float64, float64, float64, float64, float64)])
    def two_body_ufunc(rm, k2, beta, r_off, am, rz):
        """Computes the two-body correlation Jastrow function.

        :param rm:
        :param k2:
        :param beta:
        :param r_off:
        :param am:
        :param rz:
        :return:
        :return:
        """
        if gn0 == 0:
            return 1.0
        if rz < fabs(rm):
            return am * cos(k2 * (rz - r_off))
        else:
            return sin(pi * rz) ** beta

    @jit(nopython=True)
    def wave_function(parameters_set, z_drift_conf):
        """Computes the variational wave function of a system of
        bosons in a specific configuration.

        :param parameters_set:
        :param z_drift_conf:
        :return:
        """
        wf_log = wave_function_log(parameters_set, z_drift_conf)
        return exp(wf_log)

    @jit(nopython=True)
    def wave_function_log(parameters_set, z_drift_conf):
        """Computes the variational wave function of a system of bosons in
        a specific configuration.

        :param parameters_set:
        :param z_drift_conf:
        :return:
        """
        wf_log = 0.

        # Unpack the parameters.
        wf_params = parameters_set
        rm, k2, beta, r_off, am = wf_params

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        # This condition corresponds to a uniform ideal Bose gas.
        if gas_is_free and gas_is_ideal:
            return wf_log

        for idx in range(n0):

            # Do not add contributions from one-body terms if the external
            # potential is zero.
            if not gas_is_free:

                # Position in units of lattice period.
                z_idx = z_drift_conf[idx, 0]

                # Position in elementary cell.
                z_lat_idx = z_idx % LKP

                if z_a < z_lat_idx:

                    # One-body term.
                    one_body_fn = cosh(
                        kp1 * (z_lat_idx - 1 + 0.5 * z_b)
                    )

                # Region where the potential is zero.
                else:
                    one_body_fn = fabs(
                        sqrt(1 + v0 / e0 * sinh(
                            0.5 * sqrt(v0 - e0) * z_b
                        ) ** 2.0) * cos(
                            k1 * (z_lat_idx - 0.5 * z_a)
                        )
                    )

                wf_log += log(one_body_fn)

            # Do not add contributions from two-body terms if the interaction
            # magnitude is zero.
            if not gas_is_ideal:

                # Total contribution to wave function from two-body terms.
                two_body_wf_log = 0.

                z_idx = z_drift_conf[idx, 0]

                # For the particles with ``jdx > idx`` there is contribution
                # to both the wave function and the drift velocity.
                for jdx in range(idx + 1, n0):

                    # Position in units of simulation box length.
                    z_jdx = z_drift_conf[jdx, 0]

                    z_ij = z_idx - z_jdx
                    r_ij = fabs(z_ij)

                    # If the ``jdx`` particle is outside the simulation box
                    # around the particle ``idx`` then the magnitude
                    # ``r_ij`` is greater that ``0.5 * box_size``. In
                    # this case take the "image" of particle ``jdx``,
                    # so the distance between particles is effectively
                    # ``box_size - r_ij``.
                    if r_ij > 0.5 * box_size:
                        r_ij = box_size - r_ij

                    # Two-body term.
                    if r_ij < fabs(rm):
                        two_body_term = fabs(am * cos(k2 * (r_ij - r_off)))
                    else:
                        two_body_term = fabs(sin(pi * r_ij / box_size) ** beta)

                    two_body_wf_log += log(two_body_term)

                wf_log += two_body_wf_log

        return wf_log

    @jit(nopython=True)
    def uniform_advance(displace_params, main_conf, aux_conf):
        """Move the current configuration of the system. The moves are
        displacements of the original position plus a term sampled from
        a uniform distribution.

        :param displace_params: The maximum amplitude of the displacement..
        :param main_conf: The current configuration.
        :param aux_conf: Auxiliary configuration.
        """
        spread_amp, = displace_params

        # Symbol declaration must be here :)
        rand = random.rand

        for kdx in range(n0):
            z_kdx = main_conf[kdx, 0]

            sp = rand() - 0.5
            z_aux_kdx = z_kdx + sp * spread_amp

            # Boundary conditions are periodic then the position
            # must be recast into the simulation box but from the
            # opposite side.
            aux_conf[kdx, 0] = z_min + z_aux_kdx % box_size

    @jit(nopython=True)
    def potential(z_conf):
        """Calculates the potential energy of the Bose gas due to the
         external potential.

         :param z_conf: The current configuration of the positions of the
                        particles.
         :return:
        """
        z_idx = z_conf % LKP

        #: We know the potential magnitude in units of potential period,
        #: so we have to multiply by `rs ** 2` in order to convert it to
        #: simulation box length units.
        return v0 if z_a < z_idx else 0

    @jit(nopython=True)
    def particle_local_energy_parts(index, parameters_set, z_drift_conf):
        """Computes the local energy for a given configuration of the
        position of the bodies. The kinetic energy of the hamiltonian is
        computed through central finite differences.

        :param index:
        :param parameters_set:
        :param z_drift_conf: The current configuration of the positions of the
                       particles.
        :return: The local energy.
        """
        one_body_kin_energy = 0
        two_body_kin_energy = 0
        drift_magnitude = 0
        pot_energy = 0

        # Unpack the parameters.
        wf_params = parameters_set

        # The variational parameters ``rm, k2, beta, r_off, am``.
        rm, k2, beta, r_off, am = wf_params

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        # This condition corresponds to a uniform ideal Bose gas.
        if gas_is_free and gas_is_ideal:
            return 0., 0.

        # Short alias.
        idx = index

        # Drift velocity for the ``idx`` particle.
        drift_idx = 0

        if not gas_is_free:

            # Position in units of simulation box.
            z_idx = z_drift_conf[idx, 0]

            # Position in the elementary cell.
            z_lat_idx = z_idx % LKP

            if z_a < z_lat_idx:
                # External potential is non-zero in this region.

                # One-body eigen-energy in units of simulation box length.
                one_body_eigen_energy = e0 - v0

                # One-body logarithmic derivative. As `kp1` is given in
                # units of potential period length, we have ot multiply
                # by ``rs`` in order to convert it to units of simulation
                # box length.
                one_body_fn_idx_lnd = kp1 * (
                    tanh(kp1 * (z_lat_idx - 1 + 0.5 * z_b))
                )
            else:
                # One-body eigen-energy in units.
                one_body_eigen_energy = e0

                # One-body logarithmic derivative in units of simulation
                # box length.
                one_body_fn_idx_lnd = (
                    -k1 * tan(k1 * (z_lat_idx - 0.5 * z_a))
                )

            one_body_kin_energy += (
                one_body_eigen_energy + one_body_fn_idx_lnd ** 2
            )
            drift_idx += one_body_fn_idx_lnd

            # Accumulate the potential energy.
            pot_energy += potential(z_idx)

        # Do not add contributions from two-body terms if the interaction
        # magnitude is zero.
        if not gas_is_ideal:

            # Position in units of simulation box.
            z_idx = z_drift_conf[idx, 0]

            for jdx in range(n0):
                # Do not account diagonal terms.
                if jdx == idx:
                    continue

                # Position in units of simulation box length.
                z_jdx = z_drift_conf[jdx, 0]

                # Relative distance between particles.
                z_ij = z_idx - z_jdx
                sgn = -1 if z_ij < 0 else 1
                r_ij = fabs(z_ij)
                sgn = -sgn if r_ij > 0.5 * box_size else sgn

                # If the ``jdx`` particle is outside the simulation box
                # around the particle ``idx`` then the magnitude ``r_ij``
                # is greater that ``0.5``. In this case we take the "image"
                # of particle ``jdx``, so the distance between particles is
                # effectively ``1 - r_ij``.
                if r_ij > 0.5 * box_size:
                    r_ij = box_size - r_ij

                if r_ij < fabs(rm):
                    # Two-body eigen-energy in units of simulation box
                    # length.
                    two_body_eigen_energy = k2 ** 2

                    # Logarithmic derivative of the two-body term.
                    two_body_fn_idx_jdx_lnd = (
                        -k2 * tan(k2 * (r_ij - r_off)) * sgn
                    )

                else:
                    # Two-body eigen-energy in units of simulation box
                    # length.
                    two_body_eigen_energy = -(pi / box_size) ** 2 * beta * (
                        (beta - 1) / (tan(pi * r_ij / box_size) ** 2) - 1
                    )

                    # Logarithmic derivative of the two-body term.
                    two_body_fn_idx_jdx_lnd = (pi / box_size) * beta / (
                        tan(pi * r_ij / box_size) * sgn
                    )

                two_body_kin_energy += (
                    two_body_eigen_energy + two_body_fn_idx_jdx_lnd ** 2
                )

                # Add the contribution from the two-body terms to the
                # drift # velocity.
                drift_idx += two_body_fn_idx_jdx_lnd

        # Save the drift velocity
        z_drift_conf[idx, 1] = drift_idx

        # Accumulate to the drift velocity squared magnitude.
        drift_magnitude += drift_idx ** 2

        kin_energy = (one_body_kin_energy + two_body_kin_energy -
                      drift_magnitude)

        return kin_energy, pot_energy

    @jit(nopython=True)
    def local_energy_parts(parameters_set, z_drift_conf):
        """Computes the different contributions to the local energy for
        a given configuration of the position of the bodies. These
        contributions are the kinetic energy and the potential energy
        of each particle.

        :param parameters_set:
        :param z_drift_conf: The current configuration of the positions of the
                       particles.
        :return: A tuple with the local energy contributions.
        """
        kin_energy = 0.
        pot_energy = 0.

        # Stop summing contributions from one-body terms when the
        # external potential is small enough. This condition corresponds
        # to a uniform Bose gas.
        if gas_is_free and gas_is_ideal:
            return 0., 0.

        # Add the contributions from all the particles.
        for idx in range(n0):
            particle_energy_parts = particle_local_energy_parts(
                idx, parameters_set, z_drift_conf
            )
            kin_energy += particle_energy_parts[0]
            pot_energy += particle_energy_parts[1]

        return kin_energy, pot_energy

    @jit(nopython=True)
    def local_energy(parameters_set, z_drift_conf):
        """Computes the local energy for a given configuration of the
        position of the bodies.

        :param parameters_set:
        :param z_drift_conf:
        """
        total_energy = 0.
        for energy in local_energy_parts(parameters_set, z_drift_conf):
            total_energy += energy

        return total_energy

    @jit(nopython=True)
    def local_energy_to_buffer(parameters_set, z_drift_conf, result):
        """

        :param parameters_set:
        :param z_drift_conf:
        """
        result[0] = local_energy(parameters_set, z_drift_conf)

    @jit(nopython=True)
    def local_one_body_density(parameters_set, z_drift_conf, result):
        """Computes the logarithm of the local one-body density matrix
        for a given configuration of the position of the bodies and for a
        specified particle index.

        :param parameters_set:
        :param z_drift_conf:
        :param result:
        :return:
        """

        #
        wf_params, obd_params = parameters_set

        # The tuple of physical parameters to evaluate the one-body local
        # density.
        z_rel, = obd_params

        # The variational parameters ``rm, k2, beta, r_off, am``.
        rm, k2, beta, r_off, am = wf_params

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        # This condition corresponds to a uniform ideal Bose gas.
        if gas_is_free and gas_is_ideal:
            result[0] = 1.
            return

        # The average natural logarithm of the density.
        local_obd = 0.

        # The local one-body density matrix is calculated as the quotient
        # of the wave function with the ``idx`` particle shifted a distance
        # ``z_rel`` from its original position divided by the wave function
        # with the particles evaluated in their original positions. To improve
        # statistics, we average over all possible particle displacements.
        for idx in range(n0):

            # The natural logarithm of the density.
            local_obd_idx_log = 0.

            if not gas_is_free:

                # Position in units of potential period.
                z_idx = z_drift_conf[idx, 0]

                # Relative distance in units of potential period.
                # z_rel = z_rel

                one_body_amp = sqrt(
                    1 + v0 / e0 * sinh(0.5 * kp1 * z_b) ** 2.0
                )

                z_lat_idx = z_idx % LKP

                if 1 / (1 + r) < z_lat_idx:
                    one_body_fn_no_shift = cosh(kp1 * (
                        z_lat_idx - 1 + 0.5 * z_b
                    ))
                else:
                    one_body_fn_no_shift = one_body_amp * cos(
                        k1 * (z_lat_idx - 0.5 * z_a)
                    )

                z_idx_shift_left = (z_idx + z_rel) % LKP

                if 1 / (1 + r) < z_idx_shift_left:
                    one_body_fn_shift_left = cosh(kp1 * (
                        z_idx_shift_left - 1 + 0.5 * z_b
                    ))
                else:
                    one_body_fn_shift_left = one_body_amp * cos(
                        k1 * (z_idx_shift_left - 0.5 * z_a)
                    )

                # Accumulate terms.
                local_obd_idx_log += (
                    log(one_body_fn_shift_left) - log(one_body_fn_no_shift)
                )

            if not gas_is_ideal:

                # Position in units of simulation box size.
                z_idx = z_drift_conf[idx, 0]

                # Term with the ``idx`` particle shifted left.
                # Relocate the particle within the unitary cell.
                z_idx_shift_left = (z_idx + z_rel) % box_size

                for jdx in range(n0):
                    #
                    if idx == jdx:
                        continue

                    z_jdx = z_drift_conf[jdx, 0]

                    # Term with no shift
                    r_ij = fabs(z_idx - z_jdx)
                    if r_ij > 0.5 * box_size:
                        # two_body_fn_no_shift = 1.
                        r_ij = box_size - r_ij

                    if r_ij < fabs(rm):
                        # Two-body term.
                        two_body_fn_no_shift = (
                            am * cos(k2 * (r_ij - r_off))
                        )

                    else:
                        # Two-body term.
                        two_body_fn_no_shift = (
                            sin(pi * r_ij / box_size) ** beta
                        )

                    r_ij = fabs(z_idx_shift_left - z_jdx)
                    if r_ij > 0.5 * box_size:
                        # two_body_fn_shift_left = 1.
                        r_ij = box_size - r_ij

                    if r_ij < fabs(rm):
                        # Two-body term.
                        two_body_fn_shift_left = (
                            am * cos(k2 * (r_ij - r_off))
                        )

                    else:
                        two_body_fn_shift_left = (
                            sin(pi * r_ij / box_size) ** beta
                        )

                    # Accumulate terms.
                    local_obd_idx_log += (
                        log(two_body_fn_shift_left) - log(two_body_fn_no_shift)
                    )

            # Accumulate total.
            local_obd += exp(local_obd_idx_log)

        result[0] = local_obd / n0

    @jit(nopython=True)
    def local_two_body_correlation(parameters_set, z_drift_conf, result):
        """Computes the local two-body correlation function for a given
        configuration of the position of the bodies.

        :param parameters_set:
        :param z_drift_conf:
        :param result:
        :return:
        """

        # Unpack the parameters.
        _, __, ___, correlation_params = parameters_set

        # The tuple of physical parameters to evaluate the two-body
        # correlation distribution.
        left_bins, right_bins = correlation_params

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        # This condition corresponds to a uniform ideal Bose gas.
        # if gas_is_free and gas_is_ideal:
        #     result[:] = 1.
        #     return

        # Reset the count.
        result[:] = 0.

        for idx in range(n0):

            z_idx = z_drift_conf[idx, 0] % 1
            z_idx_bin = int(z_idx * left_bins)

            for jdx in range(idx + 1, n0):
                z_jdx = z_drift_conf[jdx, 0] % 1
                z_jdx_bin = int(z_jdx * right_bins)

                # Accumulate the count in the bin that corresponds to both
                # the left and right positions.
                result[z_idx_bin, z_jdx_bin, 0] += 1

    @jit(nopython=True)
    def local_structure_factor(parameters_set, z_drift_conf, result):
        """Computes the local two-body correlation function for a given
        configuration of the position of the bodies.

        :param parameters_set:
        :param z_drift_conf:
        :param result:
        :return:
        """

        # Unpack the parameters.
        physical_parameters, __, ___, ____ = parameters_set

        # The tuple of physical parameters to evaluate the two-body
        # correlation distribution.
        nkz, = physical_parameters

        # Do not add contributions from one-body and two-body terms when
        # the external potential is small enough and interactions are zero.
        # This condition corresponds to a uniform ideal Bose gas.
        # if gas_is_free and gas_is_ideal:
        #     result[:] = 1.
        #     return

        for ndx in range(1, nkz):
            # Initial contributions for the ``ndx`` momentum.
            sum_sin = 0
            sum_cos = 0

            k_ndx = 2 * pi * ndx
            # This is the value of the momentum. As we have a periodic system
            # we are constrained to momenta whose values are integer multiple
            # of 2 * pi (the simulation box size being the unit).

            for idx in range(n0):
                z_idx = z_drift_conf[idx, 0]

                # Accumulate both contributions to the structure factor.
                sum_cos += cos(k_ndx * z_idx)
                sum_sin += sin(k_ndx * z_idx)

            result[ndx, 0] = sum_cos ** 2 + sum_sin ** 2

    return OrderedDict({
        'WAVE_FUNCTION': wave_function,
        'WAVE_FUNCTION_LOG': wave_function_log,
        'UNIFORM_ADVANCE': uniform_advance,
        'LOCAL_ENERGY': local_energy,
        'LOCAL_ENERGY_TO_BUFFER': local_energy_to_buffer,
        'LOCAL_ONE_BODY_DENSITY': local_one_body_density,
        'LOCAL_TWO_BODY_CORRELATION': local_two_body_correlation,
        'LOCAL_STRUCTURE_FACTOR': local_structure_factor
    })
