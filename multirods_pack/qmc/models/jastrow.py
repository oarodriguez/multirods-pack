"""
    multirods_pack.qmc.kernels.jastrow
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""

from abc import abstractmethod
from enum import Enum
from math import (cos, exp, fabs, log, sin)
from typing import Callable

from numba import jit

from multirods_pack.qmc.base import (
    KernelFN, MultiRodsSystem, QMCDict, QMCModel, dict_attribute, min_distance,
    sign
)

POS_SLOT = 0
DRIFT_SLOT = 1
NUM_SLOTS = 2


class JastrowModelFN(Enum):
    """

    """
    OBF = 'one_body_func'
    TBF = 'two_body_func'

    OBF_LD = 'one_body_func_log_dz'
    TBF_LD = 'two_body_func_log_dz'

    OBF_LD2 = 'one_body_func_log_dz2'
    TBF_LD2 = 'two_body_func_log_dz2'


class JastrowModel(QMCModel):
    """Represents the a Quantum Monte Carlo model with a trial-wave
    function of the Bijl-Jastrow type.
    """

    FN = JastrowModelFN

    def __init__(self, system: MultiRodsSystem,
                 boson_number: int,
                 supercell_size: float):
        """

        :param system:
        :param boson_number:
        :param supercell_size:
        """
        self.system = system
        self.boson_number = boson_number
        self.supercell_size = supercell_size
        self._model_key = system, boson_number, supercell_size

    @property
    @abstractmethod
    def one_body_func(self) -> Callable:
        raise NotImplementedError

    @property
    @abstractmethod
    def two_body_func(self) -> Callable:
        raise NotImplementedError

    @property
    @abstractmethod
    def one_body_func_log_dz(self) -> Callable:
        raise NotImplementedError

    @property
    @abstractmethod
    def two_body_func_log_dz(self) -> Callable:
        raise NotImplementedError

    @property
    @abstractmethod
    def one_body_func_log_dz2(self) -> Callable:
        raise NotImplementedError

    @property
    @abstractmethod
    def two_body_func_log_dz2(self) -> Callable:
        raise NotImplementedError

    @abstractmethod
    def one_body_func_params(self, *args, **kwargs):
        raise NotImplementedError

    @abstractmethod
    def two_body_func_params(self, *args, **kwargs):
        raise NotImplementedError

    @abstractmethod
    def _init_trial_funcs(self):
        raise NotImplementedError

    # TODO: Are __eq__ and __hash__ useful?
    def __eq__(self, other):
        """

        :param other:
        :return:
        """
        if not isinstance(other, JastrowModel):
            return False
        return self._model_key == other._model_key

    def __hash__(self):
        """

        :return:
        """

        return hash(self._model_key)


class JastrowFuncs(QMCDict):
    """"""

    def __init__(self, model: JastrowModel):
        """

        :param model: The quantum model represented by the kernel.
        """
        super().__init__()
        self.model = model

    @dict_attribute(KernelFN.I_PDF_LOG)
    def ith_pdf_log_func(self):
        """

        :return:
        """
        model = self.model
        nop = int(model.boson_number)
        sc_size = int(model.supercell_size)

        system = model.system
        gas_is_free = system.is_free
        gas_is_ideal = system.is_ideal

        one_body_fn = model.one_body_func
        two_body_fn = model.two_body_func

        @jit(nopython=True, cache=True)
        def ith_pdf_log(i_, sys_conf, obf_params, tbf_params):
            """Computes the variational wave function of a system of bosons in
            a specific configuration.

            :param i_:
            :param sys_conf:
            :param obf_params:
            :param tbf_params:
            :return:
            """
            ith_pdf_log_ = 0.

            if not gas_is_free:
                # Gas subject to external potential.
                z_i = sys_conf[i_, POS_SLOT]
                obv = one_body_fn(z_i, *obf_params)
                ith_pdf_log_ += log(fabs(obv))

            if not gas_is_ideal:
                # Gas with interactions.
                z_i = sys_conf[i_, POS_SLOT]
                for j_ in range(i_ + 1, nop):
                    z_j = sys_conf[j_, POS_SLOT]
                    z_ij = min_distance(z_i, z_j, sc_size)
                    tbv = two_body_fn(fabs(z_ij), *tbf_params)

                    ith_pdf_log_ += log(fabs(tbv))

            return ith_pdf_log_

        return ith_pdf_log

    @dict_attribute(KernelFN.PDF_LOG)
    def pdf_log_func(self):
        """

        :return:
        """
        model = self.model
        nop = int(model.boson_number)

        system = model.system
        gas_is_free = system.is_free
        gas_is_ideal = system.is_ideal

        # Compiled function
        ith_pdf_log = self.ith_pdf_log_func

        @jit(nopython=True, cache=True)
        def pdf_log(sys_conf, obf_params, tbf_params):
            """Computes the variational wave function of a system of bosons in
            a specific configuration.

            :param sys_conf:
            :param obf_params:
            :param tbf_params:
            :return:
            """
            pdf_log_ = 0.
            if gas_is_free and gas_is_ideal:
                return pdf_log_

            for i_ in range(nop):
                pdf_log_ += ith_pdf_log(i_, sys_conf, obf_params, tbf_params)
            return pdf_log_

        return pdf_log

    @dict_attribute(KernelFN.PDF)
    def pdf_func(self):
        """

        :return:
        """
        pdf_log = self.pdf_log_func

        @jit(nopython=True, cache=True)
        def pdf(sys_conf, obf_params, tbf_params):
            """Computes the variational wave function of a system of
            bosons in a specific configuration.

            :param sys_conf:
            :param obf_params:
            :param tbf_params:
            :return:
            """
            pdf_log_ = pdf_log(sys_conf, obf_params, tbf_params)
            return exp(pdf_log_)

        return pdf

    @dict_attribute(KernelFN.D_PDF_LOG_K_M)
    def delta_pdf_log_kth_move_func(self):
        """

        :return:
        """
        model = self.model
        nop = int(model.boson_number)
        sc_size = int(model.supercell_size)

        system = model.system
        gas_is_free = system.is_free
        gas_is_ideal = system.is_ideal

        one_body_fn = model.one_body_func
        two_body_fn = model.two_body_func

        @jit(nopython=True, cache=True)
        def delta_pdf_log_kth_move(k_, z_k_delta,
                                   sys_conf,
                                   obf_params,
                                   tbf_params):
            """Computes the change of the logarithm of the wave function
            after displacing the `k-th` particle by a distance ``z_k_delta``.

            :param k_:
            :param z_k_delta:
            :param sys_conf:
            :param obf_params:
            :param tbf_params:
            :return:
            """
            delta_pdf_log = 0.

            if gas_is_free and gas_is_ideal:
                return delta_pdf_log

            z_k = sys_conf[k_, POS_SLOT]
            z_k_upd = z_k + z_k_delta

            if not gas_is_free:
                # Gas subject to external potential.
                obv = one_body_fn(z_k, *obf_params)
                obv_upd = one_body_fn(z_k_upd, *obf_params)
                delta_pdf_log += log(fabs(obv_upd / obv))

            if not gas_is_ideal:
                # Gas with interactions.
                for i_ in range(nop):
                    if i_ == k_:
                        continue

                    z_i = sys_conf[i_, POS_SLOT]
                    r_ki = fabs(min_distance(z_k, z_i, sc_size))
                    r_ki_upd = fabs(min_distance(z_k_upd, z_i, sc_size))

                    tbv = two_body_fn(r_ki, *tbf_params)
                    tbv_upd = two_body_fn(r_ki_upd, *tbf_params)
                    delta_pdf_log += log(fabs(tbv_upd / tbv))

            return delta_pdf_log

        return delta_pdf_log_kth_move

    @dict_attribute(KernelFN.D_I_DRIFT_K_M)
    def delta_ith_drift_kth_move_func(self):
        """

        :return:
        """
        model = self.model
        nop = int(model.boson_number)
        sc_size = int(model.supercell_size)

        system = model.system
        gas_is_free = system.is_free
        gas_is_ideal = system.is_ideal

        one_body_fn_log_dz = model.one_body_func_log_dz
        two_body_fn_log_dz = model.two_body_func_log_dz

        @jit(nopython=True, cache=True)
        def delta_ith_drift_kth_move(i_, k_,
                                     z_k_delta,
                                     sys_conf,
                                     obf_params,
                                     tbf_params):
            """Computes the change of the i-th component of the drift
            after displacing the k-th particle by a distance ``z_k_delta``.

            :param i_:
            :param k_:
            :param z_k_delta:
            :param sys_conf:
            :param obf_params:
            :param tbf_params:
            :return:
            """
            ith_drift = 0.

            if gas_is_free and gas_is_ideal:
                return ith_drift

            z_k = sys_conf[k_, POS_SLOT]
            z_k_upd = z_k + z_k_delta

            if i_ != k_:
                #
                if gas_is_ideal:
                    return ith_drift

                z_i = sys_conf[i_, POS_SLOT]
                z_ki_upd = min_distance(z_k_upd, z_i, sc_size)
                z_ki = min_distance(z_k, z_i, sc_size)

                # TODO: Move th sign to the function.
                sgn = sign(z_ki)
                ob_fn_ldz = two_body_fn_log_dz(fabs(z_ki),
                                               *tbf_params) * sgn

                sgn = sign(z_ki_upd)
                ob_fn_ldz_upd = two_body_fn_log_dz(fabs(z_ki_upd),
                                                   *tbf_params) * sgn

                ith_drift += -(ob_fn_ldz_upd - ob_fn_ldz)
                return ith_drift

            if not gas_is_free:
                #
                ob_fn_ldz = one_body_fn_log_dz(z_k, *obf_params)
                ob_fn_ldz_upd = one_body_fn_log_dz(z_k_upd, *obf_params)

                ith_drift += ob_fn_ldz_upd - ob_fn_ldz

            if not gas_is_ideal:
                # Gas with interactions.
                for j_ in range(nop):
                    #
                    if j_ == k_:
                        continue

                    z_j = sys_conf[j_, POS_SLOT]
                    z_kj = min_distance(z_k, z_j, sc_size)
                    z_kj_upd = min_distance(z_k_upd, z_j, sc_size)

                    sgn = sign(z_kj)
                    tb_fn_ldz = two_body_fn_log_dz(fabs(z_kj),
                                                   *tbf_params) * sgn

                    sgn = sign(z_kj_upd)
                    tb_fn_ldz_upd = two_body_fn_log_dz(fabs(z_kj_upd),
                                                       *tbf_params) * sgn

                    ith_drift += (tb_fn_ldz_upd - tb_fn_ldz)

            return ith_drift

        return delta_ith_drift_kth_move

    @dict_attribute(KernelFN.I_L_E)
    def ith_energy_func(self):
        """

        :return:
        """
        model = self.model
        nop = int(model.boson_number)
        sc_size = int(model.supercell_size)

        system = model.system
        gas_is_free = system.is_free
        gas_is_ideal = system.is_ideal

        potential_func = system.potential_func
        one_body_fn_log_dz = model.one_body_func_log_dz
        two_body_fn_log_dz = model.two_body_func_log_dz
        one_body_fn_log_dz2 = model.one_body_func_log_dz2
        two_body_fn_log_dz2 = model.two_body_func_log_dz2

        @jit(nopython=True, cache=True)
        def ith_energy(i_, sys_conf, obf_params, tbf_params):
            """Computes the local energy for a given configuration of the
            position of the bodies. The kinetic energy of the hamiltonian is
            computed through central finite differences.

            :param i_:
            :param sys_conf: The current configuration of the positions of the
                           particles.
            :param obf_params:
            :param tbf_params:
            :return: The local energy.
            """

            if gas_is_free and gas_is_ideal:
                return 0.

            # Unpack the parameters.
            kin_energy = 0.
            pot_energy = 0
            drift = 0.

            if not gas_is_free:
                # Case with external potential.
                z_i = sys_conf[i_, POS_SLOT]
                ob_fn_ldz2 = one_body_fn_log_dz2(z_i, *obf_params)
                ob_fn_ldz = one_body_fn_log_dz(z_i, *obf_params)

                kin_energy += (-ob_fn_ldz2 + ob_fn_ldz ** 2)
                pot_energy += potential_func(z_i)
                drift += ob_fn_ldz

            if not gas_is_ideal:
                # Case with interactions.
                z_i = sys_conf[i_, POS_SLOT]

                for j_ in range(nop):
                    # Do not account diagonal terms.
                    if j_ == i_:
                        continue

                    z_j = sys_conf[j_, POS_SLOT]
                    z_ij = min_distance(z_i, z_j, sc_size)
                    sgn = sign(z_ij)

                    tb_fn_ldz2 = two_body_fn_log_dz2(fabs(z_ij),
                                                     *tbf_params)
                    tb_fn_ldz = two_body_fn_log_dz(fabs(z_ij),
                                                   *tbf_params) * sgn

                    kin_energy += (-tb_fn_ldz2 + tb_fn_ldz ** 2)
                    drift += tb_fn_ldz

            # Accumulate to the drift velocity squared magnitude.
            drift_mag = drift ** 2
            kin_energy -= drift_mag
            sys_conf[i_, DRIFT_SLOT] = drift

            return kin_energy + pot_energy

        return ith_energy

    @dict_attribute(KernelFN.L_E)
    def energy_func(self):
        """

        :return:
        """
        model = self.model
        nop = int(model.boson_number)

        ith_energy = self.ith_energy_func

        @jit(nopython=True, cache=True)
        def energy(sys_conf, obf_params, tbf_params):
            """

            :param sys_conf:
            :param obf_params:
            :param tbf_params:
            :return:
            """
            energy_ = 0.
            for i_ in range(nop):
                energy_ += ith_energy(i_, sys_conf, obf_params, tbf_params)
            return energy_

        return energy

    @dict_attribute(KernelFN.L_E_BUFFER)
    def energy_to_buffer_func(self):
        """

        :return:
        """
        energy = self.energy_func

        @jit(nopython=True, cache=True)
        def energy_to_buffer(sys_conf, obf_params, tbf_params, result):
            """Computes the local energy for a given configuration of the
            position of the bodies.

            :param sys_conf:
            :param obf_params:
            :param tbf_params:
            :param result:
            """
            result[0] = energy(sys_conf, obf_params, tbf_params)

        return energy_to_buffer

    @dict_attribute(KernelFN.I_L_OBD)
    def ith_one_body_density_func(self):
        """

        :return:
        """
        model = self.model
        nop = int(model.boson_number)
        sc_size = int(model.supercell_size)

        system = model.system
        gas_is_free = system.is_free
        gas_is_ideal = system.is_ideal

        one_body_fn = model.one_body_func
        two_body_fn = model.two_body_func

        @jit(nopython=True, cache=True)
        def ith_one_body_density(i_, sz, sys_conf, obf_params, tbf_params):
            """Computes the logarithm of the local one-body density matrix
            for a given configuration of the position of the bodies and for a
            specified particle index.

            :param i_:
            :param sz:
            :param sys_conf:
            :param obf_params:
            :param tbf_params:
            :return:
            """
            if gas_is_free and gas_is_ideal:
                return 1.

            # The local one-body density matrix is calculated as the
            # quotient of the wave function with the ``i_`` particle
            # shifted a distance ``sz` from its original position
            # divided by the wave function with the particles evaluated
            # in their original positions. To improve statistics, we
            # average over all possible particle displacements.
            local_obd = 0.

            if not gas_is_free:

                z_i = sys_conf[i_, POS_SLOT]
                z_i_sft = z_i + sz

                ob_fn = one_body_fn(z_i, *obf_params)
                ob_fn_sft = one_body_fn(z_i_sft, *obf_params)
                local_obd += (log(ob_fn_sft) - log(ob_fn))

            if not gas_is_ideal:
                # Interacting gas.
                z_i = sys_conf[i_, POS_SLOT]
                z_i_sft = z_i + sz

                for j_ in range(nop):
                    #
                    if i_ == j_:
                        continue

                    z_j = sys_conf[j_, POS_SLOT]
                    z_ij = min_distance(z_i, z_j, sc_size)
                    tb_fn = two_body_fn(fabs(z_ij), *tbf_params)

                    # Shifted difference.
                    z_ij = min_distance(z_i_sft, z_j, sc_size)
                    tb_fn_shift = two_body_fn(fabs(z_ij), *tbf_params)

                    local_obd += (log(tb_fn_shift) - log(tb_fn))

            return exp(local_obd)

        return ith_one_body_density

    @dict_attribute(KernelFN.L_OBD)
    def one_body_density_func(self):
        """

        :return:
        """
        model = self.model
        nop = int(model.boson_number)

        ith_one_body_density = self.ith_one_body_density_func

        @jit(nopython=True, cache=True)
        def one_body_density(sz, sys_conf, obf_params, tbf_params):
            """Computes the logarithm of the local one-body density matrix
            for a given configuration of the position of the bodies and for a
            specified particle index.

            :param sz:
            :param sys_conf:
            :param obf_params:
            :param tbf_params:
            :return:
            """
            obd = 0.
            for i_ in range(nop):
                obd += ith_one_body_density(i_, sz,
                                            sys_conf,
                                            obf_params,
                                            tbf_params)
            return obd / nop

        return one_body_density

    @dict_attribute(KernelFN.L_OBD_BUFFER)
    def one_body_density_to_buffer_func(self):
        """

        :return:
        """
        one_body_density = self.one_body_density_func

        @jit(nopython=True, cache=True)
        def one_body_density_to_buffer(sz, sys_conf,
                                       obf_params,
                                       tbf_params,
                                       result):
            """Computes the logarithm of the local one-body density matrix
            for a given configuration of the position of the bodies and for a
            specified particle index.

            :param sz:
            :param sys_conf:
            :param obf_params:
            :param tbf_params:
            :param result:
            :return:
            """
            result[0] = one_body_density(sz, sys_conf,
                                         obf_params,
                                         tbf_params)

        return one_body_density_to_buffer

    @dict_attribute(KernelFN.L_SF)
    def structure_factor_func(self):
        """

        :return:
        """
        model = self.model
        nop = int(model.boson_number)

        @jit(nopython=True, cache=True)
        def structure_factor(kz, sys_conf):
            """Computes the local two-body correlation function for a given
            configuration of the position of the bodies.

            :param kz:
            :param sys_conf:
            :return:
            """
            s_sin = 0
            s_cos = 0
            for i_ in range(nop):
                z_i = sys_conf[i_, POS_SLOT]
                s_cos += cos(kz * z_i)
                s_sin += sin(kz * z_i)

            return s_cos ** 2 + s_sin ** 2

        return structure_factor
