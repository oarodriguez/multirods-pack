"""
    multirods_pack.qmc.models
    ~~~~~~~~~~~~~~~~~~~~~~~~~

    Subpackage to group the implementations of the various models of
    trial wave functions used in QMC.
"""

# Jastrow Model classes
from .jastrow import JastrowFuncs, JastrowModel, JastrowModelFN

# TODO: Remove legacy imports.
from .lieb import LiebModel
from .phonon_legacy import PhononModel
