from math import cos, pi, sin

from scipy.optimize import brentq

from multirods_pack.constants import LKP
from multirods_pack.qmc.kernels.lieb import jit_kernel


class LiebModel(object):
    """Represents a many-body quantum system whose trial wave function is
    built from a product of two-body trial functions of Lieb-Liniger type.
    """

    def __init__(self, lattice_depth,
                 lattice_ratio,
                 interaction_strength,
                 boson_number,
                 boundaries):

        """
        :param lattice_depth: The potential magnitude in dimension-less
                              units.
        :param lattice_ratio: The ratio width/separation of the barriers,
                              i.e. :math:`b/a`.
        :param interaction_strength: The interaction potential strength
                                     :math:`\gamma`.
        :param boson_number: The number (and the density) of bosons within
                             the simulation unit cell of the system.
        :param boundaries:
        """

        super().__init__()

        self.lattice_depth = lattice_depth

        self.lattice_ratio = lattice_ratio

        self.interaction_strength = interaction_strength

        self.boson_number = boson_number

        self.boundaries = boundaries

        # The parameters of the system
        self.params = (
            lattice_depth,
            lattice_ratio,
            interaction_strength,
            boson_number
        )

        # Compile the core functions.
        self._compiled_core = self.compile_core()

    @property
    def box_size(self):
        """Simulation box size."""
        z_min, z_max = self.boundaries
        return z_max - z_min

    @property
    def avg_density(self):
        """Average linear density, as the average number of bosons per
        lattice period.
        """
        nop = self.boson_number
        box_size = self.box_size
        return nop / box_size

    @property
    def wave_function(self):
        return self._compiled_core['WAVE_FUNCTION']

    @property
    def wave_function_log(self):
        return self._compiled_core['WAVE_FUNCTION_LOG']

    @property
    def local_energy(self):
        return self._compiled_core['LOCAL_ENERGY']

    @property
    def drift_parts(self):
        return self._compiled_core['DRIFT_PARTS']

    @property
    def local_energy_to_buffer(self):
        return self._compiled_core['LOCAL_ENERGY_TO_BUFFER']

    @property
    def local_one_body_density(self):
        return self._compiled_core['LOCAL_ONE_BODY_DENSITY']

    @property
    def local_two_body_correlation(self):
        return self._compiled_core['LOCAL_TWO_BODY_CORRELATION']

    @property
    def local_structure_factor(self):
        return self._compiled_core['LOCAL_STRUCTURE_FACTOR']

    @staticmethod
    def get_jit_kernel():
        """"""
        return jit_kernel

    def compile_core(self):
        """"Compiles the functions of the model.

        :return: A dictionary with the compiled routines.
        """
        _jit_kernel = self.get_jit_kernel()

        # TODO: Maybe we should save this function as a class attribute
        return _jit_kernel(self.lattice_depth,
                           self.lattice_ratio,
                           self.interaction_strength,
                           self.boson_number,
                           self.boundaries)

    def get_wf_params(self, two_body_match):
        """

        :param two_body_match:
        :return:
        """
        # TODO: property for period size instead of LKP.
        rm = two_body_match
        rl = self.box_size / LKP
        return (rm,) + self.subsidiary_params(rm, rl)

    @staticmethod
    def two_body_parameters(interaction_strength,
                            boson_number,
                            two_body_match,
                            box_lattice_ratio):
        """Calculate the unknown constants that join the two pieces of the
        two-body functions of the Jastrow trial function at the point `zm_var`.
        The parameters are a function of the boson interaction magnitude `g`
        and the average linear density `boson_number` of the system.

        :param interaction_strength: The interaction strength parameter.
        :param boson_number: The number of bosons in the simulation box.
        :param two_body_match: The point where both the pieces of the
            function must be joined.
        :param box_lattice_ratio:
        :return: The momentum of the two-body function.
        """
        gn0, nop = interaction_strength, boson_number
        rm, rl = two_body_match, box_lattice_ratio

        if gn0 == 0:
            # NOTICE: return value must ve a tuple!
            return 0,

        # Convert interaction energy to Lieb gamma.
        # TODO: Use average linear density.
        n0 = nop
        lgm = 0.5 * (rl / n0) ** 2 * gn0

        # Following equations expect rm in simulation box units.
        rm /= rl

        # The one-dimensional scattering length.
        # NOTICE: Here has to appear a two factor in order to be consistent
        # with the Lieb-Liniger theory.
        a1d = 2.0 / (lgm * n0)

        def _nonlinear_relation(k2rm):
            return k2rm * a1d * sin(k2rm) - rm * cos(k2rm)

        k2 = brentq(_nonlinear_relation, 0, pi / 2) / rm

        # NOTICE: return value must ve a tuple!
        return k2 / rl,

    def subsidiary_params(self, rm: float, rl: float) -> tuple:
        """Return the params that join the two-body piecewise function.

        :param rm: The point where both the pieces of the function must be
                   joined.
        :param rl:
        :return:
        """
        gn0 = self.interaction_strength
        n0 = self.boson_number
        return self.two_body_parameters(gn0, n0, rm, rl)

    def __repr__(self):
        params = self.params
        return "LiebModel({}, {}, {}, {}, {})".format(*params)
