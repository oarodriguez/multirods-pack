from math import atan, cos, pi, sin, sqrt, tan
from typing import Callable, MutableMapping

from numba import jit
from scipy.optimize import brentq

from multirods_pack.constants import ER
from multirods_pack.ideal import eigen_energy
from multirods_pack.qmc.base import KernelFN, potential_fn
from multirods_pack.qmc.config import Config
from multirods_pack.qmc.kernel import MultiRodsModel
from multirods_pack.qmc.variational import (
    CbCSampler as MHSampler, Estimator as BaseEstimator
)
from ..trial_funcs.bloch import (
    gs_one_body_func, gs_one_body_func_log_dz,
    gs_one_body_func_log_dz2
)
from ..trial_funcs.contact import (
    phonon_two_body_func, phonon_two_body_func_log_dz,
    phonon_two_body_func_log_dz2
)


class PhononModel(MultiRodsModel):
    """Represents a many-body quantum system whose trial
    wave function is built from a product of two-body trial
    functions of Phonon type.
    """

    def __init__(self, lattice_depth,
                 lattice_ratio,
                 interaction_strength,
                 boson_number,
                 supercell_size,
                 two_body_cutoff,
                 energy_unit=ER):
        """
        :param lattice_depth: The potential magnitude in dimension-less
              units.
        :param lattice_ratio: The ratio width/separation of the barriers,
              i.e. :math:`b/a`.
        :param interaction_strength: The interaction potential strength
             :math:`\gamma`
        :param boson_number: The density of bosons within an unit cell
             of the system.
        :param supercell_size:
        """

        self.two_body_cutoff = two_body_cutoff

        super().__init__(lattice_depth,
                         lattice_ratio,
                         interaction_strength,
                         boson_number,
                         supercell_size,
                         energy_unit=energy_unit)

    @property
    def boundaries(self):
        scs = self.supercell_size
        return 0, scs

    @property
    def one_body_fn_params(self):
        """

        :return:
        """
        v0 = self.lattice_depth
        r = self.lattice_ratio

        e0 = float(eigen_energy(v0, r))
        k1, kp1 = sqrt(e0), sqrt(v0 - e0)
        return v0, r, e0, k1, kp1

    @property
    def two_body_fn_params(self):
        """

        :return:
        """
        gn = self.interaction_strength
        nop = self.boson_number
        rm = self.two_body_cutoff
        scs = self.supercell_size

        return (rm, scs) + self.match_two_body_fn(gn, nop, rm, scs)

    @property
    def pdf_params(self):
        """

        :return:
        """
        return self.one_body_fn_params, self.two_body_fn_params

    @staticmethod
    def match_two_body_fn(interaction_strength,
                          boson_number,
                          two_body_cutoff,
                          supercell_size):
        """Calculate the unknown constants that join the two pieces of the
        two-body functions of the Jastrow trial function at the point `zm_var`.
        The parameters are a function of the boson interaction magnitude `g`
        and the average linear density `boson_number` of the system.

        :param interaction_strength: The magnitude of the interaction
                                     between bosons.
        :param boson_number: The density of bosons in the simulation box.
        :param two_body_cutoff: The point where both the pieces of the
                               function must be joined.
        :param supercell_size:
        :return: The two body momentum that match the functions.
        """
        gn0, n0 = interaction_strength, boson_number
        rm, rl = two_body_cutoff, supercell_size

        if gn0 == 0:
            return 0, 0, 1 / 2 * rl, 1

        # Convert interaction energy to Lieb gamma.
        lgm = 0.5 * (rl / n0) ** 2 * gn0

        # Following equations require rm in simulation box units.
        rm /= rl

        def _nonlinear_equation(k2rm, *args):
            a1d = args[0]
            beta_rm = tan(pi * rm) / pi if k2rm == 0 else (
                    k2rm / pi * (rm - k2rm * a1d * tan(k2rm)) * tan(pi * rm) /
                    (k2rm * a1d + rm * tan(k2rm))
            )

            # Equality of the local energy at `rm`.
            fn2d_rm_eq = (
                    (k2rm * sin(pi * rm)) ** 2 +
                    (pi * beta_rm * cos(pi * rm)) ** 2 -
                    pi ** 2 * beta_rm * rm
            )

            return fn2d_rm_eq

        # The one-dimensional scattering length.
        # ❗ NOTICE ❗: Here has to appear a two factor in order to be
        # consistent with the Lieb-Liniger theory.
        a1d = 2.0 / (lgm * n0)

        k2rm = brentq(_nonlinear_equation, 0, pi / 2, args=(a1d,))

        beta_rm = (
                k2rm / pi * (rm - k2rm * a1d * tan(k2rm)) * tan(pi * rm) /
                (k2rm * a1d + rm * tan(k2rm))
        )

        k2 = k2rm / rm
        k2r_off = atan(1 / (k2 * a1d))

        beta = beta_rm / rm
        r_off = k2r_off / k2
        am = sin(pi * rm) ** beta / cos(k2rm - k2r_off)

        # The coefficient `am` is fixed by the rest of the parameters.
        # am = sin(pi * rm) ** beta / cos(k2 * (rm - r_off))
        # Return momentum and length in units of lattice period.
        return k2 / rl, beta, r_off * rl, am
        # return k2, beta, r_off, am

    @property
    def potential_fn(self) -> 'Callable':
        """

        :return:
        """
        v0 = self.lattice_depth
        r = self.lattice_ratio

        @jit('f8(f8)', nopython=True, cache=True)
        def potential_fn_(z):
            """"""
            return potential_fn(z, v0, r)

        return potential_fn_

    @property
    def one_body_fn(self) -> 'Callable':
        """

        :return:
        """
        params = self.one_body_fn_params

        @jit('f8(f8)', nopython=True, cache=True)
        def one_body_fn_(z):
            """"""
            return gs_one_body_func(z, *params)

        return one_body_fn_

    @property
    def one_body_fn_log_dz(self) -> 'Callable':
        """

        :return:
        """
        params = self.one_body_fn_params

        @jit('f8(f8)', nopython=True, cache=True)
        def one_body_fn_ldz_(z):
            """"""
            return gs_one_body_func_log_dz(z, *params)

        return one_body_fn_ldz_

    @property
    def one_body_fn_log_dz2(self) -> 'Callable':
        """

        :return:
        """
        params = self.one_body_fn_params

        @jit('f8(f8)', nopython=True, cache=True)
        def one_body_fn_ldz2_(z):
            """"""
            return gs_one_body_func_log_dz2(z, *params)

        return one_body_fn_ldz2_

    @property
    def two_body_fn(self) -> 'Callable':
        """

        :return:
        """
        params = self.two_body_fn_params

        @jit('f8(f8)', nopython=True, cache=True)
        def two_body_fn_(rz):
            """"""
            return phonon_two_body_func(rz, *params)

        return two_body_fn_

    @property
    def two_body_fn_log_dz(self) -> 'Callable':
        """

        :return:
        """
        params = self.two_body_fn_params

        @jit('f8(f8)', nopython=True, cache=True)
        def two_body_fn_log_dz_(rz):
            """"""
            return phonon_two_body_func_log_dz(rz, *params)

        return two_body_fn_log_dz_

    @property
    def two_body_fn_log_dz2(self) -> 'Callable':
        """

        :return:
        """
        params = self.two_body_fn_params

        @jit('f8(f8)', nopython=True, cache=True)
        def two_body_fn_log_dz2_(rz):
            """"""
            return phonon_two_body_func_log_dz2(rz, *params)

        return two_body_fn_log_dz2_


class ModelSampler(object):

    def __init__(self, model: PhononModel):
        """

        :param model:
        """
        self.model = model

        self.pdf_sampler = MHSampler(model)

    def __call__(self, config: MutableMapping):
        """

        :param config
        :return:
        """
        sampler_config = Config.get_group('sampler', config,
                                          drop_gid=True)

        delta_mh = sampler_config.pop('move_delta')

        displace_params = delta_mh
        pdf_sample, ratio = self.pdf_sampler(displace_params,
                                             **sampler_config)
        return pdf_sample, ratio


class EnergyEstimator(object):
    """"""

    def __init__(self, model: PhononModel):
        """

        :param model:
        """
        property_shape = (1,)

        self.model = model

        self.property_shape = property_shape

        self.estimator = BaseEstimator(model, KernelFN.L_E)

    def __call__(self, sampler_output, config):
        """

        :param sampler_output
        :param config:
        :return:
        """
        pdf_sample, ratio = sampler_output
        est_config = Config.get_group('estimator.energy',
                                      config, drop_gid=True)
        est_params = ()
        return self.estimator(est_params, pdf_sample, **est_config)


class OBDEstimator(object):
    """"""

    def __init__(self, model: PhononModel):
        """

        :param model:
        """
        self.model = model

        self.estimator = BaseEstimator(model, KernelFN.L_OBD)

    def __call__(self, sampler_output, config):
        """

        :param sampler_output
        :param config:
        :return:
        """
        pdf_sample, ratio = sampler_output
        est_config = Config.get_group('estimator.one_body_density',
                                      config, drop_gid=True)

        z_off = est_config.pop('particle_offset')
        est_params = (z_off,)
        return self.estimator(est_params, pdf_sample, **est_config)
