from math import cos, cosh, sinh, sqrt, tan, tanh

from numba import jit

from ..base import QMCDict, dict_attribute


@jit('f8(f8,f8,f8,f8,f8,f8)', nopython=True, cache=True)
def gs_one_body_func(z, v0, r, e0, k1, kp1):
    """"""
    z_cell = (z % 1.)
    z_a, z_b = 1 / (1 + r), r / (1 + r)
    if z_a < z_cell:
        # Zero potential region.
        return cosh(kp1 * (z_cell - 1. + 0.5 * z_b))
    else:
        # Region where the potential is zero.
        cf = sqrt(1 + v0 / e0 * sinh(0.5 * sqrt(v0 - e0) * z_b) ** 2.0)
        return cf * cos(k1 * (z_cell - 0.5 * z_a))


@jit('f8(f8,f8,f8,f8,f8,f8)', nopython=True, cache=True)
def gs_one_body_func_log_dz(z, v0, r, e0, k1, kp1):
    """"""
    z_cell = (z % 1.)
    z_a, z_b = 1 / (1 + r), r / (1 + r)
    if z_a < z_cell:
        # Region with nonzero potential.
        return kp1 * tanh(kp1 * (z_cell - 1. + 0.5 * z_b))
    else:
        # Region where the potential is zero.
        return -k1 * tan(k1 * (z_cell - 0.5 * z_a))


@jit('f8(f8,f8,f8,f8,f8,f8)', nopython=True, cache=True)
def gs_one_body_func_log_dz2(z, v0, r, e0, k1, kp1):
    """"""
    z_cell = (z % 1.)
    z_a, z_b = 1 / (1 + r), r / (1 + r)
    return v0 - e0 if z_a < z_cell else -e0


class GSBlochFuncs(QMCDict):
    """"""

    @dict_attribute('one_body_func')
    def one_body_func(self):
        """"""
        return gs_one_body_func

    @dict_attribute('one_body_func_log_dz')
    def one_body_func_log_dz(self):
        """ """
        return gs_one_body_func_log_dz

    @dict_attribute('one_body_func_log_dz2')
    def one_body_func_log_dz2(self):
        """"""
        return gs_one_body_func_log_dz2


class OptGSBlochFuncs(GSBlochFuncs):
    """"""

    def __init__(self, opt_params):
        """

        :param opt_params:
        """
        super().__init__()
        self.opt_params = opt_params

    @dict_attribute('one_body_func')
    def one_body_func(self):
        """

        :return:
        """
        params = self.opt_params

        @jit('f8(f8)', nopython=True, cache=True)
        def opt_one_body_func(z):
            """"""
            return gs_one_body_func(z, *params)

        return opt_one_body_func

    @dict_attribute('one_body_func_log_dz')
    def one_body_func_log_dz(self):
        """

        :return:
        """
        params = self.opt_params

        @jit('f8(f8)', nopython=True, cache=True)
        def opt_one_body_func_log_dz(z):
            """"""
            return gs_one_body_func_log_dz(z, *params)

        return opt_one_body_func_log_dz

    @dict_attribute('one_body_func_log_dz2')
    def one_body_func_log_dz2(self):
        """

        :return:
        """
        params = self.opt_params

        @jit('f8(f8)', nopython=True, cache=True)
        def opt_one_body_func_log_dz2(z):
            """"""
            return gs_one_body_func_log_dz2(z, *params)

        return opt_one_body_func_log_dz2
