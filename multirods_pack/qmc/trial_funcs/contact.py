from math import atan, cos, fabs, pi, sin, tan

from numba import jit
from scipy.optimize import brentq

from ..base import QMCDict, dict_attribute


@jit('f8(f8,f8,f8,f8,f8,f8,f8)', nopython=True, cache=True)
def phonon_two_body_func(rz, rm, scs, k2, beta, r_off, am):
    """"""
    # Two-body term.
    if rz < fabs(rm):
        return am * cos(k2 * (rz - r_off))
    else:
        return sin(pi * rz / scs) ** beta


@jit('f8(f8,f8,f8,f8,f8,f8,f8)', nopython=True, cache=True)
def phonon_two_body_func_log_dz(rz, rm, scs, k2, beta, r_off, am):
    """"""
    # Two-body term.
    if rz < fabs(rm):
        return -k2 * tan(k2 * (rz - r_off))
    else:
        return (pi / scs) * beta / (tan(pi * rz / scs))


@jit('f8(f8,f8,f8,f8,f8,f8,f8)', nopython=True, cache=True)
def phonon_two_body_func_log_dz2(rz, rm, scs, k2, beta, r_off, am):
    """"""
    # Two-body term.
    if rz < fabs(rm):
        return -k2 * k2
    else:
        return (pi / scs) ** 2 * beta * (
                (beta - 1) / (tan(pi * rz / scs) ** 2) - 1
        )


class PhononFuncs(QMCDict):
    """"""

    @dict_attribute('two_body_func')
    def two_body_func(self):
        """"""
        return phonon_two_body_func

    @dict_attribute('two_body_func_log_dz')
    def two_body_func_log_dz(self):
        """"""
        return phonon_two_body_func_log_dz

    @dict_attribute('two_body_func_log_dz2')
    def two_body_func_log_dz2(self):
        """"""
        return phonon_two_body_func_log_dz2

    @staticmethod
    def two_body_func_match_params(gn, nop, rm, scs):
        """Calculate the unknown constants that join the two pieces of the
        two-body functions of the Jastrow trial function at the point `zm_var`.
        The parameters are a function of the boson interaction magnitude `g`
        and the average linear density `boson_number` of the system.

        :param gn: The magnitude of the interaction
                                     between bosons.
        :param nop: The density of bosons in the simulation box.
        :param rm: The point where both the pieces of the
                               function must be joined.
        :param scs:
        :return: The two body momentum that match the functions.
        """

        if gn == 0:
            return 0, 0, 1 / 2 * scs, 1

        # Convert interaction energy to Lieb gamma.
        lgm = 0.5 * (scs / nop) ** 2 * gn

        # Following equations require rm in simulation box units.
        rm /= scs

        def _nonlinear_equation(k2rm, *args):
            a1d = args[0]
            beta_rm = tan(pi * rm) / pi if k2rm == 0 else (
                    k2rm / pi * (rm - k2rm * a1d * tan(k2rm)) * tan(pi * rm) /
                    (k2rm * a1d + rm * tan(k2rm))
            )

            # Equality of the local energy at `rm`.
            fn2d_rm_eq = (
                    (k2rm * sin(pi * rm)) ** 2 +
                    (pi * beta_rm * cos(pi * rm)) ** 2 -
                    pi ** 2 * beta_rm * rm
            )

            return fn2d_rm_eq

        # The one-dimensional scattering length.
        # ❗ NOTICE ❗: Here has to appear a two factor in order to be
        # consistent with the Lieb-Liniger theory.
        a1d = 2.0 / (lgm * nop)

        k2rm = brentq(_nonlinear_equation, 0, pi / 2, args=(a1d,))

        beta_rm = (
                k2rm / pi * (rm - k2rm * a1d * tan(k2rm)) * tan(pi * rm) /
                (k2rm * a1d + rm * tan(k2rm))
        )

        k2 = k2rm / rm
        k2r_off = atan(1 / (k2 * a1d))

        beta = beta_rm / rm
        r_off = k2r_off / k2
        am = sin(pi * rm) ** beta / cos(k2rm - k2r_off)

        # The coefficient `am` is fixed by the rest of the parameters.
        # am = sin(pi * rm) ** beta / cos(k2 * (rm - r_off))
        # Return momentum and length in units of lattice period.
        return k2 / scs, beta, r_off * scs, am
        # return k2, beta, r_off, am

    @classmethod
    def two_body_func_params(cls, gn, nop, rm, scs):
        """

        :param gn:
        :param nop:
        :param rm:
        :param scs:
        :return:
        """
        match_params = cls.two_body_func_match_params(gn, nop, rm, scs)
        return (rm, scs) + match_params


class OptPhononFuncs(PhononFuncs):

    def __init__(self, opt_params):
        """

        :param opt_params:
        """
        super().__init__()
        self.opt_params = opt_params

    @dict_attribute('two_body_func')
    def two_body_func(self):
        """

        :return:
        """
        params = self.opt_params

        @jit('f8(f8)', nopython=True, cache=True)
        def opt_two_body_func(rz):
            """"""
            return phonon_two_body_func(rz, *params)

        return opt_two_body_func

    @dict_attribute('two_body_func_log_dz')
    def two_body_func_log_dz(self):
        """

        :return:
        """
        params = self.opt_params

        @jit('f8(f8)', nopython=True, cache=True)
        def opt_two_body_func_log_dz(rz):
            """"""
            return phonon_two_body_func_log_dz(rz, *params)

        return opt_two_body_func_log_dz

    @dict_attribute('two_body_func_log_dz2')
    def two_body_func_log_dz2(self):
        """

        :return:
        """
        params = self.opt_params

        @jit('f8(f8)', nopython=True, cache=True)
        def opt_two_body_func_log_dz2(rz):
            """"""
            return phonon_two_body_func_log_dz2(rz, *params)

        return opt_two_body_func_log_dz2
