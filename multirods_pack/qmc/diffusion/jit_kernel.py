from math import exp, log, sqrt

import numpy as np
from numba import jit
from numpy import random

from multirods_pack.qmc.base import KernelFN, recast_to_supercell
from multirods_pack.qmc.kernel import (
    DRIFT_SLOT, MultiRodsModel, POS_SLOT
)

STAT_REJECTED = 0
STAT_ACCEPTED = 1


@jit(['f8(f8,f8,f8,f8)'], nopython=True)
def weight_func(time_delta, loc_energy, loc_energy_next, ref_energy):
    """Calculates the weight factor.

    :param time_delta:
    :param loc_energy:
    :param loc_energy_next:
    :param ref_energy:
    :return:
    """
    tau = time_delta
    le = loc_energy
    le_next = loc_energy_next
    ref_e = ref_energy

    return exp(-tau * ((le + le_next) / 2 - ref_e))


@jit('f8[:](f8[:])', nopython=True)
def get_branch_factors(weights):
    """

    :param weights:
    :return:
    """
    branch_factors = np.zeros_like(weights)
    for i_, w in enumerate(weights):
        # Take the integer part as the branching factor.
        branch_factors[i_] = int(w + random.rand())

    return branch_factors


@jit(['f8(f8,f8[:,:],f8[:,:])'], nopython=True, cache=True)
def green_func_quot_log(time_delta, sys_conf, next_conf):
    """

    :param time_delta:
    :param sys_conf:
    :param next_conf:
    :return:
    """
    tau = time_delta

    sigma = 2 * tau
    z_sys = sys_conf[:, POS_SLOT]
    z_next = next_conf[:, POS_SLOT]
    vd_sys = sys_conf[:, DRIFT_SLOT]
    vd_next = next_conf[:, DRIFT_SLOT]

    fwd_arg = z_next - z_sys - 2. * vd_sys * tau
    bwd_arg = z_sys - z_next - 2. * vd_next * tau

    fwd_arg_sqr = np.sum(fwd_arg * fwd_arg)
    bwd_arg_sqr = np.sum(bwd_arg * bwd_arg)

    return (fwd_arg_sqr - bwd_arg_sqr) / (2 * sigma)


def jit_basic_diffusion(model: MultiRodsModel):
    """Builds a JIT-compiled function that samples the transition
    matrix of the Diffusion Monte Carlo algorithm. This function
    does not realize any Metropolis-Hastings algorithm.

    :param model:
    :return:
    """
    nop = model.boson_number
    z_min, z_max = model.boundaries
    loc_energy_func = model.kernel[KernelFN.L_E]

    @jit(nopython=True)
    def diffuse_conf(sys_conf: np.ndarray,
                     time_delta: 'float',
                     loc_energy: float,
                     ref_energy: float):
        """

        :param sys_conf:
        :param time_delta:
        :param loc_energy:
        :param ref_energy:
        :return:
        """
        normal = random.normal
        tau = time_delta
        sigma = 2 * tau
        le = loc_energy

        z_sys = sys_conf[:, POS_SLOT]
        vd_sys = sys_conf[:, DRIFT_SLOT]
        z_diff = normal(0, scale=sqrt(sigma), size=(nop,))

        z_sys = z_sys + 2 * vd_sys * tau + z_diff
        for k_ in range(nop):
            z_k = z_sys[k_]
            sys_conf[k_, POS_SLOT] = recast_to_supercell(z_k, z_min, z_max)

        next_loc_energy = loc_energy_func(sys_conf)
        weight = weight_func(tau, le, next_loc_energy, ref_energy)
        return next_loc_energy, weight

    return diffuse_conf


def jit_metropolis_diffusion(model: MultiRodsModel):
    """

    :param model:
    :return:
    """
    nop = model.boson_number  # Size of configuration space
    loc_energy_func = model.kernel[KernelFN.L_E]
    kth_pdf_log_update = model.kernel[KernelFN.D_PDF_LOG_K_M]
    ith_drift_kth_update = model.kernel[KernelFN.D_I_DRIFT_K_M]

    z_min, z_max = model.boundaries

    @jit(['(f8[:,:],f8[:,:],i8,f8)'], nopython=True, cache=True, nogil=True)
    def kth_part_diffuse(sys_conf: np.ndarray,
                         aux_conf: np.ndarray,
                         k_: int,
                         time_delta: float):
        """

        :param k_:
        :param aux_conf:
        :param time_delta:
        :param sys_conf:
        :return:
        """
        rand = random.rand
        normal = random.normal
        variance = 2 * time_delta

        z_k = sys_conf[k_, POS_SLOT]
        vd_k = sys_conf[k_, DRIFT_SLOT]

        gauss_diff = normal(0, sqrt(variance))
        z_k_drift = 2 * vd_k * time_delta + gauss_diff
        z_k_upd = recast_to_supercell(z_k + z_k_drift, z_min, z_max)

        aux_conf[k_, POS_SLOT] = z_k_upd
        for i_ in range(nop):
            vd_k_diff = ith_drift_kth_update(i_, k_, z_k_drift, sys_conf)
            aux_conf[i_, DRIFT_SLOT] = vd_k + vd_k_diff

        pdf_log_upd = kth_pdf_log_update(k_, z_k_drift, sys_conf)
        gf_quot_log_ = green_func_quot_log(time_delta, sys_conf, aux_conf)

        # Metropolis-Hasting algorithm to satisfy the detailed
        # balance condition.
        pdf_prop_log = 2 * pdf_log_upd + gf_quot_log_
        if pdf_prop_log >= log(rand()):
            # Update the system configuration.
            sys_conf[k_, POS_SLOT] = z_k_upd
            sys_conf[:, DRIFT_SLOT] = aux_conf[:, DRIFT_SLOT]
            return gauss_diff, STAT_ACCEPTED

        else:
            # Return the auxiliary configuration to the initial
            # system configuration.
            aux_conf[k_, POS_SLOT] = z_k
            aux_conf[:, DRIFT_SLOT] = sys_conf[:, DRIFT_SLOT]
            return 0., STAT_REJECTED

    @jit(nopython=True)
    def diffuse_conf(sys_conf: np.ndarray,
                     time_delta: float,
                     loc_energy: float,
                     ref_energy: float):
        """

        :param sys_conf:
        :param time_delta:
        :param loc_energy:
        :param ref_energy:
        :return:
        """
        accepted = 0
        mean_sqr_diff = 0.
        aux_conf = sys_conf.copy()

        for k_ in range(nop):
            diffuse_rv = kth_part_diffuse(sys_conf, aux_conf, k_, time_delta)
            efv_diff, move_stat = diffuse_rv
            accepted += move_stat
            mean_sqr_diff += efv_diff ** 2

        eff_time = mean_sqr_diff / nop / 2.
        next_loc_energy = loc_energy_func(sys_conf)
        weight = weight_func(eff_time,
                             loc_energy,
                             next_loc_energy,
                             ref_energy)

        return next_loc_energy, weight

    return diffuse_conf
