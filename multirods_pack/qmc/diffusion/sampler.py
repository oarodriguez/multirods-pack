from math import log

import numpy as np

from multirods_pack.qmc.base import KernelFN
from multirods_pack.qmc.diffusion import get_branch_factors
from multirods_pack.qmc.kernel import MultiRodsModel
from .jit_kernel import jit_basic_diffusion

ENERGY_SLOT = 2
WEIGHT_SLOT = 3


class DMCSampler(object):
    """"""

    max_num_conf = 5000

    def __init__(self, model: MultiRodsModel):
        """

        :param model:
        """
        self.model = model

        advance_time_step_fn = jit_basic_diffusion(model)

        self.kernel = advance_time_step_fn

    @classmethod
    def branch(cls, sys_conf_set, lew_set, branch_factors):
        """Creates (branches) or destroy configurations accordingly to
        theirs branching factors.

        :param sys_conf_set:
        :param lew_set:
        :param branch_factors:
        :return:
        """
        nw, css, v = sys_conf_set.shape
        bnw = int(np.sum(branch_factors))
        bnw = min(bnw, cls.max_num_conf)

        # NOTE: Do not allow negative weights
        if not np.alltrue(np.asarray(branch_factors) >= 0):
            raise ValueError('negative branch factors are not allowed')

        # NOTE: I think the next segment could be jit if necessary
        branched_set = np.zeros((bnw, css, v), dtype=np.float64)
        branched_lew_set = np.zeros((bnw, 2), dtype=np.float64)
        nj_off = 0
        stop_branch = False
        for j_ in range(nw):
            bf = int(branch_factors[j_])
            if not bf:
                continue

            for nj_ in range(bf):
                if nj_off + nj_ >= bnw:
                    stop_branch = True
                    break
                branched_set[nj_off + nj_, :, :] = sys_conf_set[j_, :, :]
                branched_lew_set[nj_off + nj_, :] = lew_set[j_, :]

            if stop_branch:
                break
            nj_off += bf

        # NOTE: Should we set the weight to unity for the copies?
        # branched_lew_set[:, 1] = 1.

        return branched_set, branched_lew_set

    def advance_time_step(self, time_delta: float,
                          ref_energy: float,
                          lew_set: np.ndarray,
                          sys_conf_set: np.ndarray):
        """

        :param time_delta:
        :param ref_energy:
        :param sys_conf_set:
        :param lew_set:
        :return:
        """
        next_lew_set = lew_set.copy()
        for i_, sys_conf in enumerate(sys_conf_set):
            loc_energy = lew_set[i_, 0]
            step_results = self.kernel(sys_conf,
                                       time_delta,
                                       loc_energy,
                                       ref_energy)
            le_next, weight = step_results
            next_lew_set[i_, 0] = le_next
            next_lew_set[i_, 1] = weight

        return next_lew_set

    def exec(self, time_delta: float,
             ini_conf_set: np.ndarray,
             num_time_steps: int,
             ref_energy: float = None,
             target_weight: float = None):
        """

        # :param ref_energy:
        :param time_delta:
        :param num_time_steps:
        :param ref_energy:
        :param ini_conf_set:
        :param target_weight:
        :return:
        """
        model = self.model
        loc_energy_fn = model.kernel[KernelFN.L_E]

        dt = time_delta
        nts = num_time_steps
        tw = target_weight or 2000

        ini_conf_set_ = ini_conf_set.copy()
        nw, *_ = ini_conf_set_.shape

        le_w_set = np.zeros((nw, 2), dtype=np.float64)
        loc_energies = le_w_set[:, 0]
        weights = le_w_set[:, 1]
        for i_, sys_conf in enumerate(ini_conf_set_):
            loc_energies[i_] = loc_energy_fn(sys_conf)
            weights[i_] = 1.

        total_weight = weights.sum()
        energy_avg = np.average(loc_energies, weights=weights)
        total_energy = total_weight * energy_avg

        sys_conf_set = ini_conf_set
        energy_guess = total_energy / total_weight
        ref_energy = ref_energy or energy_guess

        iter_data = [0,
                     len(weights),
                     total_weight,
                     ref_energy,
                     energy_guess]
        print(*iter_data)
        loc_energy_chain = [iter_data]
        for k_ in range(nts):
            #
            le_w_set = self.advance_time_step(time_delta,
                                              ref_energy,
                                              le_w_set,
                                              sys_conf_set)

            loc_energies = le_w_set[:, 0]
            weights = le_w_set[:, 1]
            kth_it_weight = weights.sum()
            kth_it_energy_avg = np.average(loc_energies, weights=weights)

            total_weight += kth_it_weight
            total_energy += kth_it_weight * kth_it_energy_avg

            energy_guess = total_energy / total_weight
            ref_energy = energy_guess - log(kth_it_weight / tw) / dt

            iter_data = [(k_ + 1) * dt,
                         len(weights),
                         kth_it_weight,
                         kth_it_energy_avg,
                         energy_guess]
            print(*iter_data)

            loc_energy_chain.append(iter_data)

            weights = le_w_set[:, 1]

            branch_factors = get_branch_factors(weights)
            sys_conf_set, le_w_set = self.branch(sys_conf_set,
                                                 le_w_set,
                                                 branch_factors)

        return np.array(loc_energy_chain, dtype=np.float64)
