import os

from matplotlib import style

file_path = os.path.abspath(__file__)
current_dir = os.path.dirname(file_path)


def set_base_style():
    """Sets the main style for OneGPE figures. This style is suitable for
     figures used in scientific papers and printed documents.

    :return:
    """
    style_name = os.path.join(current_dir, 'onegpe-print.mplstyle')
    style.use(style_name)
