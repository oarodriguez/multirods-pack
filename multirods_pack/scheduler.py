import uuid
from typing import (
    Any, Callable, Dict, List, MutableSequence, Sequence, Tuple,
    Union
)

from distributed import Client, Future, as_completed, wait
from progressbar import AdaptiveETA, Bar, Percentage, ProgressBar

from multirods_pack.config import (
    DISTRIBUTED, EXT_DATA, FUNC, MESH_ITEMS,
    PRE_SUBMIT_FUNC
)

pb_widgets = [
    'Progress: ', Percentage(),
    ' - ',
    ' ', Bar(marker='█'),
    ' ', AdaptiveETA()
]


def exec_(schedule: Sequence[Dict],
          client: Client = None,
          apply_for=None,
          globals_=None):
    """Executes a schedule of tasks grouped in steps. The scheduler
    uses ``dask`` to distribute de load among the processes in the
    cluster.

    # TODO: Improve these docs.
    :param schedule: The schedule: a sequence of steps that describe
        a series of tasks.
    :param client: The client that submits the load among the processes
        of the cluster.
    :param apply_for: A initial data or future. The initial step load
        is submitted with this data/future. If a sequence is given, the
        step will be submitted along each element of this sequence.
    :param globals_: A set of global data/futures. They will be passed
        as data for every task submitted.
    :return:
    """
    schedule = iter(schedule)

    ini_load = next(schedule)
    futures_load, size_load, load_order = \
        submit_step_load(client, ini_load, apply_for=apply_for,
                         globals_=globals_)
    futures, size, order = futures_load, size_load, load_order

    while True:
        try:
            step_load = next(schedule)
            futures, size, order = \
                submit_step_load(client, step_load, futures, globals_)
            futures_load.extend(futures)

        except StopIteration:
            results, order_map = [], {}
            non_last = size_load - size
            pb = ProgressBar(widgets=pb_widgets)

            pb.start(max_value=size_load)
            completed = as_completed(futures_load)
            for i, future in enumerate(completed):
                if future.key in order:
                    fr = future.result()
                    results.append(fr)
                    order_map[future.key] = i - non_last

                pb.update(i + 1)

            pb.finish()
            break

        else:
            size_load += size

    # Return results in the order the tasks were submitted.
    return [results[order_map[k]] for k in order]


def submit_step_load(client: Client,
                     step_load: Dict,
                     apply_for: Union[
                         Future, Sequence[Future]
                     ] = None,
                     globals_: Sequence[Future] = None):
    """

    :param client: The client that submits the load among the processes
        of the cluster.
    :param step_load: A mapping with the description of the task.
    :param apply_for: A initial data or future. The initial step load
        is submitted with this data/future. If a sequence is given, the
        step will be submitted for each element of this sequence.
    :param globals_: A set of global data/futures. They will be passed
        as data for every task submitted.
    :return: A collection of the futures submitted to the cluster, as
        as well as the size of the load and the order of submission.
    """
    if not isinstance(apply_for, Sequence):
        return submit_step_load(client, step_load, [apply_for], globals_)

    fn = step_load['callable']
    data = step_load['args']
    distribute = step_load.get('distribute', False)

    globals_ = globals_ or ()

    # Whether to distribute the whole data or element by element
    data = (data,) if not distribute else data

    futures, size, order = [], 0, []
    for i, ef in enumerate(apply_for):
        for j, d in enumerate(data):
            sd = client.scatter(d)
            task = client.submit(fn, (sd, ef), *globals_)
            futures.append(task)
            order.append(task.key)
            size += 1

    return futures, size, order


def gather_results(futures: Sequence[Future],
                   exclude: Callable = None,
                   size: int = None,
                   count_offset: int = None):
    """Collect the results from a collection of futures
    submitted to a cluster.

    :param futures: A collection of `Futures`.
    :param exclude: A callable applied to every future. It true, that
        particular future will be excluded from the results.
    :param size: The size of the futures sequence. Optional. If not given
        it is the length of ``futures``.
    :param count_offset: An offset to get the order of a future result as
        it is completed.
    :return: The results of the non-excluded futures, as well as a mapping
        with the order in which the results got completed.
    """
    size = size or len(futures)
    results, order_map = [], {}
    pb = ProgressBar(widgets=pb_widgets)

    pb.start(max_value=size)
    completed = as_completed(futures)
    for i, future in enumerate(completed):
        if exclude(future):
            continue
        fr = future.result()
        results.append(fr)
        order_map[future.key] = i - count_offset
        pb.update(i + 1)

    pb.finish()

    return results, order_map


class DistributedTask(object):
    """"""

    def __init__(self, task_fn: Callable,
                 task_data: Any,
                 globals_=None):
        """

        :param task_fn:
        :param task_data:
        :param globals_:
        """
        self.task_fn = task_fn
        self.task_data = task_data
        # self.client = client
        self.globals_ = globals_ or ()

        self._apply_for = [None]
        self.futures = []
        self.size = 0
        self.order = []

        self.three_futures = []
        self.three_size = []

        # TODO: Is this needed? -> Yes, it is...
        self.parent = None  # type: DistributedTask
        self.children = []  # type: MutableSequence[DistributedTask]

    def apply_for(self, futures: Union[
        Future, Sequence[Future]
    ] = None):
        """

        :param futures:
        :return:
        """
        if isinstance(futures, Future):
            self._apply_for = [futures]
            return
        self._apply_for = futures

    def get_top_parent(self):
        """

        :return:
        """
        parent = self.parent
        while True:
            if parent is None:
                break
            parent = parent.parent

        return parent

    def _submit(self, client):
        """

        :return:
        """
        fn = self.task_fn
        data = self.task_data
        apply_for = self._apply_for
        globals_ = self.globals_

        # Whether to distribute the whole data or element by element

        futures, size, order = [], 0, []
        for i, ef in enumerate(apply_for):
            for j, d in enumerate(data):
                # TODO: Scatter if data is large.
                # sd, = client.scatter((d,))
                task = client.submit(fn, (d, ef), *globals_)
                futures.append(task)
                order.append(task.key)
                size += 1

        return futures, size, order

    def add_task(self, task: 'DistributedTask'):
        """

        :param task:
        :return:
        """
        task.apply_for(self.futures)
        self.children.append(task)
        task.parent = self

    def submit(self, client):
        """

        :return:
        """
        futures, size, order = self._submit(client)
        self.futures.extend(futures)
        self.size += size
        self.order.extend(order)

        for task in self.children:
            task.submit(client)

    def gather_results(self):
        """Gather the results from the cluster.

        :return:
        """
        # Gather results directly from the children.
        if self.children:
            results = []
            for task in self.children:
                task_result = task.gather_results()
                results.append(task_result)

            return results

        else:
            # This is a task without children.
            # NOTICE: How to display the progress by the upper task.
            # The upper task collects information about all the
            # sub-tasks. It sees reasonable to handle the display
            # of the execution progress only in the top parent task.
            #
            # TODO: research about this...

            order = self.order
            results, order_map = [], {}
            pb = ProgressBar(widgets=pb_widgets)

            pb.start(max_value=self.size)
            completed = as_completed(self.futures, with_results=True)
            for i, future_fr in enumerate(completed):
                future, fr = future_fr
                if future.key in order:
                    # fr = future.result()
                    results.append(fr)
                    order_map[future.key] = i

                pb.update(i + 1)

            pb.finish()

            # Return results in the order the tasks were submitted.
            return [results[order_map[k]] for k in order]


class Executor(object):
    """"""

    def __init__(self, config: List[Dict[str, Any]],
                 parent: 'Executor' = None,
                 ext_data: Dict[str, Any] = None):
        """

        :param config:
        :param ext_data:
        """
        ext_data = dict(ext_data or [])
        config = [stage_config for stage_config in config]

        # TODO: Move to a separated method.
        self_config = config.pop(0)
        cfg = self_config.get(EXT_DATA, None)
        if cfg is None:
            self_config[EXT_DATA] = ext_data
        else:
            data = dict(self_config[EXT_DATA])
            data.update(ext_data)
            self_config[EXT_DATA] = data

        self.key = str(uuid.uuid4())

        self.config = self_config

        self.next_config = config

        self.parent = parent

        # NOTE: Do we need this?
        self.level = 0 if parent is None else parent.level + 1

        self.futures = []  # type: List[Future]

        self.submit_order = []

        self.completed = False

        self.tasks = []  # type: List[Tuple[Callable, Any]]

        self.exec_units = []  # type: List[Executor]

        # NOTE: Should call setup here or outside the constructor?
        self.setup()

    @property
    def size(self):
        return len(self.futures)

    def setup(self):
        """

        :return:
        """
        config = self.config
        next_config = self.next_config

        fn = config[FUNC]
        mesh_builder = config[MESH_ITEMS]
        self_ext_data = config[EXT_DATA]
        ext_data = dict(self_ext_data)

        mesh_data = mesh_builder(ext_data)
        for i_, d in enumerate(mesh_data):
            #
            task_unit = (fn, d)
            self.tasks.append(task_unit)

            if next_config:
                exec_unit = Executor(next_config,
                                     parent=self,
                                     ext_data=d)
                self.exec_units.append(exec_unit)

    def submit(self, client: Client,
               ext_args: Tuple[Any, ...] = None,
               globals_: Tuple[Any, ...] = None):
        """

        :param client:
        :param ext_args:
        :param globals_:
        :return:
        """
        ext_args = ext_args or ()
        globals_ = globals_ or ()

        self.submit_tasks(client, ext_args, globals_)
        self.submit_exec_units(client, ext_args, globals_)

    def submit_tasks(self, client: Client,
                     ext_args: Tuple[Any, ...],
                     globals_: Tuple[Any, ...]):
        """

        :param client:
        :param ext_args:
        :param globals_:
        :return:
        """
        distributed = self.config[DISTRIBUTED]
        pre_submit_fn = self.config[PRE_SUBMIT_FUNC]

        futures, size, order = [], 0, []
        for i_, unit in enumerate(self.tasks):
            fn, d = unit

            if pre_submit_fn is not None:
                pre_fnr = pre_submit_fn(d, *globals_)
                fn_args = (d, pre_fnr) + ext_args
            else:
                fn_args = (d,) + ext_args

            if not distributed:
                fnr = fn(fn_args, *globals_)
                future, = client.scatter((fnr,))
            else:
                future = client.submit(fn, fn_args, *globals_)

            futures.append(future)
            order.append((future.key, i_))
            size += 1

        self.futures.extend(futures)
        self.submit_order.extend(order)

    def submit_exec_units(self, client: Client,
                          ext_args: Tuple[Any, ...],
                          globals_: Tuple[Any, ...]):
        """

        :param client:
        :param ext_args:
        :param globals_:
        :return:
        """
        ext_args_ = ext_args
        order = dict(self.submit_order)

        # NOTE: Wait for the futures to complete? Proceed as they complete?
        wait(self.futures)

        if self.exec_units:
            for future in self.futures:
                i_ = order[future.key]
                exec_unit = self.exec_units[i_]
                ext_args = (future,)
                exec_unit.submit(client, ext_args, globals_)

        self.completed = True

    def gather_own_results(self):
        """

        :return:
        """
        if self.completed:
            ac_futures = self.futures
        else:
            ac_futures = as_completed(self.futures)

        order = dict(self.submit_order)
        results = [None] * self.size

        # Return results in the order the tasks were submitted.
        for future in ac_futures:
            i_ = order[future.key]
            fr = future.result()
            results[i_] = fr

        self.completed = True
        return results

    def gather_results(self):
        """

        :return:
        """
        # TODO: Return only from a certain level
        if not self.exec_units:
            return self.gather_own_results()

        results = []
        for unit in self.exec_units:
            result = unit.gather_results()
            results.append(result)

        return results

    def __repr__(self):
        """"""
        tpl_str = '<Executor key={:s}, status={:s}>'
        key = self.key
        status = 'completed' if self.completed else 'pending'
        return tpl_str.format(key, status)
